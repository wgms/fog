import io
import re
from typing import Any, Dict, Hashable, List, Literal, Sequence, Set, Union

from geoalchemy2 import Geography, Geometry  # Register spatial types
import numpy as np
import pandas as pd
import psycopg2.extensions
import sqlalchemy
import sqlalchemy.exc
import sqlalchemy.sql.elements
import sqlalchemy.sql.sqltypes

import fog.metadata
import fog.io
import fog.config
import fog.helpers

# Register exotic numpy types (float64: Float64, int64: Int64)
psycopg2.extensions.register_adapter(np.float64, psycopg2.extensions.AsIs)
psycopg2.extensions.register_adapter(np.int64, psycopg2.extensions.AsIs)


# ---- Helpers -----

def format_rows_for_db(
  df: pd.DataFrame,
  is_null_string: pd.DataFrame = None
) -> List[dict]:
  """
  Examples:
  >>> df = pd.DataFrame({
  ...   'x': pd.Series([0, pd.NA], dtype='Int64'),
  ...   'y': pd.Series([pd.NA, 'b'], dtype='string')
  ... })
  >>> format_rows_for_db(df)
  [{'x': 0, 'y': None}, {'x': None, 'y': 'b'}]
  >>> format_rows_for_db(df, pd.DataFrame({'x': [True]}))
  [{'x': 'NULL', 'y': None}, {'x': None, 'y': 'b'}]
  """
  df = df.astype(object).where(df.notnull(), None)
  if is_null_string is not None:
    df[is_null_string] = 'NULL'
  return df.to_dict(orient='records')


# ---- Class -----

class DB:

  def __init__(self, url: str = None, echo: bool = False) -> None:
    url = url or fog.config.FOG_DB
    if not url:
      raise ValueError('Provide URL or set fog.config.FOG_DB')
    self.engine = sqlalchemy.create_engine(url, echo=echo)
    self.package = fog.metadata.read(submission=False, deprecated=True)
    self.connection = self.engine.connect()
    self.dialect = self.engine.dialect
    self.metadata = sqlalchemy.MetaData()
    self.reflect()

  def reflect(self) -> None:
    """
    Reload table definitions from database.
    """
    self.metadata.reflect(bind=self.engine)
    # HACK: Get numeric as float, not decimal (must be run before any queries)
    # https://stackoverflow.com/a/19387883
    for table in self.metadata.tables.values():
      for column in table.columns.values():
        if isinstance(column.type, sqlalchemy.Numeric):
          column.type.asdecimal = False

  def execute(self, query) -> sqlalchemy.CursorResult:
    try:
      return self.connection.execute(query)
    except (sqlalchemy.exc.IntegrityError, sqlalchemy.exc.OperationalError) as e:
      self.connection.rollback()
      raise e

  def commit(self) -> None:
    try:
      self.connection.commit()
    except sqlalchemy.exc.IntegrityError as e:
      self.connection.rollback()
      raise e

  def get_resource(self, table: str) -> dict:
    for resource in self.package['resources']:
      if resource['name'] == table:
        return resource
    raise ValueError(f'Table {table} not found')

  def get_next_integer(self, table: str, column: str) -> int:
    """
    Get the next available value of an integer column.

    Increments the maximum value rather than using the database sequence.
    This is more robust to database design, but means that a value may be reused
    if the row with the maximum value is deleted.
    """
    table_column = self.metadata.tables[table].c[column]
    if not isinstance(table_column.type, sqlalchemy.Integer):
      raise ValueError(f'{table}.{column} is not an integer column')
    query = (
      sqlalchemy.select(sqlalchemy.func.max(table_column))
      .where(table_column != None)
    )
    result = self.execute(query)
    return (result.all()[0][0] or 0) + 1

  def format_result_as_df(
    self,
    result: sqlalchemy.CursorResult,
    table: str = None
  ) -> pd.DataFrame:
    """
    Format query result as a dataframe, enforcing metadata column types.
    """
    columns = [column.name for column in result.cursor.description]
    df = pd.DataFrame(result, columns=columns)
    if table is None:
      return df.convert_dtypes()
    dtypes = fog.io.get_pandas_dtypes_from_metadata(
      table, columns=columns, submission=False, deprecated=True
    )
    return df.astype(dtypes, copy=False)

  def select_table(self, table: str) -> pd.DataFrame:
    """
    Select all columns and rows from a table.
    """
    t = self.metadata.tables[table]
    query = sqlalchemy.select(t)
    result = self.execute(query)
    return self.format_result_as_df(result)

  def select_rows(
    self,
    table: str,
    keys: pd.DataFrame = None
  ) -> pd.DataFrame:
    """
    Select table rows with column selection, order, and type based on metadata.
    """
    resource = self.get_resource(table)
    t = self.metadata.tables[table]
    query = sqlalchemy.select(
      *[t.c[field['name']] for field in resource['schema']['fields']]
    )
    if keys is not None:
      values = list(keys.itertuples(index=False, name=None))
      query = query.where(
        sqlalchemy.Tuple(*[t.c[key] for key in keys]).in_(values)
      )
    result = self.execute(query)
    return self.format_result_as_df(result, table=table)

  def collect_glacier_ids(self, dfs: Dict[Hashable, pd.DataFrame]) -> Set[int]:
    """
    Collect all ids present in dataframes.
    """
    ids = []
    for resource in self.package['resources']:
      if resource['name'] in dfs:
        if resource['name'] == 'glacier':
          ids.extend(dfs[resource['name']]['id'].dropna().unique())
        elif 'glacier_id' in dfs[resource['name']]:
          ids.extend(dfs[resource['name']]['glacier_id'].dropna().unique())
    return set(ids)

  def select_glacier_rows(self, ids: Sequence[int]) -> pd.DataFrame:
    """
    Get all glacier rows associated with the provided glacier ids.

    Recursively loads rows matching parent ids of initial selection:

    WITH RECURSIVE base AS (
      SELECT * FROM glacier WHERE id IN (...)
      UNION
      SELECT * FROM glacier t2
      JOIN base ON t2.id = base.parent_glacier_id
    ) SELECT * FROM base;
    """
    t = self.metadata.tables['glacier']
    # Build base query
    base = (
      sqlalchemy.select(t)
      .where(t.c['id'].in_(ids))
      .cte(recursive=True)
    )
    # Build recursive query
    t2 = t.alias()
    recursive = (
      sqlalchemy.select(t2)
      .join(base, t2.c['id'] == base.c['parent_glacier_id'])
    )
    # Union queries
    cte = base.union(recursive)
    # Select columns from result
    resource = self.get_resource('glacier')
    query = sqlalchemy.select(
      *[cte.c[field['name']] for field in resource['schema']['fields']]
    )
    result = self.execute(query)
    return self.format_result_as_df(result, table='glacier')

  def select_all_rows(self, ids: Sequence[int]) -> Dict[str, pd.DataFrame]:
    """
    Get all rows associated with one of the provided glacier ids.
    """
    dfs = {}
    # Get all metadata tables
    for resource in self.package['resources']:
      if resource['name'] == 'glacier':
        df = self.select_glacier_rows(ids)
      elif resource['name'] in ('state_band', 'change_band'):
        # NOTE: Assumes that parent table has already been loaded
        parent = resource['name'].replace('_band', '')
        if not parent in dfs:
          continue
        parent_ids = dfs[parent]['id'].dropna().unique()
        df = self.select_rows(
          resource['name'], keys=pd.DataFrame({f'{parent}_id': list(parent_ids)})
        )
      else:
        df = self.select_rows(
          resource['name'], keys=pd.DataFrame({'glacier_id': list(ids)})
        )
      if not df.empty:
        dfs[resource['name']] = df
    return dfs

  def select_all_rows_by_pkey(
    self,
    dfs: Dict[str, pd.DataFrame]
  ) -> Dict[str, pd.DataFrame]:
    """
    Get any rows with matching primary keys.
    """
    dbdfs = {}
    for resource in self.package['resources']:
      table = resource['name']
      if table not in dfs:
        continue
      if not resource['schema'].get('primaryKey'):
        continue
      keys: List[str] = resource['schema']['primaryKey']
      if not all(key in dfs[table] for key in keys):
        continue
      values = dfs[table][keys].dropna().drop_duplicates()
      rows = self.select_rows(table, keys=values)
      if not rows.empty:
        dbdfs[table] = rows
    return dbdfs

  def select_missing_rows_by_fkey(
    self,
    dfs: Dict[str, pd.DataFrame]
  ) -> Dict[str, pd.DataFrame]:
    """
    Get missing rows referenced by foreign keys.
    """
    dbdfs = {}
    # HACK: Reverse metadata table order is not guaranteed to work in the future
    for resource in self.package['resources'][::-1]:
      table = resource['name']
      if table not in dfs and table not in dbdfs:
        continue
      for fkey in resource['schema'].get('foreignKeys', []):
        ref = fkey['reference']
        # Load local keys from input and rows loaded in earlier iterations
        values = [
          db[table][fkey['fields']]
          for db in (dfs, dbdfs)
          if table in db and all(key in db[table] for key in fkey['fields'])
        ]
        if not values:
          continue
        values = (
          pd.concat(values, ignore_index=True, copy=False)
          .dropna()
          .drop_duplicates()
        )
        values.columns = ref['fields']
        # Load foreign keys from input and rows loaded in earlier iterations
        foreign = [
          db[ref['resource']][ref['fields']]
          for db in (dfs, dbdfs)
          if (
            ref['resource'] in db and
            all(key in db[ref['resource']] for key in ref['fields'])
          )
        ]
        if foreign:
          foreign = (
            pd.concat(foreign, ignore_index=True, copy=False)
            .dropna()
            .drop_duplicates()
          )
          # Remove existing keys from search
          values = values[
            ~fog.helpers.get_index(values).isin(fog.helpers.get_index(foreign))
          ]
        if values.empty:
          continue
        if table == 'glacier':
          rows = self.select_glacier_rows(ids=values['id'])
          if isinstance(foreign, pd.DataFrame):
            # Drop any rows loaded iterively that already exist
            exists = fog.helpers.get_index(rows[['id']]).isin(
              fog.helpers.get_index(foreign[['id']])
            )
            rows = rows[~exists]
        else:
          rows = self.select_rows(ref['resource'], keys=values)
        if rows.empty:
          continue
        dbdfs[ref['resource']] = pd.concat(
          [dbdfs.get(ref['resource']), rows], ignore_index=True, copy=False
        )
    return dbdfs

  def is_in_column(
    self,
    s: pd.Series,
    table: str,
    column: str,
    where: sqlalchemy.sql.elements.BinaryExpression = None
  ) -> pd.Series:
    """
    Check if values exist in a column.
    """
    if not s.index.is_unique:
      raise ValueError('Index must be unique')
    mask = s.notnull()
    if not mask.sum():
      return pd.Series(index=s.index, dtype='boolean')
    groups = s.groupby(s, dropna=True, sort=False).groups
    # Build query of the form:
    # SELECT x.id, EXISTS (
    #   SELECT *
    #   FROM t
    #   WHERE t.id = x.id
    # ) AS value
    # FROM (VALUES (1), (2), (3), (4), (5)) AS x(id);
    t = self.metadata.tables[table]
    temp_table = sqlalchemy.select(
      sqlalchemy.Values(sqlalchemy.column('id'), name='x').data(
        [(value, ) for value in groups]
      )
    )
    temp_table_subquery = temp_table.subquery()
    exists = sqlalchemy.exists().where(t.c[column] == temp_table_subquery.c['id'])
    if where is not None:
      exists = exists.where(where)
    query = sqlalchemy.select(temp_table_subquery.c['id'], exists.label('exists'))
    # Execute query and format result
    result = self.execute(query)
    valid = (
      pd.Series([row[1] for row in result], name=s.name, dtype='boolean', index=groups.values())
      .reset_index()
      .explode('index')
      .set_index('index')
    )[s.name or 0].reindex(s.index)
    if s.name is None:
      valid.name = None
    return valid

  def select_missing_rows_by_group(
    self,
    dfs: Dict[str, pd.DataFrame]
  ) -> Dict[str, pd.DataFrame]:
    """
    Get missing rows by group key.
    """
    GROUP_COLUMNS: Dict[str, list[str]] = {
      'state_band': ['state_id'],
      'change_band': ['change_id'],
      'front_variation': ['glacier_id', 'series_id'],
      'mass_balance_band': ['glacier_id', 'year'],
      'mass_balance_point': ['glacier_id', 'year']
    }
    PARENTS: Dict[str, tuple[str, list[str]]] = {
      'state_band': ('state', ['id']),
      'change_band': ('change', ['id']),
      'mass_balance_band': ('mass_balance', ['glacier_id', 'year']),
      'mass_balance_point': ('mass_balance', ['glacier_id', 'year'])
    }
    dbdfs = {}
    for resource in self.package['resources']:
      table = resource['name']
      if table not in GROUP_COLUMNS:
        continue
      gkey = GROUP_COLUMNS[table]
      pkey = resource['schema']['primaryKey']
      if not pkey:
        raise ValueError(f'{table} has no primary key')
      groups = pd.DataFrame(columns=gkey)
      # Load group keys from parent table
      if table in PARENTS:
        parent, pgkey = PARENTS[table]
        if parent in dfs:
          # Load group keys from parent table and rename to match table group keys
          groups = (
            dfs[parent][pgkey]
            .dropna().drop_duplicates()
            .rename(columns=dict(zip(pgkey, gkey)))
          )
      if table not in dfs and groups.empty:
        continue
      if table in dfs:
        df = dfs[table]
        # Primary key always superset of group columns
        if not all(column in df for column in pkey):
          continue
        mask = df[gkey].notnull().all(axis=1)
        # Select all group rows from database
        groups = pd.concat(
          (groups, df.loc[mask, gkey].drop_duplicates()), ignore_index=True
        )
        dbdf = self.select_rows(table, keys=groups)
        # Keep missing rows
        local = fog.helpers.get_index(df.loc[mask, pkey])
        foreign = fog.helpers.get_index(dbdf[pkey])
        rows = dbdf[~foreign.isin(local)]
      else:
        rows = self.select_rows(table, keys=groups)
      if rows.empty:
        continue
      dbdfs[table] = rows
    return dbdfs

  def insert_rows(
    self,
    table: str,
    df: pd.DataFrame
  ) -> sqlalchemy.CursorResult:
    t = self.metadata.tables[table]
    query = sqlalchemy.insert(t).values(format_rows_for_db(df))
    return self.execute(query)

  def update_rows(
    self,
    table: str,
    df: pd.DataFrame,
    is_null_string: pd.DataFrame = None
  ) -> List[sqlalchemy.CursorResult]:
    """
    Update rows.

    Cannot update primary key columns since these are used to identify the row.
    """
    t = self.metadata.tables[table]
    resource = self.get_resource(table)
    key = resource['schema']['primaryKey']
    results = []
    rows = format_rows_for_db(df, is_null_string=is_null_string)
    for row in rows:
      values = {
        k: None if v == 'NULL' else v
        for k, v in row.items() if k not in key and v is not None
      }
      query = sqlalchemy.update(t).values(values).where(
        *[t.c[k] == row[k] for k in key]
      )
      results.append(self.execute(query))
    return results

  def delete_rows(
    self,
    table: str,
    df: pd.DataFrame
  ) -> sqlalchemy.CursorResult:
    t = self.metadata.tables[table]
    resource = self.get_resource(table)
    key = resource['schema']['primaryKey']
    rows = format_rows_for_db(df[key])
    query = sqlalchemy.delete(t).where(
      sqlalchemy.Tuple(*[t.c[k] for k in key]).in_(
        [row.values() for row in rows]
      )
    )
    return self.execute(query)

  def commit_submission(
    self,
    delete: Dict[str, pd.DataFrame] = None,
    insert: Dict[str, pd.DataFrame] = None,
    update: Dict[str, pd.DataFrame] = None,
    is_null_string: Dict[str, pd.DataFrame] = None,
    submission_id: str = None
  ) -> List[sqlalchemy.CursorResult]:
    results = []
    if submission_id:
      sql = f"SET LOCAL audit.submission_id='{submission_id}'"
      self.execute(sqlalchemy.sql.text(sql))
    # HACK: Delete in reverse metadata table order for foreign key integrity
    for resource in self.package['resources'][::-1]:
      table = resource['name']
      if delete is not None and table in delete:
        results.append(self.delete_rows(table, delete[table]))
    # HACK: Insert / update in metadata table order for foreign key integrity
    for resource in self.package['resources']:
      table = resource['name']
      if insert is not None and table in insert:
        results.append(self.insert_rows(table, insert[table]))
      if update is not None and table in update:
        null_strings = None
        if is_null_string is not None and table in is_null_string:
          null_strings = is_null_string[table]
        results.append(self.update_rows(table, update[table], null_strings))
    self.commit()
    return results

  def select_query_from_copy(
    self,
    sql: str,
    dtype: Union[dict, Literal['string']] = 'string'
  ) -> pd.DataFrame:
    # Strip trailing semicolon for use in COPY statement
    sql = re.sub(r';\s*', '', sql)
    file = io.StringIO()
    with self.connection.connection.cursor() as cursor:
      try:
        cursor.copy_expert(f"COPY ({sql}) TO STDOUT (HEADER true, NULL '')", file)
      except sqlalchemy.exc.ProgrammingError as e:
        self.connection.connection.rollback()
        raise e
    file.seek(0)
    return pd.read_csv(
      file, sep='\t', na_values=[''], keep_default_na=False, dtype=dtype
    )

  def select_table_from_copy(
    self,
    table: str,
    columns_include: List[str] = None,
    columns_exclude: List[str] = None,
    types_exclude: List[type] = [Geometry, Geography]
  ) -> pd.DataFrame:
    t = self.metadata.tables[table]
    columns = list(t.columns)
    if columns_include is not None:
      columns = [column for column in columns if column.name in columns_include]
    if columns_exclude is not None:
      columns = [column for column in columns if column.name not in columns_exclude]
    if types_exclude is not None:
      columns = [column for column in columns if type(column.type) not in types_exclude]
    # Prepare dtype conversion
    DTYPE_MAP = {
      sqlalchemy.sql.sqltypes.INTEGER: 'Int64',
      sqlalchemy.sql.sqltypes.BIGINT: 'Int64',
      sqlalchemy.sql.sqltypes.TEXT: 'string',
      # HACK: 't' / 'f' cannot be cast to boolean
      sqlalchemy.sql.sqltypes.BOOLEAN: 'string',
      sqlalchemy.sql.sqltypes.NUMERIC: 'Float64',
      # HACK: Defer parsing
      sqlalchemy.sql.sqltypes.DATE: 'string',
      sqlalchemy.sql.sqltypes.CHAR: 'string',
      sqlalchemy.sql.sqltypes.VARCHAR: 'string'
    }
    dtypes = {column.name: DTYPE_MAP[type(column.type)] for column in columns}
    query = sqlalchemy.select(*columns)
    sql = query.compile(self.engine).string
    df = self.select_query_from_copy(sql, dtype=dtypes)
    for column in columns:
      # HACK: Cast boolean columns to boolean
      if type(column.type) is sqlalchemy.sql.sqltypes.BOOLEAN:
        df[column.name] = df[column.name].map({'t': True, 'f': False}).astype('boolean')
    return df
