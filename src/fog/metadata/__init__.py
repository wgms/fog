import functools
import importlib
from typing import Literal, Union

from ccorp.ruamel.yaml.include import YAML


OUTLINE_ID_COLUMNS = [
  ('state', 'outline_id'),
  ('change', 'begin_outline_id'),
  ('change', 'end_outline_id'),
  ('mass_balance', 'outline_id')
]
"""Outline identifier columns that need to be parsed as such."""


def read_yaml(
  path: str, encoding: str = 'utf-8', **kwargs
) -> Union[dict, list, int, float, str, bool]:
  yaml = YAML()
  return yaml.load(open(path, mode='r', encoding=encoding, **kwargs))


@functools.lru_cache(maxsize=12)
def read(
  submission: bool | Literal['extended'] = False,
  primary_keys: bool = True,
  foreign_keys: bool = True,
  deprecated: bool = False,
  migrate_categories: bool = True
) -> dict:
  """
  Read datapackage metadata.

  Parameters
  ----------
  submission
    Whether to include tables and columns with `submission` marked as `true` (True),
    as `true` or `extended` (extended), or as `false` or `extended` (False).
  primary_keys
    Whether to include primary keys.
  foreign_keys
    Whether to include foreign keys.
  deprecated
    Whether to include tables and columns marked as `deprecated: true` (True).
  migrate_categories
    Whether to migrate categories to enum constraints and inject into descriptions.
  """
  with importlib.resources.path(f'{__package__}', 'index.yml') as path:
    metadata = read_yaml(path)
  # Filter resources
  resources = []
  for resource in metadata['resources']:
    is_submission = resource.get('submission', None)
    is_deprecated = resource.get('deprecated', False)
    if (
      (
        (submission == 'extended' and is_submission in (None, True, 'extended'))
        or (submission is False and is_submission in (None, False, 'extended'))
        or (submission is True and is_submission in (None, True))
      )
      and (deprecated is True or is_deprecated is not True)
    ):
      resources.append(resource)
  metadata['resources'] = resources
  # Filter resource fields
  field_names = {}
  for resource in metadata['resources']:
    fields = []
    for field in resource['schema']['fields']:
      is_submission = field.get('submission', None)
      is_deprecated = field.get('deprecated', False)
      if (
        (
          (submission == 'extended' and is_submission in (None, True, 'extended'))
          or (submission is False and is_submission in (None, False, 'extended'))
          or (submission is True and is_submission in (None, True))
        )
        and (deprecated is True or is_deprecated is not True)
      ):
        fields.append(field)
    names = [field['name'] for field in fields]
    if len(set(names)) < len(names):
      raise ValueError(
        f'duplicate field names in {resource["name"]}: {names}'
      )
    field_names[resource['name']] = names
    resource['schema']['fields'] = fields
  # Filter primary keys and foreign keys
  for resource in metadata['resources']:
    if primary_keys:
      # Keep only if all fields are still present
      if 'primaryKey' in resource['schema']:
        missing = [
          name for name in resource['schema']['primaryKey']
          if name not in field_names[resource['name']]
        ]
        if missing:
          resource['schema']['primaryKey'] = None
    else:
      resource['schema']['primaryKey'] = None
    if foreign_keys:
      keys = []
      # Drop missing fields from foreign keys
      for key in resource['schema'].get('foreignKeys', []):
        if key['reference']['resource'] not in field_names:
          continue
        remaining = [
          (local, foreign)
          for local, foreign in zip(key['fields'], key['reference']['fields'])
          if local in field_names[resource['name']] and
          foreign in field_names[key['reference']['resource']]
        ]
        if remaining:
          key['fields'] = [local for local, _ in remaining]
          key['reference']['fields'] = [foreign for _, foreign in remaining]
          keys.append(key)
      resource['schema']['foreignKeys'] = keys
    else:
      resource['schema']['foreignKeys'] = []
  if migrate_categories:
    migrate_categories_to_enum(metadata)
  return metadata


def migrate_categories_to_enum(metadata: dict) -> None:
  """Migrate categories to enum constraint and inject into description."""
  for resource in metadata['resources']:
    for field in resource['schema']['fields']:
      if 'categories' in field:
        values = list(field['categories'])
        descriptions = list(field['categories'].values())
        # Move categories to enum constraint
        field['constraints'] = field.get('constraints', {})
        field['constraints']['enum'] = values
        del field['categories']
        # Inject categories into field description
        if 'description' not in field:
          raise ValueError(f'no description for {field["name"]}')
        text = '\n'.join(
          [f'- {key}: {value}' for key, value in zip(values, descriptions)]
        )
        lines = field['description'].split('\n')
        field['description'] = lines[0] + '\n' + text + '\n' + '\n'.join(lines[1:])
