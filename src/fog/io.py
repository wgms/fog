from collections import defaultdict
import importlib
from pathlib import Path
import re
from typing import Any, Dict, Hashable, Iterable, List, Optional, Sequence, Tuple, Union
import warnings

import daff
import pandas as pd
from validator import Check, Column, Schema
import validator.daff
from validator.schema import Report
import xlsxwriter
import xlsxwriter.worksheet

import fog.checks
import fog.config
from fog.config import colorama
import fog.helpers
import fog.metadata
import fog.migrations


UNNAMED_COLUMN_REGEX = re.compile(r'Unnamed: [0-9]+', flags=re.IGNORECASE)
"""Unnamed column name read by pd.read_excel."""


def read_excel(path: Union[str, Path], dtype: str = None) -> Dict[str, pd.DataFrame]:
  """Read Excel file."""
  book = pd.ExcelFile(path)
  dfs = {}
  for name in book.sheet_names:
    with warnings.catch_warnings():
      # Suppress openpyxl warnings that validation and formatting are ignored
      warnings.simplefilter('ignore')
      df = book.parse(
        sheet_name=name, na_values=[''], keep_default_na=False, dtype=dtype
      )
      try:
        set_row_numbers_as_index(df, inplace=True)
      except ValueError as error:
        raise ValueError(f'{error} ({name} in {path})')
      dfs[name] = df
  fixes = Schema({
    Column(): [
      Check.format_date_read_as_date(),
      Check.format_integer_read_as_float()
    ]
  })
  return fixes(dfs, copy=False).output


def set_row_numbers_as_index(
  df: pd.DataFrame, inplace: bool = False
) -> pd.DataFrame:
  """
  Set the first column, if unnamed, as the integer row index.

  Raises
  ------
  ValueError
    If missing, duplicate, or non-integer values are found in the row index.

  Example
  -------
  >>> df = pd.DataFrame({'x': ['1']})
  >>> set_row_numbers_as_index(df)
     x
  0  1
  >>> df = pd.DataFrame({'': ['2'], 'x': ['1']})
  >>> set_row_numbers_as_index(df)
     x
  2  1
  >>> df = pd.DataFrame({'unnamed: 1': ['2'], 'x': ['1']})
  >>> set_row_numbers_as_index(df, inplace=True)
     x
  2  1
  >>> df = pd.DataFrame({'unnamed: 1': ['a'], 'x': ['1']})
  >>> set_row_numbers_as_index(df)
  Traceback (most recent call last):
    ...
  ValueError: ...
  """
  if df.columns.empty:
    return df
  name = df.columns[0]
  if name == '' or (isinstance(name, str) and UNNAMED_COLUMN_REGEX.fullmatch(name)):
    if df[name].isnull().all():
      if inplace:
        df.drop(columns=[name], inplace=inplace)
      else:
        df = df.drop(columns=[name], inplace=inplace)
      return df
    try:
      index = pd.Index(df[name]).astype('Int64', copy=False)
    except (TypeError, ValueError):
      raise ValueError('Non-integer values in row index')
    if index.isnull().any():
      raise ValueError('Missing values in row index')
    if not index.is_unique:
      raise ValueError('Duplicate values in row index')
    index.name = None
    if inplace:
      df.drop(columns=[name], inplace=inplace)
    else:
      df = df.drop(columns=[name], inplace=inplace)
    df.index = index
  return df


def guess_excel_template_version(tables: Iterable[str]) -> int:
  """
  Guess Excel template version based on table (sheet) names.
  """
  is_2020 = [
    fog.migrations.v2020.normalize_wgms_table_name(name) in
    fog.migrations.v2020.TABLE_RENAMES
    for name in tables
  ]
  if any(is_2020):
    return 2020
  is_2021 = [name in fog.migrations.v2021.TABLE_RENAMES for name in tables]
  if any(is_2021):
    return 2021
  return 2024


PANDAS_DTYPES = {
  'integer': 'Int64',
  'number': 'Float64',
  'boolean': 'boolean',
  'string': 'string',
  'year': 'Int64'
}


def get_pandas_dtypes_from_metadata(
  table: Hashable,
  columns: Iterable[Hashable] = None,
  **kwargs: Any
) -> Dict[Hashable, Optional[str]]:
  resources = {
    resource['name']: resource
    for resource in fog.metadata.read(**kwargs)['resources']
  }
  resource = resources[table]
  return {
    field['name']: PANDAS_DTYPES[field['type']]
    for field in resource['schema']['fields']
    if (True if columns is None else field['name'] in columns)
  }


def write_csvs(
  dfs: Dict[str, pd.DataFrame],
  path: Union[str, Path],
  encoding: str = 'utf-8',
  **kwargs
) -> None:
  """
  Write CSV files.
  """
  path = Path(path)
  if not path.exists():
    path.mkdir(parents=True, exist_ok=True)
  elif not path.is_dir():
    raise ValueError('{path} already exists and is not a directory')
  for name, df in dfs.items():
    df.to_csv(path / f'{name}.csv', encoding=encoding, **kwargs)


def write_excel(
  dfs: Dict[str, pd.DataFrame],
  path: Union[str, Path],
  header: bool = True,
  index: bool = False,
  **kwargs
) -> None:
  """
  Write Excel file.
  """
  path = Path(path)
  writer = pd.ExcelWriter(path, engine='xlsxwriter')
  book: xlsxwriter.Workbook = writer.book
  header_format = book.add_format({'bold': True})
  for name, df in dfs.items():
    df.to_excel(
      writer,
      sheet_name=name,
      header=False,
      index=index,
      startrow=1,
      **kwargs
    )
    sheet: xlsxwriter.worksheet.Worksheet = writer.sheets[name]
    if header:
      for i, column in enumerate(df, start=int(index)):
        sheet.write(0, i, column, header_format)
        # Resize column to fit header
        width = max(10, len(str(column)) * 1.25)
        sheet.set_column(i, i, width=width)
    # Hide unused columns
    sheet.set_column(i + 1, 16384 - 1, options={"hidden": 1})
  writer.close()


def write_excel_for_db(
  dfs: Dict[str, pd.DataFrame], path: Union[str, Path]
) -> None:
  write_excel(dfs, path=path, freeze_panes=(1, 0), index=False)


def write_excel_report(
  path: Union[str, Path],
  report: Report,
  input: Dict[str, pd.DataFrame] = None,
  output: Dict[str, pd.DataFrame] = None,
  format_label: dict = {'bold': True},
  format_error: dict = {'bg_color': '#f4cccc'},
  format_warning: dict = {'bg_color': '#fff2cc'},
  format_change: dict = {'bg_color': '#d3d3d3'},
  format_removal: dict = {'bold': True, 'bg_color': '#d3d3d3'}
) -> None:
  """
  Write Excel file with error and change reporting.
  """
  if input is None:
    if report.input is None:
      raise ValueError('Report has no input data')
    input = report.input
  if output is None:
    if report.output is None:
      output = input
    else:
      output = report.output
  # Write to Excel
  book = xlsxwriter.Workbook(path)
  if format_label:
    format_label = book.add_format(format_label)
  if format_error:
    format_error = book.add_format(format_error)
  if format_warning:
    format_warning = book.add_format(format_warning)
  if format_change:
    format_change = book.add_format(format_change)
  if format_removal:
    format_removal = book.add_format(format_removal)
  for table, df in input.items():
    # Load worksheet
    sheet = book.add_worksheet(table)
    # Resize columns to fit header
    for i, column in enumerate(df, start=1):
      width = max(10, len(str(column)) * 1.25)
      sheet.set_column(i, i, width=width)
    # Hide unused columns
    # NOTE: Prevents Excel file from opening if comment is in hidden region
    # sheet.set_column(df.shape[1] + 1, 16384 - 1, options={"hidden": 1})
    # Freeze header and index
    sheet.freeze_panes(1, 1)
    # Build error and warning array
    tags = defaultdict(bool)
    # Filter failures
    report = Report(
      results=[result for result in report.results if result.code == 'fail'],
      target=report.target
    )
    rin, _ = report.project(input)
    for result in rin.results:
      if result.table != table or (result.row is None and result.column is None):
        continue
      rows = result.row or []
      if not isinstance(rows, list):
        rows = [rows]
      columns = result.column or []
      if not isinstance(columns, list):
        columns = [columns]
      if columns:
        for column in columns:
          if not rows:
            # Tag column label
            rc = 0, df.columns.get_loc(column) + 1
            tags[rc] |= result.check.tag is None
            continue
          for row in rows:
            # Tag cell
            rc = df.index.get_loc(row) + 1, df.columns.get_loc(column) + 1
            tags[rc] |= result.check.tag is None
      else:
        for row in rows:
          # Tag row labels
          rc = df.index.get_loc(row) + 1, 0
          tags[rc] |= result.check.tag is None
    # Write cells with formatting
    rdf = None
    if table in output:
      rdf = output[table]
    # Table labels
    if rdf is None:
      sheet.write(*(0, 0), None, format_removal)
    # Row labels
    for i, row in enumerate(df.index, start=1):
      rc = i, 0
      if rc in tags:
        format = format_error if tags[rc] else format_warning
      elif rdf is not None and row not in rdf.index:
        format = format_removal
      else:
        format = format_label
      sheet.write(*rc, row, format)
    # Column labels
    for j, column in enumerate(df.columns, start=1):
      rc = 0, j
      if rc in tags:
        format = format_error if tags[rc] else format_warning
      elif rdf is not None and column not in rdf:
        format = format_removal
      else:
        format = format_label
      sheet.write(*rc, column, format)
    # Cell values
    for i, row in enumerate(df.index, start=1):
      for j, column in enumerate(df.columns, start=1):
        rc = i, j
        value = df.loc[row, column]
        if pd.isnull(value):
          value = ''
        if rc in tags:
          format = format_error if tags[rc] else format_warning
        else:
          format = None
        sheet.write(*rc, value, format)
        # Write change in comment
        is_change = False
        if rdf is not None:
          try:
            ref = rdf.loc[row, column]
            if pd.isnull(ref):
              ref = ''
            if not (
              value == ref or
              # Physically equal (e.g. True == 'True')
              str(value) == str(ref) or
              # Numerically equal (e.g. '1.0' == 1)
              fog.helpers.float_or_str(value) == fog.helpers.float_or_str(ref)
            ):
              is_change = True
          except KeyError:
            pass
        if is_change:
          sheet.write_comment(
            *rc, str(ref), {'font_name': 'Calibri', 'font_size': 11}
          )
  book.close()


def render_report(
  report: Report,
  input: Dict[str, pd.DataFrame] = None,
  order: Dict[str, Sequence[str]] = None,
  max_col_width: int = None
) -> str:
  max_col_width = max_col_width or fog.config.MAX_REPORT_COLUMN_WIDTH

  def _tabulate(df: pd.DataFrame) -> str:
    if order:
      keys = [None]
      for table in order:
        keys.append(table)
        keys.extend([(table, column) for column in order[table]])
        keys.append((table, None))
      def find_index(row: dict) -> Tuple[int, Optional[int]]:
        if 'tag' in row:
          tag = {None: 0, 'warning': 1}[row['tag']]
        else:
          tag = None
        if 'table' in row and row['table']:
          try:
            i = keys.index(row['table'])
          except ValueError:
            # After everything else
            i = None
          if i is not None and 'column' in row and row['column']:
            try:
              i = keys.index((row['table'], row['column']))
            except ValueError:
              # After other columns in table
              i = keys.index((row['table'], None))
        # Before everything else
        else:
          i = 0
        return i, tag

      indices = [find_index(row) for row in df.to_dict(orient='records')]
      df = (
        df.assign(_ia_=[x[0] for x in indices], _ib_=[x[1] for x in indices])
        .sort_values(by=['_ia_', '_ib_'], na_position='last')
        .drop(columns=['_ia_', '_ib_'])
      )
    if 'tag' in df:
      df = (
        df.assign(
          tag=df['tag'].apply(lambda x: (
              colorama.Fore.RED + colorama.Back.RED + 'ERROR' + colorama.Style.RESET_ALL
              if x is None else
              colorama.Fore.YELLOW + colorama.Back.YELLOW + 'warn ' + colorama.Style.RESET_ALL
            )
          )
        ).
        rename(columns={'tag': 'level'})
      )
    if 'check' in df:
      df = df.assign(check=df['check'].astype('string').str.slice(start=6))
    if 'row' in df:
      df = (
        df.assign(row=df['row'].apply(lambda x: len(x) if x else None)).
        rename(columns={'row': 'rows'})
      )
    df = df.applymap(lambda x: '' if x is None else x)
    return df.to_markdown(
      index=False, tablefmt='fancy_grid', maxcolwidths=max_col_width
    )

  kwargs = {}
  # Valid
  kwargs['valid'] = report.valid
  # Counts
  CODES = ['error', 'fail', 'skip', 'pass']
  counts = report.counts
  kwargs['counts'] = _tabulate(pd.DataFrame(
    {code: [counts[code]] for code in CODES if code in counts}
  ))
  # Errors
  results = [result for result in report.results if result.code == 'error']
  if results:
    df = Report(results, target=report.target).to_dataframe(explode=False)
    kwargs['errors'] = _tabulate(df[['table', 'column', 'check', 'message']])
  # Filter failures
  report = Report(
    results=[result for result in report.results if result.code == 'fail'],
    target=report.target
  )
  if input:
    # Print failures not shown in output
    rin, rout = report.project(input)
    # Filter again, since split may have created valid checks
    rin = Report(
      results=[result for result in rin.results if result.code == 'fail'],
      target=rin.target
    )
    rout = Report(
      results=[result for result in rout.results if result.code == 'fail'],
      target=rout.target
    )
  else:
    rin, rout = report, Report([], target=report.target)
  # Table-level failures
  results = [
    result for result in rin.results
    if result.column is None and result.row is None
  ]
  if results:
    df = Report(results, target=report.target).to_dataframe(explode=False)
    df = df.explode('table')
    kwargs['fails_table'] = _tabulate(df[['tag', 'table', 'check', 'message']])
  # Failures (out)
  if rout.results:
    df = rout.to_dataframe(explode=False)
    df = df.explode('table').explode('column')
    kwargs['fails_out'] = _tabulate(df[['tag', 'table', 'column', 'row', 'check', 'message']])
  # Failures (in)
  results = [
    result for result in rin.results
    if not (result.row is None and result.column is None)
  ]
  if results:
    df = Report(results, target=report.target).to_dataframe(explode=False)
    df = df.explode('table').explode('column')
    kwargs['fails_in'] = _tabulate(df[['tag', 'table', 'column', 'row', 'check', 'message']])
  # Render template
  with importlib.resources.path(f'{__package__}.templates', 'report.md.jinja') as path:
    return fog.helpers.render_template(path, data=kwargs)


def render_rows(
  df: pd.DataFrame,
  index: Sequence[Hashable],
  data: Sequence[Hashable],
  max_col_width: int = None
) -> str:
  max_col_width = max_col_width or fog.config.MAX_REPORT_COLUMN_WIDTH
  return (
    df
    .sort_values(index)[[*index, *data]]
    .dropna(how='all', axis=1)
    .applymap(lambda x: '' if pd.isna(x) else x)
    .to_markdown(
      index=False, tablefmt='fancy_grid', maxcolwidths=max_col_width
    )
  )


def write_table_db_diffs(
  path: Union[str, Path],
  dfs: Dict[str, pd.DataFrame],
  dbdfs: Dict[str, pd.DataFrame],
  primary_keys: Dict[str, List[str]],
  backfill_null: bool = False,
  flags: daff.CompareFlags = None
) -> List[str]:
  path = Path(path)
  written = []
  for name, df in dfs.items():
    key = primary_keys[name]
    if not all(col in df for col in key):
      continue
    if name in dbdfs:
      dbdf = dbdfs[name]
    else:
      dbdf = pd.DataFrame(columns=df.columns)
    df = df.dropna(subset=key)
    columns = [col for col in df if col in dbdf]
    other = dbdf[columns].dropna(subset=key)
    df_keys = df[key].astype('string')
    other_keys = other[key].astype('string')
    _, df_rows, other_rows = pd.MultiIndex.from_frame(df_keys).join(
      pd.MultiIndex.from_frame(other_keys), how='left', return_indexers=True
    )
    if df_rows is None:
      new = df
    else:
      if not pd.Index(df_rows).unique:
        raise ValueError(f'db.{name} not unique on {key}')
      new = df.iloc[df_rows]
    if other_rows is None:
      old = other
      old.index = new.index
    else:
      found = other_rows != -1
      old = other.iloc[other_rows[found]]
      old.index = new.index[found]
    # Set key columns as index
    # new = new.set_index(key, drop=True)
    # old = old.set_index(key, drop=True)
    normalize = Schema({
      Column(): Check(lambda s: s.astype('string'), test=False),
      Column(): Check.squeeze_whitespace(),
      Column(): Check.format_integer_read_as_float()
    })
    old = normalize(old).output
    new = normalize(new).output
    if backfill_null:
      new = new.combine_first(old)
    xid = new.index.intersection(old.index)
    xcolumns = list(set(new.columns).intersection(old.columns))
    if (
      (new.loc[xid, xcolumns].fillna('') == old.loc[xid, xcolumns].fillna('')).all(None)
      and len(xid) == len(new)
      and len(xcolumns) == len(new.columns)
    ):
      continue
    # Drop non-key columns that are empty in both tables
    drop = [
      col for col in set(old) & set(new)
      if col not in key and (
        (col in old and old[col].isnull().all()) and
        (col in new and new[col].isnull().all())
      )
    ]
    old.drop(columns=drop, inplace=True)
    new.drop(columns=drop, inplace=True)
    validator.daff.table_diff(
      old, new, path=path / f'{name}.html', flags=flags
    )
    written.append(name)
  return written
