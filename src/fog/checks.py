from __future__ import annotations
import datetime
import re
from typing import (
  Any, Callable, Dict, Hashable, Optional, Sequence, Tuple, TYPE_CHECKING
)

import geoalchemy2  # Needed for sqlalchemy.func.ST_*
import geopandas as gpd
import numpy as np
import pandas as pd
import shapely.validation
import sqlalchemy
import validator
from validator import Column, register_check

import fog.helpers
import fog.parsers
if TYPE_CHECKING:
  import fog.iodb


@register_check(test=False)
def drop_tables(
  dfs: Dict[Hashable, pd.DataFrame], *, tables: Sequence[Hashable]
) -> Dict[Hashable, pd.DataFrame]:
  return {table: dfs[table] for table in dfs if table not in tables}


@register_check(test=False)
def drop_empty_tables(
  dfs: Dict[Hashable, pd.DataFrame]
) -> Dict[Hashable, pd.DataFrame]:
  return {table: dfs[table] for table in dfs if not dfs[table].empty}


@register_check(test=False)
def drop_columns(df: pd.DataFrame, *, columns: Sequence[Hashable]) -> pd.DataFrame:
  """Drop columns in place."""
  return df.drop(columns=columns, errors='ignore', inplace=True)


@register_check(test=False)
def drop_columns_whose_name_match_regex(
  df: pd.DataFrame, *, regex: str, case: bool = True
) -> Optional[pd.DataFrame]:
  flags = 0 if case else re.IGNORECASE
  dropped = [column for column in df if re.fullmatch(regex, str(column), flags)]
  if dropped:
    return df.drop(columns=dropped)


@register_check(test=False)
def squeeze_whitespace(s: pd.Series) -> pd.Series:
  if str(s.dtype) != 'string':
    return s
  s = s.str.strip().str.replace(r'\s+', ' ', regex=True)
  # Replace empty string with null
  # Modify inplace since already a copy
  s.mask(s == '', other=pd.NA, inplace=True)
  return s


@register_check(test=False)
def uppercase(s: pd.Series) -> Optional[pd.Series]:
  """Convert string column to uppercase."""
  if str(s.dtype) != 'string':
    return None
  return s.str.upper()


@register_check(test=False)
def lowercase(s: pd.Series) -> Optional[pd.Series]:
  """Convert string column to lowercase."""
  if str(s.dtype) != 'string':
    return None
  return s.str.lower()


@register_check(message='Value in interval (0, 1)')
def not_between_0_and_1(s: pd.Series) -> pd.Series:
  return s.le(0) | s.ge(1)


@register_check(test=False)
def format_date(s: pd.Series) -> pd.Series:
  """
  Format submitted date to variable precision ISO 8601 format.

  Examples
  --------
  >>> s = pd.Series(['2023', '2023XXXX', '202301XX', '20230102'])
  >>> format_date(s)
  0          2023
  1          2023
  2       2023-01
  3    2023-01-02
  dtype: object
  """
  return (
    s.str.replace(r'^([0-9]{4})([0-9]{2}|XX)([0-9]{2}|XX)$', '\\1-\\2-\\3', regex=True)
    .str.replace('^([0-9]{4})-XX-XX$', '\\1', regex=True)
    .str.replace('^([0-9]{4}-[0-9]{2})-XX$', '\\1', regex=True)
  )


@register_check(message='Date does not exist')
def date_exists(s: pd.Series) -> pd.Series:
  """
  Date exists.

  Date is assumed to be a string formatted as yyyy, yyyy-mm, or yyyy-mm-dd.

  Example
  -------
  >>> s = pd.Series(['1986-02-01', '1986-02-31', '1986', '1986-01', '1986-13', '1486-02-01', pd.NA])
  >>> date_exists(s)
  0     True
  1    False
  2     True
  3     True
  4    False
  5     True
  6     <NA>
  dtype: boolean
  """
  # Cannot use pd.to_datetime due to the limited range of pd.Timestamp:
  # https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-timestamp-limits
  def parse_date(s: str) -> bool:
    if pd.isna(s):
      return pd.NA
    pattern = '%Y-%m-%d' if len(s) == 10 else '%Y-%m' if len(s) == 7 else '%Y'
    try:
      datetime.datetime.strptime(s, pattern)
      return True
    except ValueError:
      return False

  return s.apply(parse_date).astype('boolean', copy=False)


@register_check(
  test=False,
  required=lambda date: [Column(date)]
)
def convert_date_unc_to_min_max(
  df: pd.DataFrame,
  *,
  date: Hashable,
  date_min: Hashable,
  date_max: Hashable,
  unc: Hashable = None
) -> pd.DataFrame:
  """
  Convert date and uncertainty to a min-max date range.

  NOTE: Modifies table in place.

  Parameters
  ----------
  df
    Table.
  date
    Name of date column (string with format yyyy, yyyy-mm, or yyyy-mm-dd).
  date_min
    Name of minimum date column.
  date_max
    Name of maximum date column.
  unc
    Name of uncertainty column (number in days).

  Examples
  --------
  >>> df = pd.DataFrame({
  ...   'date': ['2010', '2010-03', '2010-03-02', '2010-03-02', pd.NA],
  ...   'unc': [pd.NA, pd.NA, pd.NA, 1, pd.NA]
  ... })
  >>> convert_date_unc_to_min_max(
  ...   df, date='date', unc='unc', date_min='date_min', date_max='date_max'
  ... )[['date_min', 'date_max']]
       date_min    date_max
  0  2010-01-01  2010-12-31
  1  2010-03-01  2010-03-31
  2  2010-03-02  2010-03-02
  3  2010-03-01  2010-03-03
  4        <NA>        <NA>
  """
  columns = list(df.columns) + [x for x in (date_min, date_max) if x not in df]
  df = df.reindex(columns=columns)
  mask = df[[date_min, date_max]].isnull().all(axis=1)
  minmax = fog.helpers.pd_date_unc_to_min_max(df[mask], date=date, unc=unc)
  df.loc[mask, date_min] = minmax[0].astype('string')
  df.loc[mask, date_max] = minmax[1].astype('string')
  return df


@register_check(message='Value is not null')
def is_null(s: pd.Series) -> pd.Series:
  return s.isnull()


@register_check(
  message='Value is null although {column} is {value}',
  required=lambda column: [Column(column)]
)
def not_null_if_column_equal_to(s: pd.Series, df: pd.DataFrame, *, column: Hashable, value: Any) -> pd.Series:
  return df[column].ne(value) | (df[column].eq(value) & s.notnull())


@register_check(
  message='Value is null although {column} matches {regex}',
  required=lambda column: [Column(column)]
)
def not_null_if_column_matches_regex(s: pd.Series, df: pd.DataFrame, *, column: Hashable, regex: str, case: bool = True) -> pd.Series:
  matches = df[column].str.match(regex, case=case)
  return ~matches | (matches & s.notnull())

@register_check(
  message='Value is not null although {column} matches {regex}',
  required=lambda column: [Column(column)]
)
def null_if_column_matches_regex(s: pd.Series, df: pd.DataFrame, *, column: Hashable, regex: str, case: bool = True) -> pd.Series:
  matches = df[column].str.match(regex, case=case)
  return ~matches | (matches & s.isnull())


@register_check(
  message='Value is not null although {column} is null',
  required=lambda column: [Column(column)]
)
def null_if_column_null(s: pd.Series, df: pd.DataFrame, *, column: Hashable) -> pd.Series:
  return df[column].notnull() | (df[column].isnull() & s.isnull())


@register_check(
  message='Value is not null although {columns} are null'
)
def null_if_columns_null(s: pd.Series, df: pd.DataFrame, *, columns: Sequence[Hashable]) -> Optional[pd.Series]:
  columns = [column for column in columns if column in df]
  if not columns:
    return None
  return df[columns].notnull().any(axis=1) | (df[columns].isnull().all(axis=1) & s.isnull())


@register_check(
  message='Value is not null although at least one of {columns} is not null'
)
def null_if_any_columns_not_null(s: pd.Series, df: pd.DataFrame, *, columns: Sequence[Hashable]) -> Optional[pd.Series]:
  columns = [column for column in columns if column in df]
  if not columns:
    return None
  return df[columns].isnull().all(axis=1) | (df[columns].notnull().any(axis=1) & s.isnull())


@register_check(
  message='Columns {columns} are all null',
  required=lambda columns: [Column(column) for column in columns]
)
def columns_not_all_null(
  df: pd.DataFrame, *, columns: Sequence[Hashable]
) -> pd.Series:
  columns = [column for column in columns if column in df]
  return df[columns].notnull().any(axis=1)


@register_check(
  message='Columns {columns} are all null and {column} is not equal to {value}',
  required=lambda columns: [Column(column) for column in columns]
)
def columns_not_all_null_if_column_not_equal_to(
  df: pd.DataFrame, *, columns: Sequence[Hashable], column: Hashable, value: Any
) -> pd.Series:
  columns = [column for column in columns if column in df]
  return df[columns].notnull().any(axis=1) | df[column].eq(value)

@register_check(
  message="Value is 'NULL'"
)
def is_not_null_string(
  s: pd.Series, *, drop: bool = False
) -> Tuple[pd.Series, pd.Series]:
  valid = s.ne('NULL')
  if drop:
    s = s.mask(~valid)
  return valid, s

@register_check(
  message='Value is greater than {column}',
  required=lambda column: [Column(column)]
)
def less_than_or_equal_to_column(s: pd.Series, df: pd.DataFrame, *, column: Hashable) -> pd.Series:
  mask = s.notnull() & df[column].notnull()
  return s[mask].le(df[column][mask])


@register_check(
  message='Value is greater than ||{column}||',
  required=lambda column: [Column(column)]
)
def less_than_or_equal_to_abs_column(s: pd.Series, df: pd.DataFrame, *, column: Hashable) -> pd.Series:
  return s.le(df[column].abs())


@register_check(message='Value is less than or equal to {min}')
def greater_than(s: pd.Series, *, min: Any) -> pd.Series:
  return s.gt(min)


@register_check(message='Value is greater than or equal to {max}')
def less_than(s: pd.Series, *, max: Any) -> pd.Series:
  return s.lt(max)


@register_check(
  message='Value is greater than or equal to {column}',
  required=lambda column: [Column(column)]
)
def less_than_column(s: pd.Series, df: pd.DataFrame, *, column: Hashable) -> pd.Series:
  mask = s.notnull() & df[column].notnull()
  return s[mask].lt(df[column][mask])


@register_check(
  message='{date} is not before {other}',
  required=lambda date, other: [Column(date), Column(other)]
)
def less_than_fuzzydate(
  df: pd.DataFrame,
  *,
  date: Hashable,
  other: Hashable,
  date_unc: Hashable = None,
  other_unc: Hashable = None
) -> pd.Series:
  """
  Fuzzy date is before other fuzzy date.

  Example
  -------
  >>> df = pd.DataFrame([
  ...   # valid
  ...   {'x': '2010', 'xu': pd.NA, 'y': '2010', 'yu': pd.NA},
  ...   {'x': '2010-03', 'xu': pd.NA, 'y': '2010-03', 'yu': pd.NA},
  ...   {'x': '2010-03-01', 'xu': 1, 'y': '2010-03-01', 'yu': 1},
  ...   {'x': '2010-03-01', 'xu': pd.NA, 'y': '2010-03-02', 'yu': pd.NA},
  ...   # invalid
  ...   {'x': '2010-03', 'xu': pd.NA, 'y': '2010', 'yu': pd.NA},
  ...   {'x': '2010-03-01', 'xu': pd.NA, 'y': '2010-03-01', 'yu': pd.NA},
  ...   {'x': '2010-03-01', 'xu': 0, 'y': '2010-03-01', 'yu': 1},
  ... ])
  >>> less_than_fuzzydate(df, date='x', date_unc='xu', other='y', other_unc='yu')
  0     True
  1     True
  2     True
  3     True
  4    False
  5    False
  6    False
  dtype: bool
  """
  # Drop rows with missing date
  df = df.dropna(subset=[date, other], how='any')
  begin = fog.helpers.pd_date_unc_to_min_max(df, date, date_unc)
  end = fog.helpers.pd_date_unc_to_min_max(df, other, other_unc)
  mask = begin.notnull().all(axis=1) & end.notnull().all(axis=1)
  begin = begin[mask]
  end = end[mask]
  begin_fuzzy = begin[0] != begin[1]
  end_fuzzy = end[0] != end[1]
  return (
    (begin_fuzzy & end_fuzzy & begin[0].le(end[0]) & begin[1].le(end[1])) |
    (~begin_fuzzy & ~end_fuzzy & begin[0].lt(end[0])) |
    ((begin_fuzzy | end_fuzzy) & begin[1].le(end[0]))
  )


@register_check(message='Value is equal to {value}')
def not_equal_to(s: pd.Series, *, value: Any) -> pd.Series:
  """
  Column values are not equal to a value.

  Example
  -------
  >>> s = pd.Series([0, 1, pd.NA])
  >>> not_equal_to(s, value=1)
  0     True
  1    False
  2     True
  dtype: bool
  >>> not_equal_to(s.astype('Int64'), value=1)
  0     True
  1    False
  2     <NA>
  dtype: boolean
  """
  return s.ne(value)


@register_check(message='Value and {column} have different sign (+, -, 0)')
def same_sign_as_column(s: pd.Series, df: pd.DataFrame, *, column: Hashable) -> pd.Series:
  """
  Column values have the same sign as values in another column.

  Example
  -------
  >>> df = pd.DataFrame([
  ...   {'x': 1, 'y': 2},
  ...   {'x': -1, 'y': -2},
  ...   {'x': 0, 'y': 0},
  ...   {'x': pd.NA, 'y': 2},
  ...   {'x': 1, 'y': pd.NA},
  ...   {'x': pd.NA, 'y': pd.NA},
  ...   {'x': 1, 'y': -1},
  ...   {'x': 1, 'y': 0},
  ...   {'x': -1, 'y': 1},
  ... ])
  >>> same_sign_as_column(df['x'], df, column='y')
  0     True
  1     True
  2     True
  6    False
  7    False
  8    False
  dtype: bool
  """
  mask = s.notnull() & df[column].notnull()
  return (
    (s[mask].eq(0) & df[column][mask].eq(0)) |
    (s[mask].gt(0) & df[column][mask].gt(0)) |
    (s[mask].lt(0) & df[column][mask].lt(0))
  )


@register_check(message='Value has fewer than {min} decimal places')
def minimum_decimals(s: pd.Series, *, min: int) -> pd.Series:
  """
  Number (or a string representation) has a minimum number of decimal places.

  Numbers are first converted to string to support floating point.
  Trailing zeros are ignored.

  Example
  -------
  >>> s = pd.Series([pd.NA, 0, 0.0, 0.1, -0.12])
  >>> minimum_decimals(s, min=1)
  0     <NA>
  1    False
  2    False
  3     True
  4     True
  dtype: boolean
  >>> s = pd.Series([pd.NA, '0', '0.0', '0.1', '-0.12', '0.a'])
  >>> minimum_decimals(s, min=1)
  0     <NA>
  1    False
  2    False
  3     True
  4     True
  5     <NA>
  dtype: boolean
  """
  if str(s.dtype) != 'string':
    s = s.astype('string')
  groups = s.str.extract(
    r'^-?(?P<integer>[0-9]*)(?:\.(?P<fraction>[0-9]*?)0*)?$'
  )
  decimals = groups['fraction'].str.len()
  decimals[groups['fraction'].isnull() & groups['integer'].notnull()] = 0
  decimals.name = s.name
  return decimals >= min


@register_check(
  message='Not within {round(100 * tolerance)}% of product of {columns}',
  required=lambda columns: [Column(name) for name in columns]
)
def close_to_product_of_columns(
  s: pd.DataFrame,
  df: pd.DataFrame,
  *,
  columns: Sequence[Hashable],
  tolerance: float = 0.0
) -> pd.Series:
  """
  Volume change is consistent with elevation change and area.

  `volume_change = elevation_change * area`

  Example
  -------
  >>> df = pd.DataFrame([
  ...   {'area': 1, 'height': 2, 'volume': 2},
  ...   {'area': 1, 'height': 2, 'volume': pd.NA},
  ...   {'area': 1, 'height': pd.NA, 'volume': 2},
  ...   {'area': pd.NA, 'height': 2, 'volume': 2},
  ...   {'area': 1, 'height': 2, 'volume': 3},
  ... ])
  >>> close_to_product_of_columns(
  ...   df['volume'], df, columns=['area', 'height'], tolerance=0.15
  ... )
  0    True
  1    <NA>
  2    <NA>
  3    <NA>
  4   False
  dtype: boolean
  """
  actual = s
  # Convert to Float64 to ensure <NA> propagation
  predicted = df[columns].product(axis=1, skipna=False).astype('Float64', copy=False)
  deviation = abs((actual - predicted) / actual).astype('Float64', copy=False)
  return deviation < abs(tolerance)


@register_check(
  message='Value not within {round(100 * tolerance)}% of sum of {columns}',
  required=lambda columns: [Column(name) for name in columns]
)
def close_to_sum_of_columns(s: pd.Series, df: pd.DataFrame, *, columns: Sequence[Hashable], tolerance: float = 0.0) -> pd.Series:
  actual = s
  predicted = df[columns].sum(axis=1, skipna=False).astype('Float64', copy=False)
  is_zero = actual.eq(0)
  valid = is_zero & predicted.eq(0)
  deviation = abs(
    (actual[~is_zero] - predicted[~is_zero]) / actual[~is_zero]
  ).astype('Float64', copy=False)
  valid[~is_zero] = deviation < abs(tolerance)
  return valid


@register_check(
  message=(
    'Value not within {round(100 * tolerance)}% of AAR calculated from {acc} and {abl}'
  ),
  required=lambda acc, abl: [Column(acc), Column(abl)]
)
def close_to_acc_area_ratio(
  s: pd.Series,
  df: pd.DataFrame,
  *,
  acc: Hashable,
  abl: Hashable,
  tolerance: float = 0.0
) -> pd.Series:
  """
  Example
  -------
  >>> df = pd.DataFrame({
  ...   'aar': pd.Series([0.01, 0.49, 0, 0.5, pd.NA, 0.5]),
  ...   'acc': pd.Series([0., 1, 0, 1, 1, 1]),
  ...   'abl': pd.Series([1., 1, 1, 1, 1, pd.NA])
  ... }).convert_dtypes()
  >>> valid = close_to_acc_area_ratio(df['aar'], df, acc='acc', abl='abl')
  >>> df[~valid]
       aar  acc   abl
  0   0.01    0   1.0
  1   0.49    1   1.0
  >>> valid = close_to_acc_area_ratio(df['aar'], df, acc='acc', abl='abl', tolerance=0.05)
  >>> df[~valid]
       aar  acc   abl
  0   0.01    0   1.0
  """
  valid = pd.Series(dtype='boolean', index=s.index)
  # If accumulation area = 0, require AAR = zero or null
  is_zero = df[acc].eq(0)
  valid[is_zero] = s.eq(0) | s.isnull()
  # Otherwise, check within tolerance
  actual = s
  predicted = df[acc] / (df[abl] + df[acc])
  # Calculate deviation
  deviation = abs((actual - predicted) / actual).astype('Float64', copy=False)
  valid[~is_zero] = (deviation <= abs(tolerance))[~is_zero]
  return valid


# ---- Elevation bands ----

def transform_group_test(
  df: pd.DataFrame, *args, fn: Callable, **kwargs
) -> pd.Series:
  grouped = df.groupby(*args, **kwargs)
  results = [
    pd.Series(fn(gdf), dtype='boolean', index=gdf.index)
    for _, gdf in grouped
  ]
  # Avoid ValueError: No objects to concatenate
  if results:
    merged = pd.concat(results)
    return merged[df.index.intersection(merged.index, sort=False)]
  return pd.Series(dtype='boolean')


@register_check(
  message='Not within {round(100 * tolerance)}% of {table}.{column} sum (weighted by: {weights})',
  required=(
    lambda table, by, column, bounds, weights:
      [Column(name) for name in by] +
      [Column(name, table=table) for name in [*by.values(), *bounds, column]] +
      ([] if weights is None else [Column(weights, table=table)])
  )
)
def close_to_band_sum(
  s: pd.Series,
  df: pd.DataFrame,
  dfs: Dict[Hashable, pd.DataFrame],
  *,
  table: Hashable,
  column: Hashable,
  by: Dict[Hashable, Hashable],
  bounds: Tuple[Hashable, Hashable],
  weights: Hashable = None,
  tolerance: float = 0.0
) -> pd.Series:
  """
  Value is consistent with sum of elevation band values.

  Example
  -------
  >>> glacier = pd.DataFrame({
  ...   'glacier': [1, 1, 2],
  ...   'year': [1980, 2000, 1980],
  ...   'area': [4, 4, 4],
  ...   'balance': [0.2, 0.2, 0.2]
  ... }).convert_dtypes()
  >>> band = pd.DataFrame({
  ...   'glacier': [1, 1, 2, 2, 1, 1, 2, 3, 3],
  ...   'year': [1980, 1980, 1980, 1980, 2000, 2000, pd.NA, 1980, 1980],
  ...   'lower': [0, 10, 0, 10, 0, 10, pd.NA, 0, 10],
  ...   'upper': [10, 40, 10, 40, 10, pd.NA, pd.NA, 10, 40],
  ...   'area': [1, 3, 1, 4, 1, 3, pd.NA, 1, 3],
  ...   'balance': [-3, 1, -3, 1, -3, 1, pd.NA, -3, 1]
  ... }).convert_dtypes()
  >>> dfs = {'glacier': glacier, 'band': band}
  >>> close_to_band_sum(
  ...   glacier['area'], glacier, dfs, table='band', column='area',
  ...   by={'glacier': 'glacier', 'year': 'year'}, bounds=('lower', 'upper')
  ... )
  0     True
  2    False
  dtype: boolean
  >>> close_to_band_sum(
  ...   glacier['balance'], glacier, dfs, table='band', column='balance',
  ...   by={'glacier': 'glacier', 'year': 'year'}, bounds=('lower', 'upper'),
  ...   weights='area'
  ... )
  0    False
  2     True
  dtype: boolean
  """
  bands = dfs[table]
  key = list(by.values())
  # Ignore groups with missing values in column, weights, or bounds
  columns = [*by.values(), *bounds, column]
  if weights is not None:
    columns.append(weights)
  is_complete_row = bands[columns].notnull().all(axis=1)
  group_index = pd.MultiIndex.from_frame(bands[key])
  is_complete_group = is_complete_row.groupby(group_index).transform('all')
  bands = bands[is_complete_group]
  # Estimate glacier-wide value
  group_index = pd.MultiIndex.from_frame(bands[key])
  if weights is None:
    predicted = bands[column].groupby(group_index).sum()
  else:
    predicted = (
      (bands[column] * bands[weights]).groupby(group_index).sum() /
      bands[weights].groupby(group_index).sum()
    )
  # Align with glacier-wide value
  index = pd.MultiIndex.from_frame(df[list(by)])
  mask = index.isin(predicted.index)
  actual = s[mask]
  predicted = predicted.loc[index[mask]]
  predicted.index = actual.index
  # Compare to glacier-wide value
  deviation = abs((actual - predicted) / actual).astype('Float64', copy=False)
  deviation.name = None
  return deviation <= abs(tolerance)


@register_check(
  message='Not within range of {table}.{column}',
  required=(
    lambda table, by, column:
      [Column(name) for name in by] +
      [Column(name, table=table) for name in [*by.values(), column]]
  )
)
def within_band_range(
  s: pd.Series,
  df: pd.DataFrame,
  dfs: Dict[Hashable, pd.DataFrame],
  *,
  table: Hashable,
  column: Hashable,
  by: Dict[Hashable, Hashable]
) -> pd.Series:
  """
  Value is consistent with range of elevation band values.

  Example
  -------
  >>> glacier = pd.DataFrame({
  ...   'group': [1, 2, 4],
  ...   'value': [1, 3, 3],
  ... }).convert_dtypes()
  >>> band = pd.DataFrame({
  ...   'group': [1, 1, 2, 2, 3],
  ...   'value': [0, 2, 0, 2, 0],
  ... }).convert_dtypes()
  >>> dfs = {'glacier': glacier, 'band': band}
  >>> within_band_range(
  ...   glacier['value'], glacier, dfs, table='band', column='value',
  ...   by={'group': 'group'}
  ... )
  0     True
  1    False
  dtype: boolean
  """
  bands = dfs[table]
  key = list(by.values())
  # Ignore groups with missing key and rows with missing values
  columns = key
  is_complete_row = bands[columns].notnull().all(axis=1)
  group_index = pd.MultiIndex.from_frame(bands[key])
  is_complete_group = is_complete_row.groupby(group_index).transform('all')
  bands = bands[is_complete_group & bands[column].notnull()]
  # Estimate glacier-wide value
  group_index = pd.MultiIndex.from_frame(bands[key])
  predicted = bands[column].groupby(group_index).agg(['min', 'max'])
  # Align with glacier-wide value
  index = pd.MultiIndex.from_frame(df[list(by)])
  mask = index.isin(predicted.index)
  actual = s[mask]
  predicted = predicted.loc[index[mask]]
  predicted.index = actual.index
  # Compare to glacier-wide value
  return (
    actual.isnull() |
    (actual.ge(predicted['min']) & actual.le(predicted['max']))
  )


@register_check(
  message='Not within range of {table}.{bounds}',
  required=(
    lambda table, by, bounds:
      [Column(name) for name in by] +
      [Column(name, table=table) for name in [*by.values(), *bounds]]
  )
)
def within_band_bounds(
  s: pd.Series,
  df: pd.DataFrame,
  dfs: Dict[Hashable, pd.DataFrame],
  *,
  table: Hashable,
  by: Dict[Hashable, Hashable],
  bounds: Tuple[Hashable, Hashable]
) -> pd.Series:
  """
  Value is within range of elevation band bounds.

  Example
  -------
  >>> glacier = pd.DataFrame({
  ...   'group': [1, 2, 4],
  ...   'value': [1, 5, 3]
  ... }).convert_dtypes()
  >>> band = pd.DataFrame({
  ...   'group': [1, 1, 2, 2, 3],
  ...   'lower': [0, 2, 0, 2, 0],
  ...   'upper': [2, 4, 2, 4, 2]
  ... }).convert_dtypes()
  >>> dfs = {'glacier': glacier, 'band': band}
  >>> within_band_bounds(
  ...   glacier['value'], glacier, dfs, table='band', bounds=('lower', 'upper'),
  ...   by={'group': 'group'}
  ... )
  0     True
  1    False
  dtype: boolean
  """
  bands = dfs[table]
  key = list(by.values())
  # Ignore groups with missing key
  columns = key
  is_complete_row = bands[columns].notnull().all(axis=1)
  group_index = pd.MultiIndex.from_frame(bands[key])
  is_complete_group = is_complete_row.groupby(group_index).transform('all')
  bands = bands[is_complete_group]
  # Estimate glacier-wide value
  group_index = pd.MultiIndex.from_frame(bands[key])
  predicted = bands[list(bounds)].groupby(group_index).agg(['min', 'max'])
  predicted = pd.DataFrame({
    'min': predicted.min(axis=1),
    'max': predicted.max(axis=1)
  })
  # Align with glacier-wide value
  index = pd.MultiIndex.from_frame(df[list(by)])
  mask = index.isin(predicted.index)
  actual = s[mask]
  predicted = predicted.loc[index[mask]]
  predicted.index = actual.index
  # Compare to glacier-wide value
  return (
    actual.isnull() |
    (actual.ge(predicted['min']) & actual.le(predicted['max']))
  )


@register_check(
  message='Elevation band bounds overlap',
  required=lambda by, bounds: [Column(name) for name in [*by, *bounds]]
)
def bounds_do_not_overlap(
  df: pd.DataFrame, *, bounds: Tuple[Hashable, Hashable], by: Sequence[Hashable]
) -> Optional[pd.Series]:
  """
  Elevation band bounds do not overlap.

  Example
  -------
  >>> df = pd.DataFrame({
  ...   'group': [1, 1, 2, 2, 3],
  ...   'lower': [0, 1, 0, 2, 0],
  ...   'upper': [2, 4, 2, 4, 2]
  ... }).convert_dtypes()
  >>> bounds_do_not_overlap(df, bounds=('lower', 'upper'), by=['group'])
  0    False
  1    False
  dtype: boolean
  """
  # Revise the following to treat each group separately
  def fn(gdf: pd.DataFrame):
    indices = list(fog.helpers.find_overlapping_ranges(gdf[list(bounds)]))
    if indices:
      # Flatten list of lists
      indices = [item for sublist in indices for item in sublist]
      return pd.Series(False, dtype='boolean', index=indices)
    return pd.Series(dtype='boolean')

  results = [
    fn(gdf) for _, gdf in df.groupby(by, as_index=False, dropna=True, sort=False)
  ]
  if results:
    return pd.concat(results, ignore_index=False, axis=0)


@register_check(
  message='Elevation band bounds contain gaps',
  required=lambda by, bounds: [Column(name) for name in [*by, *bounds]]
)
def bounds_are_continuous(
  df: pd.DataFrame, *, bounds: Tuple[Hashable, Hashable], by: Sequence[Hashable]
) -> Optional[pd.Series]:
  """
  Elevation band bounds do not overlap.

  Example
  -------
  >>> df = pd.DataFrame({
  ...   'group': [1, 1, 2, 2, 2],
  ...   'lower': [0, 1, 0, 2, 5],
  ...   'upper': [2, 4, 2, 4, 6]
  ... }).convert_dtypes()
  >>> bounds_are_continuous(df, bounds=('lower', 'upper'), by=['group'])
  3    False
  4    False
  dtype: boolean
  """
  # Revise the following to treat each group separately
  def fn(gdf: pd.DataFrame):
    indices = list(fog.helpers.find_gaps_between_ranges(gdf[list(bounds)]))
    if indices:
      # Flatten list of lists
      indices = [item for sublist in indices for item in sublist]
      return pd.Series(False, dtype='boolean', index=indices)
    return pd.Series(dtype='boolean')

  results = [
    fn(gdf) for _, gdf in df.groupby(by, as_index=False, dropna=True, sort=False)
  ]
  if results:
    return pd.concat(results, ignore_index=False, axis=0)


@register_check(test=False)
def format_date_read_as_date(s: pd.Series) -> pd.Series:
  """
  Format dates read as dates to the expected format.

  Dates are stored in Excel as floating-point numbers, and only displayed as a
  date based on the cell format. When saving to a CSV from Excel, the displayed string is written to file.
  But it is very difficult to reconstruct this string when reading the Excel
  file directly from Python (https://stackoverflow.com/a/55813901).

  A compromise is to parse date-formatted numbers as dates (default behavior)
  and convert these back to strings (`pd.read_excel(dtype='string')`), then
  format these strings as needed by later checks.

  Example
  -------
  >>> s = pd.Series(['2019-09-19 00:00:00', '2019-09-19', pd.NA], dtype='string')
  >>> format_date_read_as_date(s)
  0    2019-09-19
  1    2019-09-19
  2          <NA>
  dtype: string
  """
  return s.str.replace(
    pat=r'^([0-9]{4})-([0-9]{2})-([0-9]{2}) 00:00:00$',
    repl=r'\1-\2-\3',
    regex=True
  )


@register_check(test=False)
def format_integer_read_as_float(s: pd.Series) -> pd.Series:
  """
  Strip zero-decimal from integers read as floats before conversion to string.

  Example
  -------
  >>> s = pd.Series(['1', '1.', '1.0', '-1.00', pd.NA], dtype='string')
  >>> format_integer_read_as_float(s)
  0       1
  1       1
  2       1
  3      -1
  4    <NA>
  dtype: string
  """
  return s.str.replace(pat=r'^(-?)([0-9]+)\.0*$', repl=r'\1\2', regex=True)


@register_check(
  message='Inconsistent with {column}',
  required=lambda column: [Column(column)]
)
def consistent_with_quantitative_variation(
  s: pd.Series, df: pd.DataFrame, *, column: Hashable
) -> pd.Series:
  """
  Check that qualitative variation is consistent with quantitative variation.

  NOTE: Expects numeric quantitative variation.

  Example
  -------
  >>> df = pd.DataFrame(
  ...   [(pd.NA, 0), ('stationary', 0), ('advance', 1), ('retreat', -1), ('advance', 0)],
  ...   columns=['code', 'value']
  ... )
  >>> consistent_with_quantitative_variation(df['code'], df, column='value')
  0     True
  1     True
  2     True
  3     True
  4    False
  dtype: bool
  """
  x = df[column]
  return (
    s.isnull() |
    (s.eq('stationary') & x.eq(0)) |
    (s.eq('advance') & x.gt(0)) |
    (s.eq('retreat') & x.lt(0))
  )


@register_check(
  message='Does not match {table}.{foreign}',
  required=lambda table, on, local, foreign: [
    Column(on[0]),
    Column(on[1], table=table),
    Column(local),
    Column(foreign, table=table)
  ]
)
def lookup_column(
  # HACK: Use a column check to generate a column-level error
  s: pd.Series,
  df: pd.DataFrame,
  dfs: Dict[Hashable, pd.DataFrame],
  *,
  table: Hashable,
  on: Tuple[Hashable, Hashable],
  local: Hashable,
  foreign: Hashable
) -> Optional[pd.Series]:
  mask = df[on[0]].notnull()
  return validator.checks.table.in_foreign_columns(
    df[mask],
    dfs,
    table=table,
    columns={on[0]: on[1], local: foreign}
  )


@register_check(
  message=(
    'Does not match {glacier["table"]}.{glacier["name"]} (if provided), ' +
    'or {glacier["table"]}.{glacier["short_name"]} ' +
    'or {name["table"]}.{name["name"]} (otherwise)'
  ),
  required=lambda glacier_id: [Column(glacier_id)]
)
def lookup_glacier_name(
  s: pd.Series,
  df: pd.DataFrame,
  dfs: Dict[Hashable, pd.DataFrame],
  *,
  glacier_id: Hashable,
  glacier: dict[str, Hashable],
  name: dict[str, Hashable],
) -> pd.Series:
  """
  Lookup submitted glacier name.

  Matching is case-insensitive.

  Parameters
  ----------
  glacier_id
    Name of column with glacier ID.
  glacier
    Glacier table and column names (table, id, name, short_name).
  name
    Glacier name table and column names (table, glacier_id, name).
  """
  mask = s.notnull() & df[glacier_id].notnull()
  index = pd.MultiIndex.from_arrays((
    df[mask][glacier_id], df[mask][s.name].str.lower()
  ))
  valid = pd.Series(False, index=s[mask].index)
  # Find which glaciers have a glacier.name
  if (
    glacier['table'] in dfs and
    glacier['id'] in dfs[glacier['table']]
  ):
    glacier_table = dfs[glacier['table']]
    glacier_has_name = pd.Series(False, index=glacier_table.index)
    if glacier['name'] in dfs[glacier['table']]:
      glacier_has_name = (
        glacier_table[glacier['name']]
        .notnull()
        .groupby(glacier_table[glacier['id']])
        .transform('any')
      ).fillna(False)
      # Match name to (glacier.id, glacier.name)
      name_index = pd.MultiIndex.from_arrays((
        glacier_table[glacier_has_name][glacier['id']],
        glacier_table[glacier_has_name][glacier['name']].str.lower()
      ))
      valid |= index.isin(name_index)
    # If glacier.name is null
    # Find in (glacier.id, glacier.short_name)
    if glacier['short_name'] in glacier_table:
      name_index = pd.MultiIndex.from_arrays((
        glacier_table[~glacier_has_name][glacier['id']],
        glacier_table[~glacier_has_name][glacier['short_name']].str.lower()
      ))
      valid |= index.isin(name_index)
  # Find in (glacier_name.glacier_id, glacier_name.name)
  if (
    name['table'] in dfs and
    name['name'] in dfs[name['table']] and
    name['glacier_id'] in dfs[name['table']]
  ):
    name_index = pd.MultiIndex.from_arrays((
      dfs[name['table']][name['glacier_id']],
      dfs[name['table']][name['name']].str.lower()
    ))
    valid |= index.isin(name_index)
  return valid


@register_check(
  message=(
    'Not found in column {glacier["short_name"]} or table {name["table"]}. ' +
    'Maybe you need to add it to table {name["table"]}?'
  ),
  required=lambda glacier: [Column(glacier['id'])]
)
def glacier_name_exists(
  s: pd.Series,
  df: pd.DataFrame,
  dfs: Dict[Hashable, pd.DataFrame],
  *,
  glacier: dict[str, Hashable],
  name: dict[str, Hashable]
) -> pd.Series:
  """
  Check if glacier.name exists (in glacier.short_name or glacier_name.name).

  Matching is case-insensitive.
  """
  mask = s.notnull() & df[glacier['id']].notnull()
  df = df[mask]
  valid = pd.Series(False, index=df.index)
  # Valid if matches glacier.short_name
  if glacier['short_name'] in df:
    valid |= s.str.lower().eq(df[glacier['short_name']].str.lower())
  # Valid if matches glacier_name.name on glacier.id
  if name['table'] in dfs:
    name_index = pd.MultiIndex.from_arrays((
      dfs[name['table']][name['glacier_id']],
      dfs[name['table']][name['name']].str.lower()
    ))
    valid |= pd.MultiIndex.from_arrays((
      df[glacier['id']], df[s.name].str.lower()
    )).isin(name_index)
  return pd.Series(valid, index=s[mask].index)


@register_check(
  test=False,
  required=lambda glacier: [
    Column(glacier['id'], table=glacier['table']),
    Column(glacier['name'], table=glacier['table']),
  ]
)
def tabulate_glacier_names(
  dfs: dict[Hashable, pd.DataFrame],
  *,
  glacier: dict[str, Hashable],
  name: dict[str, Hashable]
) -> dict[Hashable, pd.DataFrame]:
  """
  Tabulate glacier names.

  Parameters
  ----------
  dfs
    DataFrames.
  glacier
    Table name and name of columns containing the glacier id, name, and short name.
  name
    Table name and name of columns containing the glacier id and name.
  """
  # Extract unique new glacier id, name combinations
  gdf = dfs[glacier['table']]
  mask = gdf[glacier['id']].notnull() & gdf[glacier['name']].notnull()
  if glacier['short_name'] in gdf:
    mask &= (
      gdf[glacier['short_name']].isnull()
      | gdf[glacier['name']].ne(gdf[glacier['short_name']])
    )
  gdf = gdf[mask][[glacier['id'], glacier['name']]].drop_duplicates()
  # Drop any already present in the glacier name table
  if name['table'] in dfs:
    ndf = dfs[name['table']]
    name_index = pd.MultiIndex.from_frame(ndf[[name['id'], name['name']]])
    glacier_index = pd.MultiIndex.from_frame(gdf)
    gdf = gdf[~glacier_index.isin(name_index)]
    gdf = pd.concat([ndf, gdf], ignore_index=True)
  dfs[name['table']] = gdf
  return dfs


@register_check(
  message='Not found in database column {table}.{column}'
)
def in_db_column(
  s: pd.Series,
  *,
  db: fog.iodb.DB,
  table: str,
  column: str,
  where_equals: tuple[str, str | int | float | bool] = None,
) -> pd.Series:
  if where_equals:
    where = db.metadata.tables[table].columns[where_equals[0]] == where_equals[1]
  else:
    where = None
  mask = s.notnull()
  return db.is_in_column(
    s=s[mask],
    table=table,
    column=column,
    where=where
  )

@register_check(
  message='Not found in RGI 7.0 (glacier or complex)'
)
def in_db_column_rgi7_id(
  s: pd.Series,
  *,
  db: fog.iodb.DB,
  glacier_table: str,
  complex_table: str,
  column: str
) -> Optional[pd.Series]:
  mask = s.notnull()
  return (
    db.is_in_column(s[mask], table=glacier_table, column=column) |
    db.is_in_column(s[mask], table=complex_table, column=column)
  )


@register_check(
  message='Country does not contain point {coordinates}',
  required=lambda coordinates: [Column(name) for name in coordinates]
)
def country_contains_point(
  s: pd.Series,
  df: pd.DataFrame,
  *,
  coordinates: tuple[Hashable, Hashable],
  db: fog.iodb.DB
) -> Optional[pd.Series]:
  columns = [s.name, *coordinates]
  # Determine which rows to check
  mask = df[columns].notnull().all(axis=1)
  df = df[mask]
  if df.empty:
    return None
  # Build query
  # select st_contains(country.polygon, st_point(x.longitude, x.latitude, 4326))
  # from (values ({name}, {latitude}, {longitude}), ...) as x (name, latitude, longitude)
  # left join country on country.name = x.name;
  table = db.metadata.tables['country']
  values = sqlalchemy.sql.values(
    sqlalchemy.column('name'),
    sqlalchemy.column('latitude'),
    sqlalchemy.column('longitude'),
  ).data(df[columns].values).alias('x')
  query = sqlalchemy.select(
    sqlalchemy.func.ST_Contains(
      table.c['polygon'],
      sqlalchemy.func.ST_Point(
        values.c['longitude'],
        values.c['latitude'],
        4326
      )
    ).label('contains')
  ).select_from(
    values.join(table, table.c['name'] == values.c['name'], isouter=True)
  )
  result = db.execute(query)
  result_df = db.format_result_as_df(result)
  return pd.Series(result_df['contains'].values, index=df.index)


@register_check(
  test=False
)
def increment_id_from_db(
  df: pd.DataFrame,
  *,
  id: Hashable,
  table: Hashable,
  column: Hashable,
  db: fog.iodb.DB
) -> pd.Series:
  start = db.get_next_integer(table, column)
  return fog.helpers.fill_and_increment_id(df, column=id, start=start)

# @register_check(
#   message='Not within min-max dates (or ± days is not null)',
#   required=lambda min, max: [Column(name) for name in [min, max]]
# )
# def date_without_unc_and_within_min_max_if_min_max_not_null(
#   s: pd.Series,
#   df: pd.DataFrame,
#   *,
#   unc: Hashable,
#   min: Hashable,
#   max: Hashable
# ) -> pd.Series:
#   mask =

@register_check(
  required=lambda glacier, band: [
    Column(glacier['glacier_id'], table=glacier['table']),
    *[Column(date, table=glacier['table']) for date in glacier['dates']],
    Column(band['glacier_id'], table=band['table']),
    *[Column(date, table=band['table']) for date in band['dates']]
  ],
  test=False
)
def replace_band_dates_with_id(
  dfs: dict[Hashable, pd.DataFrame],
  *,
  glacier: dict[str, Hashable],
  band: dict[str, Hashable]
) -> dict[Hashable, pd.DataFrame]:
  """
  Replace band dates with glacier survey id.

  Parameters
  ----------
  dfs
    DataFrames.
  glacier
    Glacier survey table and column names (table, glacier_id, id, list of dates).
  band
    Band table and column names (table, glacier_id, id, list of dates).

  Example
  -------
  >>> glacier = pd.DataFrame({
  ...   'glacier_id': [1, 1, 2],
  ...   'date': [1980, 2000, 1980],
  ...   'id': [1, 2, 3],
  ... }).convert_dtypes()
  >>> band = pd.DataFrame({
  ...   'glacier_id': [1, 1, 2, 2],
  ...   'date': [1980, 2000, 1980, 1980],
  ...   'lower': [0, 10, 0, 10],
  ...   'upper': [10, 40, 10, 40],
  ... }).convert_dtypes()
  >>> dfs = {'state': glacier, 'band': band}
  >>> dfs = replace_band_dates_with_id(
  ...   dfs,
  ...   glacier={'table': 'state', 'glacier_id': 'glacier_id', 'dates': ['date'], 'id': 'id'},
  ...   band={'table': 'band', 'glacier_id': 'glacier_id', 'dates': ['date'], 'id': 'state_id'}
  ... )
  >>> dfs['band'][['glacier_id', 'lower', 'upper', 'state_id']]
    glacier_id  lower  upper  state_id
  0          1      0     10        1
  1          1     10     40        2
  2          2      0     10        3
  3          2     10     40        3
  """
  # Ignore state rows with equal glacier_id and date but different group id
  gdf = dfs[glacier['table']]
  columns = [glacier['glacier_id'], *glacier['dates']]
  duplicated = gdf.groupby(columns, dropna=False)[glacier['id']].transform('nunique').gt(1)
  gdf = gdf[~duplicated].set_index(columns)
  # Lookup band group id by glacier_id and date
  bdf = dfs[band['table']]
  mask = bdf[band['glacier_id']].notnull() & bdf[band['dates']].notnull().all(axis=1)
  if band['id'] in bdf:
    mask &= bdf[band['id']].isnull()
  else:
    bdf[band['id']] = pd.NA
  index = pd.MultiIndex.from_frame(
    bdf[[band['glacier_id'], *band['dates']]]
  )
  mask &= index.isin(gdf.index)
  bdf.loc[mask, band['id']] = gdf.loc[index[mask], glacier['id']].values
  dfs[band['table']] = bdf
  return dfs


@register_check(
  message='Invalid reference syntax: multiple DOI or URL | spurious [ or ]'
)
def references_syntax_is_valid(s: pd.Series) -> pd.Series:
  """
  Check whether references are syntactically valid.

  Examples
  --------
  >>> s = pd.Series(['[prefix] reference', 'reference [', pd.NA])
  >>> references_syntax_is_valid(s)
  0     True
  1    False
  2     <NA>
  dtype: boolean
  """
  strings = s.dropna().drop_duplicates()
  valid = (
    strings
    .apply(fog.parsers.parse_references)
    .apply(fog.parsers.are_references_valid)
  )
  valid.index = strings
  return s.map(valid).astype('boolean')


@register_check(
  test=False
)
def process_references(
  dfs: dict[Hashable, pd.DataFrame],
  *,
  references: Hashable,
  bibliography_id: Hashable,
  db: fog.iodb.DB
) -> dict[Hashable, pd.DataFrame]:
  """Process references."""
  # Compile all bibliography strings
  tables = [table for table in dfs if references in dfs[table]]
  strings = []
  for table in tables:
    strings.append(dfs[table][references])
  if not strings:
    return dfs
  strings = pd.concat(strings, ignore_index=True).drop_duplicates().dropna()
  if strings.empty:
    return dfs
  # Load tables from database
  bdf = db.select_table('bibliography')
  brdf = db.select_table('bibliography_reference')
  rdf = db.select_table('reference')
  # Find bibliography.id for each string
  bid = strings.map(
    bdf.set_index('original_string')['id']
  ).astype('Int64')
  # Assign new bibliography.id to new strings
  start = bdf['id'].max() + 1
  is_new = bid.isnull()
  bid[is_new] = np.arange(start, start + is_new.sum())
  # Parse references from new strings
  parsed = strings[is_new].apply(fog.parsers.parse_references)
  parsed.index = bid[is_new]
  parsed = parsed.explode().dropna()
  parsed = pd.DataFrame(list(parsed), index=parsed.index).convert_dtypes()
  if parsed.empty:
    return dfs
  # Find reference.id for each reference
  rid = parsed['reference'].map(brdf.groupby('original_substring')['reference_id'].first())
  is_single_doi = parsed['dois'].apply(len).eq(1)
  rid[is_single_doi] = rid[is_single_doi].combine_first(
    parsed['dois'][is_single_doi].apply(lambda x: x[0]).map(
      rdf.query('doi.notnull()').set_index('doi')['id']
    )
  )
  is_single_url = parsed['urls'].apply(len).eq(1)
  rid[is_single_url] = rid[is_single_url].combine_first(
    parsed['urls'][is_single_url].apply(lambda x: x[0]).map(
      rdf.query('url.notnull()').set_index('url')['id']
    )
  )
  # Number references
  parsed['position'] = parsed.groupby(level=0).transform('cumcount') + 1
  # Tabulate new bibliographies
  bibliography_df = pd.DataFrame({
    'id': bid,
    'original_string': strings
  })[is_new]
  if 'bibliography' in dfs:
    dfs['bibliography'] = pd.concat(
      [dfs['bibliography'], bibliography_df], ignore_index=True
    )
  else:
    dfs['bibliography'] = bibliography_df
  dfs['bibliography_reference'] = pd.DataFrame({
    'bibliography_id': parsed.index,
    'reference_id': rid,
    'original_substring': parsed['reference'],
    'position': parsed['position'],
    'prefix': parsed['prefix'],
    'suffix': parsed['suffix']
  }).reset_index(drop=True)
  # Replace references column with bibliography_id column
  for table in tables:
    df = dfs[table]
    mask = df[references].notnull()
    if bibliography_id not in df:
      df[bibliography_id] = pd.Series(dtype='Int64')
    df.loc[mask, bibliography_id] = df.loc[mask, references].map(
      pd.Series(bid.values, index=strings)
    )
  return dfs


@register_check(
  message='Invalid investigators & agencies syntax: missing or duplicate agency # | spurious [( or )] | ...',
)
def investigators_agencies_syntax_is_valid(
  df: pd.DataFrame,
  *,
  investigators: Hashable,
  agencies: Hashable
) -> pd.Series | None:
  """
  Check whether investigators and agencies are syntactically valid.

  Examples
  --------
  >>> df = pd.DataFrame([
  ...   {'investigators': 'Michael Zemp (1)', 'agencies': '1. WGMS'},
  ...   {'investigators': '(1)', 'agencies': '1. WGMS'},
  ...   {'investigators': 'Michael Zemp', 'agencies': pd.NA},
  ...   {'investigators': pd.NA, 'agencies': pd.NA},
  ...   {'investigators': 'Michael Zemp', 'agencies': 'WGMS'},
  ...   {'investigators': 'Michael Zemp', 'agencies': '1. WGMS'},
  ...   {'investigators': 'Michael Zemp (2)', 'agencies': '1. WGMS'},
  ...   {'investigators': pd.NA, 'agencies': '1. WGMS'},
  ... ])
  >>> investigators_agencies_syntax_is_valid(
  ...   df, investigators='investigators', agencies='agencies'
  ... )
  0     True
  1     True
  2     True
  3     True
  4    False
  5    False
  6    False
  7    False
  dtype: bool
  """
  if investigators not in df and agencies not in df:
    return None
  # TODO: Deduplicate before parsing
  # Validate based on null, then mask
  if investigators in df:
    parsed_investigators = df[investigators].apply(
      lambda x: [] if pd.isna(x) else fog.parsers.parse_investigators(x)
    )
  else:
    parsed_investigators = [[]] * len(df)
  if agencies in df:
    parsed_agencies = df[agencies].apply(
      lambda x: [] if pd.isna(x) else fog.parsers.parse_agencies(x)
    )
  else:
    parsed_agencies = [[]] * len(df)
  return pd.Series([
    fog.parsers.are_investigators_agencies_valid(i, a)
    for i, a in zip(parsed_investigators, parsed_agencies)
  ], index=df.index)


@register_check(
  test=False
)
def process_investigators_agencies(
  dfs: dict[Hashable, pd.DataFrame],
  *,
  investigators: Hashable,
  agencies: Hashable,
  team_id: Hashable,
  db: fog.iodb.DB
) -> Optional[dict[Hashable, pd.DataFrame]]:
  """Process investigators and agencies."""
  # Compile all combinations of investigators and agencies
  tables = [
    table for table in dfs
    if (investigators in dfs[table] or agencies in dfs[table])
    # HACK: Exclude team table if present
    and table != 'team'
  ]
  strings = []
  for table in tables:
    columns = [column for column in (investigators, agencies) if column in dfs[table]]
    strings.append(dfs[table][columns])
  if not strings:
    return None
  strings = (
    pd.concat(strings, ignore_index=True)
    .drop_duplicates()
    .dropna(how='all').reindex(columns=[investigators, agencies])
    .reset_index(drop=True)
  )
  if strings.empty:
    return None
  # Load tables from database
  tdf = db.select_table('team').query('~coop')
  pdf = db.select_table('person')
  adf = db.select_table('agency')
  # Find team.id for each string pair
  index = pd.MultiIndex.from_frame(strings)
  tid = (
    index.map(tdf.set_index(['investigators', 'agencies'])['id'])
    .to_series()
    .reset_index(drop=True)
  )
  # Assign new team.id to new string pairs
  start = tdf['id'].max() + 1
  is_new = tid.isnull()
  tid[is_new] = np.arange(start, start + is_new.sum())
  # Parse investigators from new strings
  parsed = strings[is_new][investigators].apply(
    lambda x: pd.NA if pd.isna(x) else fog.parsers.parse_investigators(x)
  )
  parsed.index = tid[is_new]
  parsed = parsed.explode()
  parsed.dropna(inplace=True)
  if parsed.empty:
    return None
  parsed_investigators = pd.DataFrame(list(parsed), index=parsed.index).convert_dtypes()
  # Parse agencies from new strings
  parsed = strings[is_new][agencies].apply(
    lambda x: pd.NA if pd.isna(x) else fog.parsers.parse_agencies(x)
  )
  parsed.index = tid[is_new]
  parsed = parsed.explode()
  parsed.dropna(inplace=True)
  parsed_agencies = pd.DataFrame(list(parsed), index=parsed.index).convert_dtypes()
  # Find investigator.id for each investigator
  # NOTE: Assumes a person can be uniquely identified by orcid, name, or synonym
  pid = pd.Series(dtype='Int64', index=parsed_investigators.index)
  # Convert person and agency table columns to lowercase
  for df in (pdf, adf):
    for column in df.select_dtypes(include='string'):
      df[column] = df[column].str.lower()
  pdf['synonyms'] = pdf['synonyms'].str.split(' | ', regex=False)
  # orcid
  if 'orcid' in parsed_investigators:
    pid = pid.combine_first(
      parsed_investigators['orcid'].str.lower().map(
        pdf.query('orcid.notnull()').set_index('orcid')['id']
      )
    )
  # name, english_name
  for column in ('name', 'english_name'):
    if column in parsed_investigators:
      pid = pid.combine_first(
        parsed_investigators[column].str.lower().map(
          pdf.query('name.notnull()').set_index('name')['id']
        )
      )
      pid = pid.combine_first(
        parsed_investigators[column].str.lower().map(
          pdf.query('synonyms.notnull()').explode('synonyms').set_index('synonyms')['id']
        )
      )
  # Find agency.id for each agency
  # NOTE: Assumes that multiple matches are likely
  results = []
  # names
  for column in ('name', 'english_name'):
    if column in parsed_agencies:
      results += [
        parsed_agencies[column].str.lower().map(adf.groupby(attribute)['id'].agg(set))
        for attribute in ('name', 'alternate_name', 'english_name')
      ]
  # abbreviations
  for column in ('name', 'english_name', 'abbreviation', 'english_abbreviation'):
    if column in parsed_agencies:
      results += [
        parsed_agencies[column].str.lower().map(adf.groupby(attribute)['id'].agg(set))
        for attribute in ('abbreviation', 'alternate_abbreviation', 'english_abbreviation')
      ]
  # first parent
  parent_results = []
  if 'parents' in parsed_agencies:
    # Make a version of adf with parent info instead of child but with child id
    has_parent = adf['parent_agency_id'].notnull()
    padf = adf.set_index('id').loc[adf['parent_agency_id'][has_parent]].reset_index()
    padf['id'] = adf['id'][has_parent].values
    # Tabulate immediate parent
    # (assumes either scalar NA or list of results)
    parent = parsed_agencies['parents'].apply(
      lambda x: {} if (not isinstance(x, list) and pd.isna(x)) else x[-1]
    )
    # parent.dropna(inplace=True)
    parent = pd.DataFrame(list(parent), index=parent.index).convert_dtypes()
    # Search by child by parent
    for column in ('name', 'english_name'):
      if column in parent:
        parent_results += [
          parent[column].str.lower().map(padf.groupby(attribute)['id'].agg(set))
          for attribute in ('name', 'alternate_name', 'english_name')
        ]
    for column in ('name', 'english_name', 'abbreviation', 'english_abbreviation'):
      if column in parent:
        parent_results += [
          parent[column].str.lower().map(padf.groupby(attribute)['id'].agg(set))
          for attribute in ('abbreviation', 'alternate_abbreviation', 'english_abbreviation')
        ]
  # Combine results by ignoring nulls and taking the intersection of sets in each series
  if results:
    final_results = pd.concat(results, axis=1).apply(
      lambda x: pd.NA if x.isnull().all() else set.intersection(*[xi for xi in x.dropna()]),
      axis=1
    )
    if parent_results:
      # Intersect parent results, union child results, then intersect
      final_parent_results = pd.concat(parent_results, axis=1).apply(
        lambda x: pd.NA if x.isnull().all() else set.intersection(*[xi for xi in x.dropna()]),
        axis=1
      )
      has_parent = final_parent_results.notnull()
      child_union = pd.concat(results, axis=1).apply(
        lambda x: pd.NA if x.isnull().all() else set.union(*[xi for xi in x.dropna()]),
        axis=1
      )
      final_results[has_parent] = [
        pd.NA if a is pd.NA or b is pd.NA else set(a).intersection(b)
        for a, b in zip(child_union[has_parent], final_parent_results[has_parent])
      ]
    is_null = final_results.isnull()
    final_results[is_null] = [set()] * is_null.sum()
    aid = final_results.apply(lambda x: list(x)[0] if len(x) == 1 else pd.NA)
  else:
    final_results = pd.Series()
    aid = pd.Series(dtype='Int64', index=parsed_agencies.index)
  # HACK: Don't allow multiple matches
  # HACK: Don't allow null matches
  error = ''
  missing_person = parsed_investigators[
    pid.isnull() & parsed_investigators.reindex(columns=['name'])['name'].notnull()
  ]
  if not missing_person.empty:
    text = ' · '.join(
      np.unique(list(str(x) for x in missing_person.to_dict(orient='records')))
    )
    error += f'Missing person: {text}\n\n'
  if not final_results.empty:
    ambiguous_agency = parsed_agencies[final_results.apply(len).gt(1)]
    missing_agency = parsed_agencies[final_results.apply(len).eq(0)]
    if not ambiguous_agency.empty:
      text = ' · '.join(
        np.unique(list(str(x) for x in ambiguous_agency.to_dict(orient='records')))
      )
      error += f'Ambiguous agency: {text}\n\n'
    if not missing_agency.empty:
      text = ' · '.join(
        np.unique(list(str(x) for x in missing_agency.to_dict(orient='records')))
      )
      error += f'Missing agency: {text}\n\n'
  if error:
    raise ValueError(error)
  # Tabulate new teams
  team_df = pd.DataFrame({
    'id': tid,
    'investigators': strings['investigators'],
    'agencies': strings['agencies']
  })[is_new].convert_dtypes()
  if 'team' in dfs:
    dfs['team'] = pd.concat([dfs['team'], team_df], ignore_index=True)
  else:
    dfs['team'] = team_df
  # Tabulate new team members
  dfs['team_member'] = (
    parsed_investigators.assign(person_id=pid).reset_index(names=['team_id'])
    .reindex(columns=['team_id', 'person_id', 'agencies', 'remarks'])
    .explode('agencies')
    .merge(
      parsed_agencies.assign(agency_id=aid).reset_index(names=['team_id']).reindex(columns=['team_id', 'id', 'agency_id']),
      how='left',
      left_on=['team_id', 'agencies'],
      right_on=['team_id', 'id']
    )
    .convert_dtypes()
    .assign(position=lambda df: df.groupby('team_id').transform('cumcount').add(1))
    .reindex(columns=['team_id', 'person_id', 'agency_id', 'position', 'remarks'])
  )
  # Replace investigators, agencies column with team_id column
  for table in tables:
    df = dfs[table]
    mask = df[investigators].notnull() | df[agencies].notnull()
    if team_id not in df:
      df[team_id] = pd.Series(dtype='Int64')
    columns = [column for column in (investigators, agencies) if column in df]
    temp_df = df[columns].reindex(columns=(investigators, agencies))
    temp_index = pd.MultiIndex.from_frame(temp_df)
    # HACK: Match null behavior by replacing pd.NA with nan
    df.loc[mask, team_id] = temp_index[mask].map(
      pd.Series(tid.values, index=strings.fillna(float('nan')))
    )
  return dfs


@register_check(
  test=False,
  required=[Column('id')]
)
def process_rgi_ids(
  df: pd.DataFrame,
  dfs: dict[Hashable, pd.DataFrame],
  *,
  db: fog.iodb.DB
) -> None:
  """Process glacier RGI IDs."""
  pairs = []
  for column in ('rgi50_id', 'rgi60_id'):
    if column in df:
      pairs.append(df[['id', column]].rename(
        columns={'id': 'glacier_id', column: 'rgi_id'}
      ))
  if not pairs:
    return None
  pairs = pd.concat(pairs, ignore_index=True).dropna().drop_duplicates()
  if pairs.empty:
    return None
  # Find the corresponding outline.id of each outline.original_id
  t = db.metadata.tables['outline']
  query = sqlalchemy.select(t.c['id'], t.c['original_id']).where(
    t.c['inventory'].in_(['rgi5', 'rgi6']),
    t.c['original_id'].in_(pairs['rgi_id'])
  )
  rgi = db.format_result_as_df(db.execute(query))
  pairs['outline_id'] = pairs['rgi_id'].map(rgi.set_index('original_id')['id'])
  pairs['manual'] = True
  # Check whether these associations already exist
  if 'glacier_outline' in dfs:
    db_pairs = dfs['glacier_outline']
  else:
    t = db.metadata.tables['glacier_outline']
    query = sqlalchemy.select(t.c['glacier_id'], t.c['outline_id'], t.c['manual']).where(
      t.c['glacier_id'].in_(pairs['glacier_id']),
      t.c['outline_id'].in_(pairs['outline_id'])
    )
    db_pairs = db.format_result_as_df(db.execute(query))
  # Filter out existing associations
  index = pairs.set_index(['glacier_id', 'outline_id', 'manual']).index
  db_index = db_pairs.set_index(['glacier_id', 'outline_id', 'manual']).index
  pairs = pairs[~index.isin(db_index)]
  dfs['glacier_outline'] = pairs[['glacier_id', 'outline_id', 'manual']]
  return None


FOG_REGEX = re.compile(r'fog:[0-9]+', flags=re.IGNORECASE)
GLIMS_REGEX = re.compile(r'glims:[0-9]+', flags=re.IGNORECASE)
RGI_REGEX = {
  'rgi5': re.compile(r'RGI50-[0-1][0-9]\.[0-9]{5}', flags=re.IGNORECASE),
  'rgi6': re.compile(r'RGI60-[0-1][0-9]\.[0-9]{5}', flags=re.IGNORECASE),
  'rgi7': re.compile(r'RGI2000-v7\.0-[CG]-[0-1][0-9]-[0-9]{5}', flags=re.IGNORECASE)
}


@register_check(
  message='Not found in database nor in a submitted outline table'
)
def parse_outline_id(
  s: pd.Series,
  dfs: dict[Hashable, pd.DataFrame],
  *,
  db: fog.iodb.DB
) -> Optional[Tuple[pd.Series, pd.Series]]:
  """
  Check whether outline IDs are valid and replace them with an outline id.
  """
  mask = s.notnull()
  if not mask.any():
    return None
  input_ids = s[mask].drop_duplicates()
  output_ids = pd.Series(index=input_ids.index)
  valid = pd.Series(index=input_ids.index)
  # Check for valid outline IDs
  # fog: Strip prefix and match to outline.id
  is_match = input_ids.str.fullmatch(FOG_REGEX)
  if is_match.any():
    ids = (
      input_ids[is_match].str.replace('^fog:', '', case=False, regex=True)
      .astype('Int64')
    )
    is_found = db.is_in_column(ids, 'outline', 'id')
    ids[~is_found] = pd.NA
    output_ids[is_match] = ids
    valid[is_match] = is_found
  # glims: Strip prefix and fetch (outline.id, outline.original_id)
  is_match = input_ids.str.fullmatch(GLIMS_REGEX)
  if is_match.any():
    ids = input_ids[is_match].str.replace('^glims:', '', case=False, regex=True)
    t = db.metadata.tables['outline']
    query = sqlalchemy.select(t.c['id'], t.c['original_id']).where(
      t.c['inventory'] == 'glims',
      t.c['original_id'].in_(ids)
    )
    result = db.format_result_as_df(db.execute(query))
    output_ids[is_match] = ids.map(result.set_index('original_id')['id'])
    valid[is_match] = output_ids[is_match].notnull()
  # rgi: Fetch (outline.id, outline.original_id)
  for inventory, regex in RGI_REGEX.items():
    is_match = input_ids.str.fullmatch(regex)
    if is_match.any():
      ids = input_ids[is_match]
      t = db.metadata.tables['outline']
      query = sqlalchemy.select(t.c['id'], t.c['original_id']).where(
        t.c['inventory'] == inventory,
        t.c['original_id'].in_(ids)
      )
      result = db.format_result_as_df(db.execute(query))
      output_ids[is_match] = ids.map(result.set_index('original_id')['id'])
      valid[is_match] = output_ids[is_match].notnull()
  # custom: Check local (outline.id, outline.original_id)
  is_custom = valid.isnull()
  if is_custom.any():
    if 'outline' in dfs and 'original_id' in dfs['outline']:
      output_ids[is_custom] = input_ids[is_custom].map(
        dfs['outline'].set_index('original_id')['id']
      )
      valid[is_custom] = output_ids[is_custom].notnull()
    else:
      valid[is_custom] = False
  # Expand result to the original series
  return (
    s.map(dict(zip(input_ids, valid))),
    s.map(dict(zip(input_ids, output_ids))).astype('Int64')
  )


def gather_outline_ids(dfs: dict[Hashable, pd.DataFrame]) -> pd.Series:
  """Harvest outline IDs from submitted tables."""
  outline_ids = []
  for table, column in fog.metadata.OUTLINE_ID_COLUMNS:
    if table in dfs and column in dfs[table]:
      outline_ids.append(dfs[table][column])
  if not outline_ids:
    return pd.Series(dtype='string')
  return pd.concat(outline_ids, ignore_index=True).dropna().drop_duplicates()


@register_check(
  message='Outlines were submitted but none were used',
  required=[Column('id', table='outline')]
)
def submitted_outlines_are_used(dfs: dict[Hashable, pd.DataFrame]) -> bool:
  """Check if submitted outlines are used."""
  outline_ids = gather_outline_ids(dfs)
  is_used = outline_ids.isin(dfs['outline']['id'])
  # HACK: Prevent error from validator because result is numpy.bool_ instead of bool
  return bool(is_used.any())


@register_check(
  test=False
)
def fillna_front_variation_series_id(df: pd.DataFrame, *, value: int = 0) -> pd.DataFrame:
  """Fill missing front variation series ID."""
  column = 'series_id'
  if column not in df:
    df[column] = pd.Series(dtype='Int64')
  df[column].fillna(value, inplace=True)
  return df


@register_check(
  message='Polygon is invalid or empty'
)
def wkt_polygon_is_valid(
  s: pd.Series, *, fix_invalid: bool = False
) -> Tuple[pd.Series, pd.Series]:
  """Check if WKT polygon is valid and not empty."""
  s = gpd.GeoSeries.from_wkt(s)
  is_valid = s.is_valid
  if fix_invalid:
    s[~is_valid] = s[~is_valid].apply(shapely.validation.make_valid)
    is_valid[~is_valid] = s[~is_valid].is_valid
  is_valid &= s.area > 0
  # Convert GeometryCollection to MultiPolygon
  is_collection = s.geom_type == 'GeometryCollection'
  s[is_collection] = s[is_collection].apply(
    lambda geom: shapely.MultiPolygon([x for x in geom.geoms if x.area])
  )
  return is_valid, s.to_wkt()
