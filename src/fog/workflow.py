"""
Submission processing workflow.

The `fog` workflow expects each submission to be a single Excel file
(with an 'xls' or 'xlsx' extension) placed in its own folder.
The folder can also contain subfolders, which are ignored. For example:

- `CH_Landmann_point_MB/`
  - `FoG_Submission_Landmann_20211129.xlsx`: Submission file to process
  - `original/`: Subfolder with all the original submission files

Each submission is prepared for the database following the same steps:

- `read`: Read all sheets in the Excel file. The WGMS template version (`2021`:
  latest, `2020`: anything before 2021) is inferred from sheet names, and used
  to decide which rows to skip and which rows contain column names. Should only
  fail if the Excel file is corrupted and cannot be opened, in which case the
  software cannot proceed.
- `migrate`: Transform the data as needed (e.g. rename tables) to conform to the
  reference (metadata) format. Should only fail if the Excel file was not a
  supported WGMS template (in which case table or column names are not as
  expected) or if the data contains legacy columns which cannot be migrated
  automatically.
- `check`: Check the data against itself and the database and calculate
  differences with the database.
- `final`: The data committed to the database.

Each of `migrate`, `check`, and `final` result in a new Excel file
identified by a prefix.
If issues are found during `migrate` or `check`,
processing stops and a report is printed to the console.
The corresponding Excel file (`(1-migrate)-*.xlsx` or `(2-check)-*.xlsx`)
will contain helpful markup:

- red row/column/cell: Row/column/cell failed an error-level check
- yellow row/column/cell: Row/column/cell failed a warning-level check
- grey row/column label: Row/column was removed (either dropped or renamed)
- cell comment: Cell value was changed (new value is shown in comment)

Open the Excel file, make changes as needed, save and close the file, and rerun.
Processing will restart from the furthest-advanced Excel file.
In the end, the submission folder will look like the following:

- `CH_Landmann_point_MB/`
  - `(1-migrate)-FoG_Submission_Landmann_20211129.xlsx`: Migrated, with markup
  - `(2-check)-FoG_Submission_Landmann_20211129.xlsx`: Checked, with markup
  - `(3-final)-FoG_Submission_Landmann_20211129.xlsx`: Committed to database
  - `FoG_Submission_Landmann_20211129.xlsx`: Original
  - `GLACIER.html`, ...: For each table with changes, a visualization of how the
    database changes if `(2-check)-*.xlsx` is applied. New records are shown in
    green, value changes are shown in blue with arrows (e.g. 'old' -> 'new').
  - `reports.log`: Text file containing the console output for each processing
    run.
"""
import datetime
import logging
from pathlib import Path
import re
import time
from typing import Dict, List, Optional, Union

import daff
import geopandas as gpd
import pandas as pd
from validator import Tables
from validator.schema import Report

from fog.config import colorama
import fog.helpers
import fog.io
import fog.iodb
import fog.metadata
import fog.schema

# ---- Configure logging ----

class ConsoleFormatter(logging.Formatter):
  """
  Logging formatter for console output that colors messages by level.

  * warning: yellow
  * error: red
  * critical: bright red

  See https://alexandra-zaharia.github.io/posts/make-your-own-custom-color-formatter-with-python-logging.
  Adapted from https://stackoverflow.com/a/56944256/3638629.
  """

  def __init__(self, fmt: str = '%(message)s') -> None:
    super().__init__()
    self.fmt: str = fmt
    self.FORMATS: Dict[int, str] = {
      logging.DEBUG: self.fmt,
      logging.INFO: self.fmt,
      logging.WARNING: (
        colorama.Style.BRIGHT + colorama.Fore.YELLOW + self.fmt +
        colorama.Style.RESET_ALL
      ),
      logging.ERROR: (
        colorama.Style.BRIGHT + colorama.Fore.RED + self.fmt +
        colorama.Style.RESET_ALL
      ),
      logging.CRITICAL: (
        colorama.Style.BRIGHT + colorama.Fore.RED + self.fmt +
        colorama.Style.RESET_ALL
      ),
    }

  def format(self, record: logging.LogRecord) -> str:
    log_fmt = self.FORMATS.get(record.levelno)
    formatter = logging.Formatter(log_fmt)
    return formatter.format(record)


class FileFormatter(logging.Formatter):
  """
  Logging formatter for file output that strips ansi escape sequences.

  See https://stackoverflow.com/a/14693789.
  """

  def __init__(self, fmt: str = '%(message)s') -> None:
    super().__init__()
    self.fmt = fmt
    self.REGEX = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')

  def format(self, record: logging.LogRecord) -> str:
    formatter = logging.Formatter(self.fmt)
    return self.REGEX.sub('', formatter.format(record))

# Enable logging
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

# Log to console
CONSOLE = logging.StreamHandler()
CONSOLE.setLevel(logging.INFO)
CONSOLE.setFormatter(ConsoleFormatter())
LOGGER.addHandler(CONSOLE)

# Enable logging to file
REPORT = logging.getLogger('report')
REPORT.setLevel(logging.INFO)

SESSION: Dict[str, int] = {
  'final': 0
}
"""
Session statistics.

* final: Number of committed submissions.
"""

# ---- Configure reporting ----

DAFF_FLAGS = daff.CompareFlags()
# Show unchanged rows
DAFF_FLAGS.show_unchanged = True
# Show unchanged columns
DAFF_FLAGS.show_unchanged_columns = True

COLUMN_ORDER: Dict[str, List[str]] = {
  resource['name']: [field['name'] for field in resource['schema']['fields']]
  for resource in fog.schema.EXTENDED_SUBMISSION_METADATA['resources']
}
"""
Column order by table.

Used for ordering table and column checks in report.
"""

DIFF_KEYS: Dict[str, List[str]] = {
  resource['name']: resource['schema']['primaryKey']
  for resource in fog.schema.OUTPUT_METADATA['resources']
}
"""
Primary key by table.

Used for computing the tabular diffs.
"""

TAGS: List[str] = ['original', 'migrate', 'check', 'preview', 'final']
"""
Workflow tags added to the original file name as [{index}-{tag}]-{name}.

* original: Original submission. Should not be edited.
* migrate: Failed automatic migration. Needs to be edited by a human.
* check: Failed checks. Needs to be reviewed and/or edited by a human.
* preview: Preview of what will be committed. Should not be edited.
* final: Committed to database. Should not be edited.

If a tag is omitted, 'original' is assumed.
"""


# ---- Helper functions ----

def find_excel_files(path: Union[str, Path]) -> List[Path]:
  """
  Find Excel files in a directory.

  Arguments
  ---------
  path
    Directory in which to search (non-recursive).

  Raises
  ------
  ValueError
    Path is not a directory.
  ValueError
    Path does not contain Excel files.

  Examples
  --------
  >>> import tempfile
  >>> dir = tempfile.TemporaryDirectory()
  >>> kwargs = {'dir': dir.name, 'suffix': '.xlsx'}

  >>> find_excel_files(dir.name)
  Traceback (most recent call last):
  ValueError: ... does not contain Excel files

  >>> original = tempfile.NamedTemporaryFile(**kwargs)
  >>> find_excel_files(dir.name)[0].as_posix() == original.name
  True

  >>> original.close()
  >>> dir.cleanup()
  """
  path = Path(path)
  if not path.is_dir():
    raise ValueError(f'{path} is not a directory')
  files = []
  for file in path.glob('*'):
    if (
      file.is_file() and
      not file.name.startswith('~$') and
      file.name.lower().endswith(('.xls', '.xlsx'))
    ):
      files.append(file)
  if not files:
    raise ValueError(f'{path} does not contain Excel files')
  return sorted(files)


def parse_excel_file_path(path: Union[str, Path]) -> Dict[str, Union[int, str]]:
  """
  Parse Excel file path.

  Arguments
  ---------
  path
    Path of Excel file.

  Returns
  -------
  dict
    Parsed path.

    - basename: Base filename.
    - tag: Workflow tag (default: 'original').
    - index: Whether a `tag` index is present (default: True).
    - left: Character used on left side of `tag` ('[' or '(', default: '(').
    - right: Character used on right side of `tag` (']' or ')', default: ')').

  Examples
  --------
  >>> parse_excel_file_path('[1-migrate]-fog-submission.xlsx')
  {'basename': 'fog-submission', 'tag': 'migrate', 'index': True,
  'left': '[', 'right': ']'}
  >>> parse_excel_file_path('(check)-fog-(submission).xlsx')
  {'basename': 'fog-(submission)', 'tag': 'check', 'index': False,
  'left': '(', 'right': ')'}
  >>> parse_excel_file_path('fog-(submission).xlsx')
  {'basename': 'fog-(submission)', 'tag': 'original', 'index': True,
  'left': '(', 'right': ')'}
  """
  path = Path(path)
  match = re.fullmatch(
    r'^(?P<left>[\[\(])(?:(?P<index>[0-9]+)-)?(?P<tag>[A-z]+)(?P<right>[\]\)])-(?P<basename>.+)$',
    path.stem
  )
  default = {
    'basename': path.stem,
    'tag': TAGS[0],
    'index': 0,
    'left': '(',
    'right': ')'
  }
  if match:
    parsed = match.groupdict()
    default.update(parsed)
  default['index'] = default['index'] is not None
  return default


def build_excel_file_path(
  basename: str,
  tag: str,
  index: bool = True,
  left: str = '(',
  right: str = ')'
) -> str:
  """
  Build Excel file path.

  Arguments
  ---------
  basename
    Base filename.
  tag
    Workflow tag (0: 'original', 1: 'migrate', 2: 'check', 3: 'final').
  index
    Whether to use a numeric `tag` prefix to control file order.
    Determined from `tag`.
  left
    Character used on left side of `tag` (and prefix if `index=True`).
  right
    Character used on right side of `tag` (and prefix if `index=True`).

  Examples
  --------
  >>> build_excel_file_path('submission', 'migrate')
  '(1-migrate)-submission.xlsx'
  >>> build_excel_file_path('submission', 'migrate', index=False)
  '(migrate)-submission.xlsx'
  """
  prefix = f'{TAGS.index(tag)}-' if index else ''
  return f'{left}{prefix}{tag}{right}-{basename}.xlsx'


def select_excel_file(path: Union[str, Path], tag: str = None) -> Path:
  """
  Select Excel file from directory based on workflow tag.

  Arguments
  ---------
  path
    Directory in which to search.
  tag
    Workflow tag.

  Raises
  ------
  ValueError
    Either none or multiple files meeting the criteria.

  Examples
  --------
  >>> import tempfile
  >>> dir = tempfile.TemporaryDirectory()
  >>> kwargs = {'dir': dir.name, 'suffix': '.xlsx'}

  No Excel files present.

  >>> select_excel_file(dir.name)
  Traceback (most recent call last):
  ValueError: ... does not contain Excel files

  Original Excel file.

  >>> original = tempfile.NamedTemporaryFile(**kwargs)
  >>> select_excel_file(dir.name).as_posix() == original.name
  True
  >>> original2 = tempfile.NamedTemporaryFile(**kwargs)
  >>> select_excel_file(dir.name)
  Traceback (most recent call last):
  ValueError: Multiple untagged Excel files ...

  Normal workflow.

  >>> migrate = tempfile.NamedTemporaryFile(**kwargs, prefix='(1-migrate)-')
  >>> select_excel_file(dir.name).as_posix() == migrate.name
  True
  >>> check = tempfile.NamedTemporaryFile(**kwargs, prefix='(2-check)-')
  >>> select_excel_file(dir.name).as_posix() == check.name
  True
  >>> final = tempfile.NamedTemporaryFile(**kwargs, prefix='(3-final)-')
  >>> select_excel_file(dir.name).as_posix() == final.name
  True
  >>> select_excel_file(dir.name, tag='check').as_posix() == check.name
  True

  Duplicate workflow tags.

  >>> check2 = tempfile.NamedTemporaryFile(**kwargs, prefix='(2-check)-')
  >>> select_excel_file(dir.name, tag='check')
  Traceback (most recent call last):
  ValueError: Multiple Excel files tagged as 'check' in ...

  >>> for file in [original, original2, migrate, check, final, check2]:
  ...   file.close()
  >>> dir.cleanup()
  """
  matches = []
  max_index = 0
  for file in find_excel_files(path):
    parsed = parse_excel_file_path(file)
    if tag and tag.lower() != parsed['tag']:
      continue
    if parsed['tag'] not in TAGS:
      logging.warning(
        f"[warning] File with unrecognized tag '{parsed['tag']}': {file.name}"
      )
      continue
    index = TAGS.index(parsed['tag'])
    if index == max_index:
      matches.append(file)
    elif index > max_index:
      matches = [file]
      max_index = index
  tag = tag or TAGS[max_index]
  if not matches:
    if tag == 'original':
      msg = f"No untagged Excel files (or files tagged as '{tag}') in {path}"
    else:
      msg = f"No Excel files tagged as '{tag}' in {path}"
    raise ValueError(msg)
  if len(matches) > 1:
    if tag == 'original':
      msg = f"Multiple untagged Excel files (or files tagged as '{tag}') in {path}"
    else:
      msg = f"Multiple Excel files tagged as '{tag}' in {path}"
    raise ValueError(f'{msg}:\n' + '\n'.join(f'* {p.stem}' for p in matches))
  return matches[0]


COMMIT_TIMEOUT = 120
"""
Seconds permitted before commit prompt times out.
"""

GENERIC_ERROR_MESSAGE = 'Something unexpected happened'
"""
Error message shown by default if an assertion fails.
"""


def guess_cfd_year(date: datetime.date = None) -> int:
  """
  Guess call-for-data year from date.

  Examples
  --------
  >>> import datetime
  >>> guess_cfd_year(datetime.date(2022, 9, 30))
  2021
  >>> guess_cfd_year(datetime.date(2022, 10, 1))
  2022
  >>> guess_cfd_year(datetime.date(2023, 1, 23))
  2022
  """
  if date is None:
    date = datetime.date.today()
  return date.year if date.month >= 10 else date.year - 1


def build_submission_id(path: Union[str, Path], cfd_year: int) -> str:
  """
  Build submission ID.

  Arguments
  ---------
  path
    Path to final Excel file committed to database
  cfd_year
    Year of call-for-data (CfD) of which this submission is a part

  Examples
  --------
  >>> build_submission_id('data/CH_Elias_GLAMOS/final_submission.xlsx', 2022)
  '2022/CH_Elias_GLAMOS/final_submission.xlsx'
  """
  path = Path(path)
  return f'{cfd_year}/{path.parent.stem}/{path.name}'


def process_submission(
  path: Union[str, Path],
  outline_path: Union[str, Path] = None,
  version: int = None,
  ignore_warnings: bool = False,
  allow_commit: bool = False,
  cfd_year: int = None,
  ignore_omitted: bool = False,
  ignore_checks: bool = False
) -> Report:
  """
  Process a submission.

  Follow the instructions printed to the console and rerun as many times as needed.

  In the validation report, a summary table gives the number of checks by outcome.

  - `error`: Check encountered a software error. Seek technical support.
  - `fail`: Check failed, as described in the report. Fix the data as needed.
  - `skip`: Check did not apply (e.g. table not present in data). Ignore.
  - `pass`: Check passed. Celebrate!

  *If the tables in the report printed to the console are too wide (or narrow),
  adjust `fog.config.MAX_REPORT_COLUMN_WIDTH`.*

  Empty cells are filled with their current value in the database.
  To clear a cell, write `'NULL'` into that cell.

  Arguments
  ---------
  path
    Path to directory containing the submission.
  outline_path
    Path to the outline file (readable by GDAL) relative to path.
  version
    Submission schema version: 2020, 2021, or 2024.
    If `None`, guessed from table names.
  ignore_warnings
    Whether to ignore warnings and forge ahead.
    Only set to `True` after confirming all remaining warnings can be ignored.
  allow_commit
    Whether to prompt user to commit submission to database if tests pass.
  cfd_year
    Year of call-for-data (CfD) of which this submission is a part
    (e.g. 2022 for CfD announced on 2022-10-01).
    If `None`, guessed from the current date assuming a CfD starts on October 1.
  ignore_omitted
    Whether to ignore all warnings and errors in omitted tables or rows.
  ignore_checks
    Whether to ignore errors in checks and forge ahead. Dangerous!
    Only set to `True` after confirming all remaining errors can be ignored.
    Committing to the database will fail unless the database is more permissive.
  """
  # ---- Initialize ----

  # Find most advanced excel file and its tag
  file = select_excel_file(path)
  parsed = parse_excel_file_path(file)
  tag: str = parsed['tag']
  report = Report([], target=Tables())
  dfs: Optional[Dict[str, pd.DataFrame]] = None
  # Exit if submission already processed
  if tag == 'final':
    logging.info(f"[{tag}] Found an Excel file tagged as [{tag}]: {file.name}")
    return report
  # Delete preview file and return to checks
  if tag == 'preview':
    file.unlink()
    file = select_excel_file(path)
    parsed = parse_excel_file_path(file)
    tag: str = parsed['tag']
  # Delete existing diffs
  for resource in fog.schema.OUTPUT_METADATA['resources']:
    temp = file.parent / f"{resource['name']}.html"
    temp.unlink(missing_ok=True)

  # Append timestamp to logfile
  logfile_path = Path(path) / 'reports.log'
  with open(logfile_path, 'w') as fp:
    timestamp = datetime.datetime.now().isoformat(
      sep=' ', timespec='milliseconds'
    )
    fp.write(f'\n# ---- {timestamp} ---- #\n\n')
  # Register logfile handler, deleting any others
  logfile = logging.FileHandler(logfile_path, mode='a', encoding='utf-8')
  logfile.setLevel(logging.INFO)
  logfile.setFormatter(FileFormatter())
  REPORT.handlers = [logfile]
  # Store reference to updated rows
  # NOTE: Currently unused
  # is_change: Dict[str, pd.DataFrame] = {}

  # ---- Read original submission ----

  if tag == TAGS[0]:
    REPORT.info(f'[{tag}] Reading data from file: {file.name}')
    dfs = fog.io.read_excel(file, dtype='string')
    version = version or fog.io.guess_excel_template_version(dfs)
    # Clean submission
    dfs = fog.schema.CLEAN_SUBMISSION(dfs).output
    report.input = dfs
    # Move on to migration
    tag = TAGS[1]

  # ---- Migrate submission ----

  if tag == TAGS[1]:

    # ---- Read migrated submission ----
    if dfs is None:
      REPORT.info(f'[{tag}] Reading data from file: {file.name}')
      # Read row index from first column
      dfs = fog.io.read_excel(file, dtype='string')
      report.input = dfs

    # ---- Guess version from table names in original submission ----
    if version is None:
      original_file = select_excel_file(path, tag='original')
      book = pd.ExcelFile(original_file)
      version = fog.io.guess_excel_template_version(book.sheet_names)

    REPORT.info(f'[{tag}] Running migration')

    # ---- Migrate submission template ----
    # Includes table and column presence and names.
    # Must be robust to 'NULL' cells.
    submission_migration = fog.schema.get_submission_migration(version=version)
    # Repeat cleaning, since migration may have dropped ignorable content
    report += (submission_migration + fog.schema.CLEAN_SUBMISSION)(dfs)

    # ---- Migrate submission schema ----
    # Includes table and column presence and names, and column values.
    # Must be robust to 'NULL' cells.
    # Use start of schema migration as basis for Excel file
    report_basis = report.output
    schema_migration = fog.schema.get_schema_migration(version=version)
    report += schema_migration(report.output)

    # ---- Write Excel file ----
    out_name = build_excel_file_path(**{**parsed, 'tag': tag})
    fog.io.write_excel_report(
      path=file.parent / out_name, report=report, input=report_basis
    )

    # ---- Log summary result to console ----
    failures = report.counts_by_tag.get('fail', {})
    ignored_warnings = (
      ignore_warnings and
      not failures.get(None) and
      failures.get('warning')
    )
    if report.valid:
      REPORT.info(f'[{tag}] Migration passed: {out_name}')
    elif ignored_warnings:
      REPORT.warning(
        f'[{tag}] Migration ignored {ignored_warnings} warnings: {out_name}'
      )
    else:
      REPORT.error(f'[{tag}] Migration failed: Rerun after editing {out_name}')

    # ---- Print report on failure ----
    if not report.valid:
      REPORT.info(f'\n# File: {out_name}')
      printed_report = fog.io.render_report(
        report, input=report_basis, order=COLUMN_ORDER
      )
      REPORT.info(printed_report)

    # --- Exit on failure (unless warnings were ignored) ----
    if not report.valid and not ignored_warnings:
      return report

    # ---- Proceed to checks ----
    dfs, tag = report.output, TAGS[2]

  # ---- Check migrated submission ----

  if tag == TAGS[2]:

    # ---- Read checked submission ----
    if dfs is None:
      REPORT.info(f'[{tag}] Reading data from file: {file.name}')
      dfs = fog.io.read_excel(file, dtype='string')
      # Clean submission
      dfs = fog.schema.CLEAN_SUBMISSION(dfs).output

    # Load custom outlines
    if outline_path:
      outline_ids = fog.checks.gather_outline_ids(dfs)
      if outline_ids.empty:
        raise ValueError('Outline path provided but no outline referenced in submission')
      try:
        outlines_gdf = gpd.read_file(Path(path) / outline_path)
      except Exception as e:
        raise ValueError(f'Failed to read outlines from path: {e}')
      outlines_gdf.rename_geometry('polygon', inplace=True)
      # HACK: Remove 'wkt' column left in place if read from CSV
      outlines_gdf.drop(columns=['wkt', 'WKT'], errors='ignore', inplace=True)
      # HACK: Pass through as DataFrame to avoid GeoDataFrame behaviors
      outlines_gdf = outlines_gdf.to_wkt()

    # Fill existing tables with any missing columns
    dfs = fog.schema.FILL_SUBMISSION(dfs).output
    REPORT.info(f'[{tag}] Running checks')

    # Use start of checks as basis for the Excel file
    report.input = dfs
    report_basis = dfs

    # --- Prepare submission for merge ----
    db = fog.iodb.DB()

    # Drop unsupported 'NULL' cells
    check_report = fog.schema.DROP_UNSUPPORTED_NULL_STRINGS(dfs)
    dfs = check_report.output

    # Inject outlines after report basis but before submission checks
    if outline_path:
      dfs['outline'] = outlines_gdf

    # Find and mask remaining 'NULL' cells
    is_null_string = {
      table: df.eq('NULL').fillna(False)
      for table, df in dfs.items()
    }
    fog.helpers.mask_dataframes(dfs, is_null_string, inplace=True)

    # Process submission
    check_report += (
      # Parse and check submitted columns in place
      fog.schema.REJECT_EXTRA_COLUMNS +
      fog.schema.PARSE_SUBMISSION +
      fog.schema.get_remote_fkey_checks(db) +
      fog.schema.CHECK_SUBMITTED_DATES +
      # Fill incremental id columns (requires state, change date columns)
      fog.schema.get_serial_key_checks(db) +
      # Format date columns
      fog.schema.PRE_MERGE_MIGRATION
    )(dfs)
    dfs = check_report.output

    # ---- Merge submission with database ----

    # Find non-null cells
    is_not_null = {}
    for table, df in dfs.items():
      columns = df.columns.union(is_null_string[table].columns)
      is_not_null[table] = (
        df.notnull().reindex(columns=columns, fill_value=False) |
        is_null_string[table].reindex(columns=columns, fill_value=False)
      )

    # Select and merge with existing rows (by primary key)
    dbdfs = db.select_all_rows_by_pkey(dfs)
    for resource in fog.schema.OUTPUT_METADATA['resources']:
      table = resource['name']
      if table in dbdfs:
        key = resource['schema']['primaryKey']
        # NOTE: is_change performed later by direct comparison with dbdfs
        # is_change[table] = fog.helpers.fill_dataframe(
        fog.helpers.fill_dataframe(
          df=dfs[table],
          defaults=dbdfs[table],
          key=key,
          mask=is_not_null[table]
        )

    # Add missing rows of existing groups
    db_group_rows = db.select_missing_rows_by_group(dfs)
    for resource in fog.schema.OUTPUT_METADATA['resources']:
      table = resource['name']
      if table in db_group_rows:
        if table in dfs:
          key = resource['schema']['primaryKey']
          dfs[table] = fog.helpers.extend_dataframe(
            dfs[table], db_group_rows[table], key=key
          )
        else:
          dfs[table] = db_group_rows[table]

    # Add glacier names and outlines
    # (Before foreign key rows to include glacier_name.bibliography_id)
    glacier_ids = pd.DataFrame({'glacier_id': list(db.collect_glacier_ids(dfs))})
    for resource in fog.schema.OUTPUT_METADATA['resources']:
      table = resource['name']
      if table in ('glacier_name', 'glacier_outline'):
        dbdfs[table] = db.select_rows(table, keys=glacier_ids)
        if table in dfs:
          key = resource['schema']['primaryKey']
          dfs[table] = fog.helpers.extend_dataframe(dfs[table], dbdfs[table], key=key)
        else:
          dfs[table] = dbdfs[table]

    # Add missing foreign key references
    db_fkey_rows = db.select_missing_rows_by_fkey(dfs)
    for resource in fog.schema.OUTPUT_METADATA['resources']:
      table = resource['name']
      if table in db_fkey_rows:
        if table in dfs:
          key = resource['schema']['primaryKey']
          dfs[table] = fog.helpers.extend_dataframe(
            dfs[table], db_fkey_rows[table], key=key
          )
        else:
          dfs[table] = db_fkey_rows[table]

    # Add foreign key and group rows to dbdf
    for rows in (db_group_rows, db_fkey_rows):
      for resource in fog.schema.OUTPUT_METADATA['resources']:
        table = resource['name']
        if table in rows:
          if table in dbdfs:
            key = resource['schema']['primaryKey']
            dbdfs[table] = fog.helpers.extend_dataframe(
              dbdfs[table], rows[table], key=key
            )
          else:
            dbdfs[table] = rows[table]

    # ---- Check merged result ----

    # Validate and migrate to database format
    check_report += (
      # Check and drop remaining submission-only columns
      fog.schema.POST_MERGE_MIGRATION +
      # (glacier.country)
      fog.schema.get_country_checks(db) +
      # (glacier.rgi50_id, glacier.rgi60_id)
      fog.schema.get_rgi_checks(db) +
      # (references)
      fog.schema.get_references_checks(db) +
      # (investigators, agencies)
      fog.schema.get_investigators_agencies_checks(db) +
      # (outline)
      fog.schema.get_outline_checks(db)
    )(dfs)
    dfs = check_report.output

    # Vector constraints (required, unique), foreign keys, and (multi) row checks
    # Keep only errors and failures from unsubmitted data
    standard_report = fog.schema.get_standard_checks(submission=True)(dfs)
    submitted_dfs = fog.helpers.filter_dataframes(dfs, is_not_null, columns=False)
    rin, rout = standard_report.project(submitted_dfs)
    rout.results = [
      result for result in rout.results
      if (
        result.code == 'error' or
        (
          not ignore_omitted and
          result.code == 'fail' and
          result.check.tag is None
        )
      )
    ]
    check_report += rin + rout
    check_report.output = standard_report.output
    report += check_report

    # ---- Write Excel file ----
    out_name = build_excel_file_path(**{**parsed, 'tag': tag})
    # Sort tables and columns
    fog.io.write_excel_report(
      path=file.parent / out_name,
      report=check_report,
      input=fog.schema.SORT_SUBMISSION(report_basis).output,
      # Restore 'NULL' to report output
      output=fog.helpers.mask_dataframes(
        check_report.output, is_null_string, value='NULL', dtype='object'
      )
    )

    # ---- Log summary result to console ----
    failures = check_report.counts_by_tag.get('fail', {})
    ignored_warnings = (
      ignore_warnings and
      not check_report.counts.get('error') and
      not failures.get(None) and
      failures.get('warning')
    )
    if check_report.valid:
      REPORT.info(f'[{tag}] Checks passed: {out_name}')
    elif ignored_warnings:
      REPORT.warning(
        f'[{tag}] Checks ignored {ignored_warnings} warnings: {out_name}'
      )
    else:
      REPORT.error(f'[{tag}] Checks failed: Rerun after editing {out_name}')

    # ---- Print report on failure -----
    if not check_report.valid:
      REPORT.info(f'\n# File: {out_name}')
      printed_report = fog.io.render_report(
        check_report, input=report_basis, order=COLUMN_ORDER
      )
      REPORT.info(printed_report)

    # ---- Generate tabular diffs ----
    # Prepare data for diffs
    # Limit to submitted tables and rows
    diff_dfs = fog.helpers.filter_dataframes(
      report.output, is_not_null, columns=False
    )
    # + tables that can be generated from submitted tables
    for table in fog.schema.GENERATED_TABLES:
      if table in report.output and table not in diff_dfs:
        diff_dfs[table] = report.output[table]
    # HACK: Shorten outline.polygon column to WKT geometry type
    if 'outline' in diff_dfs:
      diff_dfs['outline'] = diff_dfs['outline'].assign(
        polygon=lambda df: df['polygon'].str.extract(r'^([A-Z]+)')[0]
      )
    # Fill serial key columns
    # NOTE: Now performed earlier and theoretically not needed here
    # diff_dfs = fog.schema.get_serial_key_checks(db=db)(diff_dfs).output
    # Write diffs to file
    written = fog.io.write_table_db_diffs(
      path=file.parent,
      dfs=diff_dfs,
      dbdfs=dbdfs,
      primary_keys=DIFF_KEYS,
      flags=DAFF_FLAGS
    )
    if written:
      REPORT.warning(
        f'[{tag}] Tabular diffs (db -> submission) written for: {written}'
      )

    # ---- Exit on failure (unless warnings were ignored) ----
    if not check_report.valid and not ignored_warnings and not ignore_checks:
      return report

    # ---- Proceed to final ----
    dfs, tag = report.output, TAGS[-1]

  # ---- Finalize submission ----

  if tag == TAGS[-1]:
    REPORT.info(f'[check] Submission ready for database')

    # Compute changes between dfs and dbdfs
    inserts = {}
    updates = {}
    for table, df in dfs.items():
      if table in dbdfs:
        dbdf = dbdfs[table]
        key = DIFF_KEYS[table]
        # Find new rows
        df = df.set_index(key)
        dbdf = dbdf.set_index(key)
        is_old = df.index.isin(dbdf.index)
        inserts[table] = dfs[table].loc[~is_old]
        # Find changes in existing rows
        # NOTE: Assumes dbdf has a superset of columns in df
        old = df[is_old]
        dbold = dbdf.loc[old.index, old.columns]
        is_equal = (
          (old.isnull() & dbold.isnull()) |
          (old.notnull() & old.eq(dbold)).fillna(False)
        )
        old = old.mask(is_equal)
        any_change = (~is_equal).any(axis=1)
        # HACK: Reindex rows (necessary because of sorting?) to prevent user warning
        updates[table] = old[any_change.reindex(index=old.index)].reset_index()
        updates[table].index = dfs[table].index[is_old][any_change]
      else:
        inserts[table] = df
    # Restore 'NULL' strings to updates (before dropping empty content)
    updates = fog.helpers.mask_dataframes(
      updates, is_null_string, value='NULL', dtype='object'
    )
    # Drop empty columns
    for tables in (inserts, updates):
      for table, df in tables.items():
        tables[table] = df.drop(columns=df.columns[df.isnull().all(axis=0)])
    # Drop empty tables
    inserts = {table: df for table, df in inserts.items() if not df.empty}
    updates = {table: df for table, df in updates.items() if not df.empty}
    # Build Excel file
    excel_dfs = {}
    for table in dfs:
      if table in updates and table in inserts:
        excel_dfs[table] = pd.concat([updates[table], inserts[table]]).sort_index()
      elif table in updates:
        excel_dfs[table] = updates[table]
      elif table in inserts:
        excel_dfs[table] = inserts[table]
    # Write preview Excel file
    out_file = file.parent / build_excel_file_path(**{**parsed, 'tag': 'preview'})
    fog.io.write_excel(excel_dfs, path=out_file, freeze_panes=(1, 0), index=False)
    REPORT.info(f'[preview] To be committed to database: {out_file.name}')

    if not allow_commit:
      return report
    # Build submission id
    if cfd_year is None:
      cfd_year = guess_cfd_year()
      REPORT.warning(
        f'[final] Guessed cfd_year: {cfd_year} (to override, set cfd_year)'
      )
    out_file = file.parent / build_excel_file_path(**{**parsed, 'tag': tag})
    submission_id = build_submission_id(out_file, cfd_year=cfd_year)
    REPORT.info(f'[final] Submission ID: {submission_id}')
    REPORT.info("[final] To commit to database, type COMMIT and enter:")
    start = time.time()
    if input().lower() != 'commit':
      REPORT.warning('[final] Commit cancelled. Please rerun.')
      return report
    if (time.time() - start) > COMMIT_TIMEOUT:
      REPORT.warning(f'[final] More than {COMMIT_TIMEOUT} s have passed. Please rerun.')
      return report
    REPORT.info('[final] Committing to database...')

    # # Prepare data for output
    # # Limit to submitted tables and rows
    # dfs = fog.helpers.filter_dataframes(dfs, is_not_null, columns=False)
    # # Clear values filled from database
    # dfs = fog.helpers.mask_dataframes(
    #   dfs, {table: ~mask for table, mask in is_not_null.items()}
    # )

    # # Append new rows in generated tables
    # for table in fog.schema.GENERATED_TABLES:
    #   if table in report.output:
    #     assert table not in dfs
    #     df = report.output[table]
    #     if table in dbdfs:
    #       dbdf = dbdfs[table]
    #       key = DIFF_KEYS[table]
    #       # Find new rows
    #       ix = df.set_index(key).index
    #       dbix = dbdf.set_index(key).index
    #       rows = df.loc[~ix.isin(dbix)]
    #     else:
    #       rows = df
    #     if not rows.empty:
    #       dfs[table] = rows

    # Format serial keys, references, investigators, agencies
    # NOTE: Now performed earlier and theoretically not needed here
    # report += (
    #   fog.schema.get_serial_key_checks(db) +
    #   fog.schema.get_references_checks(db) +
    #   fog.schema.get_investigators_agencies_checks(db)
    # )(dfs)

    # NOTE: Not needed here because no change is made to report
    # if not report.valid:
    #   REPORT.error(f'[final] Formatting submission failed')
    #   return report
    # dfs = report.output

    # Null unchanged non primary-key cells in updated rows
    # NOTE: Maybe not needed and currently not working properly
    # fog.helpers.mask_dataframes(
    #   dfs,
    #   masks={
    #     table: ~mask.drop(columns=DIFF_KEYS[table])
    #     for table, mask in is_change.items()
    #   },
    #   inplace=True
    # )

    # # Drop unused data
    # for table, df in dfs.items():
    #   # # Drop rows with empty updates
    #   # # NOTE: Not working properly
    #   # if table in is_change:
    #   #   is_empty_update = ~is_change[table].any(axis=1)
    #   #   empty_update_index = is_change[table].index[is_empty_update]
    #   #   df.drop(index=empty_update_index, inplace=True)
    #   # Drop empty columns
    #   df.drop(columns=df.columns[df.isnull().all(axis=0)], inplace=True)
    # # Drop empty tables
    # dfs = {table: df for table, df in dfs.items() if not df.empty}
    # # Distinguish inserts from updates
    # inserts, updates = {}, {}
    # for table, df in dfs.items():
    #   if table in is_change:
    #     # Extract updated rows
    #     is_update = is_change[table].any(axis=1)
    #     update_index = is_change[table].index[is_update]
    #     if not update_index.empty:
    #       updates[table] = df.loc[update_index]
    #     # Extract inserted rows
    #     insert_index = df.index[~df.index.isin(is_change[table].index)]
    #     if not insert_index.empty:
    #       inserts[table] = df.loc[insert_index]
    #   else:
    #     # Extract all rows as inserted rows
    #     inserts[table] = df
    # Write to database
    try:
      db.commit_submission(
        insert=inserts,
        update=updates,
        submission_id=submission_id
      )
    except Exception as e:
      REPORT.error(f'[final] Writing to database failed with error: {e}')
      return report

    # Write to file
    fog.io.write_excel(excel_dfs, path=out_file, freeze_panes=(1, 0), index=False)
    REPORT.info(f'[final] Submission committed to database: {out_file.name}')
    SESSION['final'] += 1
    logging.info('\n🎉🎉 Congratulations 🎉🎉')
    logging.info(
      f"⭐⭐ {SESSION['final']} submissions committed (this session) ⭐⭐"
    )
    return report

  # Workflow did not proceed as expected
  assert False, GENERIC_ERROR_MESSAGE
