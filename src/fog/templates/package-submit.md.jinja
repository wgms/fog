# FoG Submission instructions {{ year }}

<span class="larger-text">[Overview](#overview) · [Major changes](#major-changes)<br>
[Special topics](#special-topics)</span> : [Glacier identification](#glacier-identification) · [Units](#units) · [Dates](#dates) · [Outlines](#outlines) · [Investigators & agencies](#investigators-agencies) · [References](#references) · [Remarks](#remarks)<br>
<span class="larger-text">[Tables](#tables)</span> : {% for resource in package.resources %}
[`{{ resource.name }}`](#{{ resource.name }}){% if not loop.last %} · {% endif %}
{% endfor %}{{ "\n" }}

## Overview

Follow these steps to determine which tables to fill out:

1. Check if the glacier already exists using this [online lookup table](https://docs.google.com/spreadsheets/d/106tjQr0b4NX98fSupbqEWNH1dsGjQ5uvPIHivO2xKw0). If so, note its `id` and name (either `short_name` or one of the existing `names`). If not, add a row to [`glacier`](#glacier) with a [`name`](#glacier.name) and temporary [`id`](#glacier.id). Use this as the `glacier_name` and `glacier_id` in all other tables to refer to this glacier.
2. For measurements of glacier area, elevation (mean, lowest, highest), length, or terminus type, use [`state`](#state). Use different rows if measurements do not share the same metadata (e.g. area and elevation surveyed on different dates or by different investigators). For area and mean elevation of elevation bands, use [`state_band`](#state_band) with the `glacier_id` and `date` of the corresponding `state`.
3. For glacier-wide elevation or volume change (from geodetic surveys), use [`change`](#change). Changes in area should be submitted as a series of glacier areas in [`state`](#state).
4. For changes in length, use [`front_variation`](#front_variation).
5. Mass balance (measured only by the direct glaciological method) is split across three tables:
    - Add a row to [`mass_balance`](#mass_balance) for each glacier ([`glacier_id`](#mass_balance.glacier_id)) and hydrological year ([`year`](#mass_balance.year)).
    - Add rows to [`mass_balance_point`](#mass_balance_point) for point measurements and/or [`mass_balance_band`](#mass_balance_band) for mass balance by elevation band, using the `glacier_id` and `year` of the corresponding [`mass_balance`](#mass_balance) entry.
6. For extraordinary events concerning glaciers, use [`event`](#event).

**Only include new data or deliberate updates to previously-submitted data.** Including previous submissions risks overwriting the database with out-of-date information! For specific updates to existing data, we prefer if you download the most recent database version (https://wgms.ch/data_databaseversions), extract the rows of interest, make (and mark) your changes, and submit that instead. Since empty cells are ignored, use `NULL` if you want a cell's existing value to be deleted.

## Major changes

- **All measurement units are standardized to the meter**: m, m w.e., m², m³, kg m⁻³ ([Units](#units)).
- Accumulation-area ratio (AAR) is a fraction (0-1) rather than a percentage (0-100%).
- All table and column names are lowercase, although they can be submitted as lower or upper case.
- `WGMS_ID` is renamed `glacier_id`, `POLITICAL_UNIT` is renamed `country` and only needed to submit a new glacier, and `NAME` is renamed `glacier_name` and should be a full name – e.g. `Glacier du Giétro` rather than `GIETRO` ([Glacier identification](#glacier-identification)).
- Dates should be formatted as `yyyy`, `yyyy-mm`, or `yyyy-mm-dd` ([Dates](#dates)). The required `99` for unknown month/day is replaced by an optional `XX`. Columns are added to represent date uncertainties in days.
- Investigators and agencies ([Investigators & agencies](#investigators-agencies)), references ([References](#references)), and remarks ([Remarks](#remarks)) should follow specific formats.
- Tables `MASS_BALANCE` (glacier and elevation-band balance) and `MASS_BALANCE_OVERVIEW` (overview) are renamed `mass_balance` (overview and glacier balance) and `mass_balance_band` (elevation-band balance).
- Glacier outlines ([Outlines](#outlines)) can be linked to glacier-wide measurements using columns `*outline_id`.

## Special topics

### Glacier identification

An integer identifies each glacier in the WGMS Fluctuations of Glaciers database. It is stored in the `id` column of the `glacier` table ([`glacier.id`](#glacier.id) for short), and required in all measurement tables as column `glacier_id` (formerly `WGMS_ID`). To prevent errors, measurement tables also require the glacier's name (`glacier_name`), which can either be the short name used historically (e.g. `GIETRO`) or a full name (preferred, e.g. `Glacier du Giétro`), but should be the same everywhere. If submitting a new name for the glacier in [`glacier.name`](#glacier.name) (e.g. to replace a placeholder like 'UNNAMED 15157' or 'RGI60-05.20098'), `glacier_name` must match this new name. As an additional safeguard, a `country` is required when submitting a new glacier.

See the [online lookup table](https://docs.google.com/spreadsheets/d/106tjQr0b4NX98fSupbqEWNH1dsGjQ5uvPIHivO2xKw0) or the WGMS [Fluctuations of Glaciers Browser](https://experience.arcgis.com/experience/836c66d14c8b410f940355056ddb1bf8) for help finding the `id` for your glacier.

### Units

To reduce confusion around unit conversions, all measurements must be submitted in SI units:

- m (meter): elevation, length
- m² (square meter): area
- m³ (cubic meter): volume
- m w.e. (meter water equivalent): mass balance
- kg m⁻³ (kilogram per cubic meter): density

When this results in very small or very large numbers, it is recommended to use scientific notation during manual entry (`1.23e6` m² instead of `1230000` m² to express 1.23 km²). The spreadsheet templates format numbers with a space between thousands (`1 230 000`) to improve readability.

### Dates

Dates (columns named `*date`) must be formatted as `yyyy-mm-dd` (4-digit year, 2-digit month, 2-digit day). Dashes (`-`) are optional (but encouraged for clarity) and unknown month and/or day may be omitted or indicated by `XX`, so that the following are equivalent:

- `1986-09-30` (preferred) · `19860930`
- `1986-09` (preferred) · `198609` · `1986-09-XX` · `198609XX`
- `1986` (preferred) · `1986-XX-XX` · `1986XXXX`

Every date has an optional uncertainty in days (column named `*date_unc`) to represent an estimated date (e.g. the date of the last summer horizon in a winter mass balance measurement) or an exact date range (e.g. the date of a multiday geodetic survey). For example, `date: 1986-09-30` and `date_unc: 1` represents the interval 1986-09-29 to 1986-10-01.

### Outlines

Glacier-wide measurements (in tables [`state`](#state), [`change`](#change), and [`mass_balance`](#mass_balance)) should be linked to the glacier outlines used to derive them, using columns named `*outline_id` (e.g. [`state.outline_id`](#state.outline_id)). Ideally the outline is included in Global Land Ice Measurements from Space (GLIMS) or a Randolph Glacier Inventory (RGI), in which case the `outline_id` must be the GLIMS _analysis_ identifier (`anlys_id`) prefixed by `GLIMS:` (e.g. `GLIMS:754359`) or the RGI identifier (e.g. `RGI60-11.02773`, `RGI2000-v7.0-G-11-02773`). If not, outlines can be submitted as (multi)polygons in any file format readable by GDAL (e.g. Shapefile, GeoJSON). In this case, the `outline_id` must match the `id` attribute of a polygon in this file.

When an outline is provided, the columns typically used to identify the glacier (`glacier_name` and `glacier_id`) may be omitted if unkown. This allows results from regional-scale geodetic studies to be submitted without the need for arbitrary glacier names or the tedious process of matching outlines to WGMS glacier identifiers.

### Investigators & agencies

The WGMS maintains a structured directory of people and agencies who performed each measurement. To support this effort, columns `investigators` (author names) and `agencies` (author affiliations) should be formatted as illustrated by the following example:

<table class="example">
<tr><td>`investigators`</td><td>`Michael Zemp (1, 2) | 李忠勤 [Li Zhongqin] | (3)`</td></tr>
<tr><td>`agencies`</td><td>`1. WGMS | 2. UZH > GIUZ: Geographisches Institut | 3. Tianshan Glaciological Station`</td></tr>
</table>

- Separate people with a pipe (`|`). Each person can be affiliated with one or more agencies, given in parentheses and separated with a comma (`Michael Zemp (1, 2)`). Omit the person for agencies without a person (`(3)`).
- Provide the person's full name (`Michael Zemp`, not `M. Zemp` or `Zemp, Michael`). Names in non-Latin script are encouraged, but should be followed by the preferred latinization in square brackets (`李忠勤 [Li Zhongqin`). The family name (for correct citation) can be indicated by curly braces (`{Li} Zhongqin`, `Guillermo {Cobos Campos}`). To resolve any ambiguity, an ORCID can be included after the name (`Michael Zemp 0000-0003-2391-7877`).
- Separate agencies with a pipe (`|`) and prefix each agency with its number and `.` (`1. WGMS`). Provide the agency's name, abbreviation, or both (`GIUZ: Geographisches Institut`). The original name is encouraged, and can be followed (especially for non-Latin names) by the English name in square brackets (`名古屋大学 [Nagoya University]`). Parent agencies can be provided by prefixing with `>` (`UZH > GIUZ`).
- Provide people and agency names from the time of the measurement, _not_ those at the time of submission.

### References

The WGMS maintains a structured bibliography in order to make your references more findable by users. To support this effort, references (columns named `references`) should be formatted as illustrated by the following example:

`Villalba 1990 (https://doi.org/10.2307/1551585) | Freysteinsson 1968 (https://timarit.is/page/6575323): Tungnárjökull. Jökull, vol. 18, p. 371-378`

- For references with a DOI, include at least the DOI. The suggested format is `{author} {year} (https://doi.org/{doi})`, where `author` can be as simple as the first author's family name.
- For references without a DOI, include additional information. The suggested format (for a journal article) is `{author} {year} ({url}): {title}. {journal}, {volume}, {issue}, {pages}, ...`.
- Separate references with a pipe (`|`).

### Remarks

Remarks (columns named `remarks`) can contain anything, but some conventions are encouraged to make them more human and machine readable, as illustrated by the following example:

`annual_balance: Includes -0.25 m due to calving | area: Surveyed on 1986-12-31`

- Separate remarks with a pipe (`|`).
- For remarks specific to a column, prefix the remark with the column name.
- Do not include information that can be expressed elsewhere. For example, `Day and month unknown` can be expressed in the `date` column (see [Dates](#dates)).

<div class='break-after'></div>

## Tables

{% for resource in package.resources %}
[`{{ resource.name }}`](#{{ resource.name }}){% if not loop.last %} · {% endif %}
{% endfor %}{{ "\n" }}

{% for resource in package.resources %}
  {% include 'resource-submit.md.jinja' %}
  {% if not loop.last %}
  <div class='break-after'></div>
  {% endif %}
{% endfor %}
