"""
Randolph Glacier Inventory (RGI) 7.0

See https://nsidc.org/data/nsidc-0770/versions/7.
"""
import zipfile

import geopandas as gpd
import pandas as pd

from fog.imports.base import Import, request_file_nsidc


class RGI7Glacier(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0770_rgi_v7/global_files/RGI2000-v7.0-G-global.zip',
      name='rgi7_glacier',
      table='rgi7_glacier',
      request=request_file_nsidc
    )

  def _extract(self) -> pd.DataFrame:
    gdfs = []
    with zipfile.ZipFile(self.download_path) as parent_zip:
      for child_name in parent_zip.namelist():
        if not child_name.endswith('.zip'):
          continue
        print(child_name)
        with parent_zip.open(child_name, "r") as child_zip:
          gdfs.append(gpd.read_file(child_zip))
    return pd.concat(gdfs, ignore_index=True)

  def _transform(self) -> pd.DataFrame:
    columns = {
      'rgi_id': 'id',
      'glims_id': 'glims_glacier_id',
      'anlys_id': 'glims_outline_id'
    }
    df = pd.read_parquet(self.extract_path, columns=columns.keys())
    df.rename(columns=columns, inplace=True)
    return df


class RGI7Complex(RGI7Glacier):

  def __init__(self) -> None:
    super(RGI7Glacier, self).__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0770_rgi_v7/global_files/RGI2000-v7.0-C-global.zip',
      name='rgi7_complex',
      table='rgi7_complex',
      request=request_file_nsidc
    )

  def _transform(self) -> pd.DataFrame:
    columns = {'rgi_id': 'id'}
    df = pd.read_parquet(self.extract_path, columns=columns.keys())
    df.rename(columns=columns, inplace=True)
    return df
