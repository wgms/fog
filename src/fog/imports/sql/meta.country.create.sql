create table meta.country (
  id char(2) primary key check (id ~ '^[A-Z]{2}$'),
  name text not null unique check (name ~ '^\S+( \S+)*$'),
  polygon geometry(multipolygon, 4326) not null check (st_isvalid(polygon) and not st_isempty(polygon))
);

create index on meta.country using gist (polygon);
