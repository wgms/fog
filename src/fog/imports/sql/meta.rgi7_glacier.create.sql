create table meta.rgi7_glacier (
  id text primary key,
  glims_glacier_id text,
  glims_outline_id integer
);
