create table meta.gtng_subregion_2023 (
  id text not null primary key check (id ~ '^[0-2][0-9]-[0-9]{2}$' and substring(id, 1, 2) = gtng_region_id),
  long_id text not null check (long_id ~ '^[0-2][0-9]-[0-9]{2}_[a-z0-9_]+$'),
  gtng_region_id text not null references meta.gtng_region_2023 (id),
  name text not null check (name ~ '^\S+( \S+)*$'),
  polygon geometry(multipolygon, 4326) not null check (st_isvalid(polygon) and not st_isempty(polygon))
);

create index on gtng_subregion_2023 using gist (polygon);

create view glacier_region_2023 as
select
  glacier.id as glacier_id,
  gtng_subregion_2023.gtng_region_id,
  gtng_subregion_2023.id as gtng_subregion_id
from glacier
join gtng_subregion_2023 on st_intersects(glacier.point, gtng_subregion_2023.polygon);
