create table if not exists meta.glims_glacier (
  id text primary key,
  latitude numeric not null check (latitude between -90 and 90),
  longitude numeric not null check (longitude between -180 and 180),
  -- generated columns
  point geometry(point, 4326) not null generated always as (ST_SetSRID(ST_MakePoint(longitude, latitude), 4326)) stored
);
