create table meta.gtng_region (
  id text not null primary key check (id ~ '^[A-Z]{3}$'),
  name text not null check (name ~ '^\S+( \S+)*$'),
  polygon geometry(multipolygon, 4326) not null check (st_isvalid(polygon) and not st_isempty(polygon))
);

create index on gtng_region using gist (polygon);
