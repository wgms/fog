create table meta.wgi_glacier (
  id text primary key,
  name text check (name ~ '^\S+( \S+)*$'),
  latitude numeric not null,
  longitude numeric not null,
  -- generated columns
  point geometry(point, 4326) not null generated always as (ST_SetSRID(ST_MakePoint(longitude, latitude), 4326)) stored
);
