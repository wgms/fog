create table meta.gtng_region_2023 (
  id text not null primary key check (id ~ '^[0-2][0-9]$'),
  long_id text not null check (long_id ~ '^[0-2][0-9]_[a-z_]+$'),
  name text not null check (name ~ '^\S+( \S+)*$'),
  polygon geometry(multipolygon, 4326) not null check (st_isvalid(polygon) and not st_isempty(polygon))
);

create index on gtng_region_2023 using gist (polygon);
