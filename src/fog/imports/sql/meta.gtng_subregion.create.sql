create table meta.gtng_subregion (
  id text not null primary key check (id ~ '^[A-Z]{3}-[0-9]{2}$' and substring(id, 1, 3) = gtng_region_id),
  gtng_region_id text not null references meta.gtng_region (id),
  name text not null check (name ~ '^\S+( \S+)*$'),
  polygon geometry(multipolygon, 4326) not null check (st_isvalid(polygon) and not st_isempty(polygon))
);

create index on gtng_subregion using gist (polygon);

create view glacier_region as
select
  glacier.id as glacier_id,
  gtng_subregion.gtng_region_id,
  gtng_subregion.id as gtng_subregion_id
from glacier
join gtng_subregion on st_intersects(glacier.point, gtng_subregion.polygon);
