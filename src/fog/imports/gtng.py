"""
Global Terrestrial Network for Glaciers (GTN-G) Glacier regions

See https://www.gtn-g.ch/data_catalogue_glacreg.
"""
import geopandas as gpd
import shapely.validation

from fog.imports.base import Import, multipolygonize


class GTNGRegion2017(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://www.gtn-g.ch/database/GlacReg_2017.zip',
      name='gtng_region_2017',
      table='gtng_region'
    )

  def _extract(self) -> gpd.GeoDataFrame:
    path = self.download_path
    gdf = gpd.read_file(f'{path}!GTN-G_glacier_regions_201707.shp').to_crs(4326)
    # Fix invalid geometries
    for i in gdf.index:
      code = gdf['WGMS_CODE'][i]
      if not gdf.geometry[i].is_valid:
        details = shapely.validation.explain_validity(gdf.geometry[i])
        print(f'Fixing invalid {code} geometry ({details})')
      gdf.geometry[i] = shapely.validation.make_valid(gdf.geometry[i])
    return gdf

  def _transform(self) -> gpd.GeoDataFrame:
    gdf = self.extract()
    # Dissolve by code
    gdf = gdf.dissolve(by=['WGMS_CODE'], aggfunc='first', as_index=False, sort=False)
    gdf.geometry = gdf.geometry.apply(multipolygonize)
    columns = {
      'WGMS_CODE': 'id',
      'geometry': 'polygon',
      'FULL_NAME': 'name'
    }
    return (
      gdf.reindex(columns=columns)
      .rename(columns=columns)
      .set_geometry('polygon')
      .convert_dtypes()
      .sort_values('id')
    )


class GTNGSubregion2017(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://www.gtn-g.ch/database/GlacReg_2017.zip',
      name='gtng_subregion_2017',
      table='gtng_subregion'
    )

  def _extract(self) -> gpd.GeoDataFrame:
    path = self.download_path
    gdf = gpd.read_file(f'{path}!GTN-G_glacier_subregions_201707.shp').to_crs(4326)
    # Fix invalid geometries
    for i in gdf.index:
      code = gdf['WGMS_CODE'][i]
      if not gdf.geometry[i].is_valid:
        details = shapely.validation.explain_validity(gdf.geometry[i])
        print(f'Fixing invalid {code} geometry ({details})')
      gdf.geometry[i] = shapely.validation.make_valid(gdf.geometry[i])
    # Cut out Greenland ice sheet from Greenland periphery
    outer = gdf.index[gdf['WGMS_CODE'] == 'GRL-01'][0]
    inner = gdf.index[gdf['WGMS_CODE'] == 'GRL-11'][0]
    gdf.geometry[outer] = gdf.geometry[outer].difference(gdf.geometry[inner])
    return gdf

  def _transform(self) -> gpd.GeoDataFrame:
    gdf = self.extract()
    # Dissolve by code
    gdf = gdf.dissolve(by=['WGMS_CODE'], aggfunc='first', as_index=False, sort=False)
    gdf.geometry = gdf.geometry.apply(multipolygonize)
    # Extract region code
    gdf['gtng_region_id'] = gdf['WGMS_CODE'].str.slice(0, 3)
    columns = {
      'WGMS_CODE': 'id',
      'geometry': 'polygon',
      'FULL_NAME': 'name',
      'gtng_region_id': 'gtng_region_id'
    }
    return (
      gdf.reindex(columns=columns)
      .rename(columns=columns)
      .set_geometry('polygon')
      .convert_dtypes()
      .sort_values('id')
    )


class GTNGRegion2023(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://www.gtn-g.ch/database/GlacReg_2023.zip',
      name='gtng_region_2023',
    )

  def _extract(self) -> gpd.GeoDataFrame:
    path = self.download_path
    return gpd.read_file(f'{path}!GTN-G_202307_o1regions.shp').to_crs(4326)

  def _transform(self) -> gpd.GeoDataFrame:
    gdf = self.extract()
    # Dissolve by code
    gdf = gdf.dissolve(by=['o1region'], aggfunc='first', as_index=False, sort=False)
    gdf.geometry = gdf.geometry.apply(multipolygonize)
    columns = {
      'o1region': 'id',
      'long_code': 'long_id',
      'full_name': 'name',
      'geometry': 'polygon'
    }
    return (
      gdf.reindex(columns=columns)
      .rename(columns=columns)
      .set_geometry('polygon')
      .convert_dtypes()
      .sort_values('id')
    )


class GTNGSubregion2023(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://www.gtn-g.ch/database/GlacReg_2023.zip',
      name='gtng_subregion_2023',
    )

  def _extract(self) -> gpd.GeoDataFrame:
    path = self.download_path
    return gpd.read_file(f'{path}!GTN-G_202307_o2regions.shp').to_crs(4326)

  def _transform(self) -> gpd.GeoDataFrame:
    gdf = self.extract()
    # Dissolve by code
    gdf = gdf.dissolve(by=['o2region'], aggfunc='first', as_index=False, sort=False)
    gdf.geometry = gdf.geometry.apply(multipolygonize)
    columns = {
      'o2region': 'id',
      'long_code': 'long_id',
      'o1region': 'gtng_region_id',
      'full_name': 'name',
      'geometry': 'polygon'
    }
    return (
      gdf.reindex(columns=columns)
      .rename(columns=columns)
      .set_geometry('polygon')
      .convert_dtypes()
      .sort_values('id')
    )
