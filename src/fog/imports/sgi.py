"""
Swiss Glacier Inventory (SGI)

See https://doi.glamos.ch.
"""
import datetime
import geopandas as gpd

from fog.imports.base import Import


class SGI1973(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://doi.glamos.ch/data/inventory/inventory_sgi1973_r1976.zip',
      name='sgi1973'
    )

  def _extract(self) -> gpd.GeoDataFrame:
    path = self.download_path
    return (
      gpd.read_file(f'{path}!SGI_1973.shp')
      .convert_dtypes()
      .to_crs(4326)
    )

  def _transform(self) -> gpd.GeoDataFrame:
    gdf = self.extract()
    return gpd.GeoDataFrame({
      'original_id': gdf['OBJECTID'],
      'polygon': gdf['geometry'],
      # https://www.tandfonline.com/doi/full/10.1657/1938-4246-46.4.933
      # "aerial photography data collected in early September 1973"
      'date_min': datetime.datetime(1973, 9, 1),
      'date_max': datetime.datetime(1973, 9, 30),
      'reference': 'Müller, Fritz; Caflish, Toni; Müller, Gerhard (1976). Swiss Glacier Inventory 1973. Geographisches Institut, Eidgenössische Technische Hochschule Zürich (ETHZ). https://doi.org/10.18750/inventory.sgi1973.r1976'
    }).set_geometry('polygon')


class SGI2010(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://doi.glamos.ch/data/inventory/inventory_sgi2010_r2010.zip',
      name='sgi2010'
    )

  def _extract(self) -> gpd.GeoDataFrame:
    path = self.download_path
    return (
      gpd.read_file(f'{path}!SGI_2010.shp')
      .convert_dtypes()
      .to_crs(4326)
    )

  def _transform(self) -> gpd.GeoDataFrame:
    gdf = self.extract()
    # # Zero pad SGI 1973 ID following dash
    # gdf['Parent1973'] = gdf['Parent1973'].str.replace(
    #   '\-([0-9][^0-9]*)$', '-0\\1', regex=True
    # )
    return gpd.GeoDataFrame({
      'original_id': gdf['SGI'],
      'geometry': gdf['geometry'],
      # https://www.tandfonline.com/doi/full/10.1657/1938-4246-46.4.933, Figure 1
      'date_min': datetime.datetime(2003, 1, 1),
      'date_max': datetime.datetime(2011, 12, 31),
      'reference': 'Fischer, Mauro; Huss, Matthias; Barboux, Chloe; Hoelzle, Martin (2010). Swiss Glacier Inventory 2010. Department of Geosciences, University of Fribourg. https://doi.org/10.18750/inventory.sgi2010.r2010'
    })
