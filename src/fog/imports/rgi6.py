"""
Randolph Glacier Inventory (RGI) 6.0

See https://nsidc.org/data/nsidc-0770/versions/6.
"""
import re
import zipfile

import geopandas as gpd
import pandas as pd

from fog.imports.base import (
  Import, request_file_nsidc, multipolygonize, snap_to_start_date, snap_to_end_date
)


class RGI6(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0770_rgi_v6/nsidc0770_00.rgi60.complete.zip',
      name='rgi6',
      request=request_file_nsidc
    )

  def _extract(self) -> pd.DataFrame:
    pattern = re.compile(r'^(?P<prefix>[^_]+)_(?P<name>[0-9]{2}_rgi60_[^\.]+)')
    gdfs = []
    with zipfile.ZipFile(self.download_path) as parent_zip:
      for child_name in parent_zip.namelist():
        layer = pattern.match(child_name).groupdict()
        if layer['name'].startswith('00'):
          continue
        print(layer['name'])
        with parent_zip.open(child_name, 'r') as child_zip:
          gdfs.append(gpd.read_file(child_zip))
    gdf = pd.concat(gdfs, ignore_index=True)
    # Fix invalid geometries
    invalid = ~gdf['geometry'].is_valid
    gdf.loc[invalid, 'geometry'] = gdf.loc[invalid, 'geometry'].make_valid()
    # Clean up multi-geometries
    mask = gdf['geometry'].type != 'Polygon'
    gdf.loc[mask, 'geometry'] = gdf.loc[mask, 'geometry'].apply(multipolygonize)
    # Fix dates
    gdf['BgnDate'] = gdf['BgnDate'].mask(gdf['BgnDate'].eq('-9999999'))
    gdf['EndDate'] = (
      gdf['EndDate']
      .mask(gdf['EndDate'].eq('-9999999'))
      .combine_first(gdf['BgnDate'])
    )
    # Set CRS explicitly
    gdf['geometry'].set_crs('EPSG:4326', inplace=True)
    return gdf

  def _transform(self) -> pd.DataFrame:
    gdf = self.extract()
    return gpd.GeoDataFrame({
      'original_id': gdf['RGIId'],
      'original_glacier_id': gdf['GLIMSId'],
      'glacier_name': gdf['Name'],
      'geometry': gdf['geometry'],
      'date_min': gdf['BgnDate'].apply(lambda x: pd.NA if pd.isnull(x) else snap_to_start_date(x)),
      'date_max': gdf['EndDate'].apply(lambda x: pd.NA if pd.isnull(x) else snap_to_end_date(x)),
      'reference': 'RGI Consortium (2017). Randolph Glacier Inventory - A Dataset of Global Glacier Outlines, Version 6. National Snow and Ice Data Center. https://doi.org/10.7265/4m1f-gd79'
    }).set_geometry('polygon')
