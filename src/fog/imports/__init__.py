from .wgi import WGI
from .rgi5 import RGI5
from .rgi6 import RGI6
from .rgi7 import RGI7Glacier, RGI7Complex
from .glims import GLIMSPointNorth, GLIMSPointSouth, GLIMSPolygonNorth, GLIMSPolygonSouth
from .sgi import SGI1973, SGI2010
from .osm import OSMCountry
from .gtng import GTNGRegion2017, GTNGSubregion2017, GTNGRegion2023, GTNGSubregion2023

__all__ = [
  'WGI',
  'RGI5',
  'RGI6',
  'RGI7Glacier',
  'RGI7Complex',
  'GLIMSPointNorth',
  'GLIMSPointSouth',
  'GLIMSPolygonNorth',
  'GLIMSPolygonSouth',
  'SGI1973',
  'SGI2010',
  'OSMCountry',
  'GTNGRegion2017',
  'GTNGSubregion2017',
  'GTNGRegion2023',
  'GTNGSubregion2023'
]
