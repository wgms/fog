"""
World Glacier Inventory (WGI)

See https://nsidc.org/data/glacier_inventory/.
"""
import pandas as pd

from fog.imports.base import Import, request_file_nsidc


class WGI(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://masie_web.apps.nsidc.org/pub/DATASETS/NOAA/G01130/wgi_feb2012.csv',
      name='wgi',
      table='wgi_glacier',
      request=request_file_nsidc
    )

  def _extract(self) -> pd.DataFrame:
    return pd.read_csv(self.download_path, encoding='latin-1').convert_dtypes()

  def _transform(self) -> pd.DataFrame:
    columns = {
      'wgi_glacier_id': 'id',
      'glacier_name': 'name',
      'lat': 'latitude',
      'lon': 'longitude'
    }
    df = (
      self.extract()
      .reindex(columns=columns)
      .rename(columns=columns)
      # Some glaciers have multiple entries
      .drop_duplicates()
      .sort_values('id')
    )
    # Trim whitespace in names
    df['name'] = df['name'].str.strip().str.replace('\s+', ' ', regex=True)
    return df.convert_dtypes()
