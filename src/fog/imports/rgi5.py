"""
Randolph Glacier Inventory (RGI) 5.0

See https://nsidc.org/data/nsidc-0770/versions/5.
"""
import io
import zipfile

import dbf_light
import fiona.io
import geopandas as gpd
import pandas as pd

from fog.imports.base import (
  Import, request_file_nsidc, multipolygonize, snap_to_start_date, snap_to_end_date
)


class RGI5(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0770_rgi_v5/nsidc0770_00.rgi50.complete.zip',
      name='rgi5',
      request=request_file_nsidc
    )

  def _extract(self) -> pd.DataFrame:
    gdfs = []
    with zipfile.ZipFile(self.download_path, 'r') as archive:
      for region_name in archive.namelist():
        print(region_name)
        region_data = archive.read(region_name)
        with fiona.io.ZipMemoryFile(region_data) as region:
          with region.open() as collection:
            shp = gpd.GeoDataFrame.from_features(collection, crs=collection.crs)
            # Shapefile is misconfigured: Read attributes from DBF
            dbf_name = f'{region.listlayers()[0]}.dbf'
            dbf_data = zipfile.ZipFile(io.BytesIO(region_data)).read(dbf_name)
            dbf = dbf_light.Dbf(io.BytesIO(dbf_data))
            # Skip over deletion flag in first line only (DBF malformed)
            # Otherwise, could do dbf.iter_rows()
            rows = []
            dbf._fileobj.read(1)
            for _ in range(dbf.prolog.records_count):
              values = []
              for field in dbf.fields:
                data = dbf._fileobj.read(field.len)
                values.append(field.cast(data))
              rows.append(dbf.cls_row(*values))
            df = pd.DataFrame(rows).astype({
              # Convert decimal to Float
              'cenlon': 'Float64',
              'cenlat': 'Float64',
              'area': 'Float64',
              'slope': 'Float64'
            }).convert_dtypes()
            assert df.shape[0] == shp.shape[0]
            gdfs.append(pd.concat((shp, df), axis=1))
    gdf = pd.concat(gdfs, ignore_index=True).to_crs(4326)
    # Fix invalid geometries
    invalid = ~gdf['geometry'].is_valid
    gdf.loc[invalid, 'geometry'] = gdf.loc[invalid, 'geometry'].make_valid()
    # Clean up multi-geometries
    mask = gdf['geometry'].type != 'Polygon'
    gdf.loc[mask, 'geometry'] = gdf.loc[mask, 'geometry'].apply(multipolygonize)
    # Fix dates
    gdf['bgndate'] = gdf['bgndate'].mask(gdf['bgndate'].eq('-9999999'))
    gdf['enddate'] = (
      gdf['enddate']
      .mask(gdf['enddate'].eq('-9999999'))
      .combine_first(gdf['bgndate'])
    )
    # Set CRS explicitly
    gdf['geometry'].set_crs('EPSG:4326', inplace=True)
    # Replace empty strings with NA
    gdf['name'].replace({'': pd.NA}, inplace=True)
    return gdf

  def _transform(self) -> pd.DataFrame:
    gdf = self.extract()
    return gpd.GeoDataFrame({
      'original_id': gdf['rgiid'],
      'original_glacier_id': gdf['glimsid'],
      'glacier_name': gdf['name'],
      'polygon': gdf['geometry'],
      'date_min': gdf['bgndate'].apply(lambda x: pd.NA if pd.isnull(x) else snap_to_start_date(x)),
      'date_max': gdf['enddate'].apply(lambda x: pd.NA if pd.isnull(x) else snap_to_end_date(x)),
      'reference': 'RGI Consortium (2015). Randolph Glacier Inventory - A Dataset of Global Glacier Outlines, Version 5. National Snow and Ice Data Center. https://doi.org/10.7265/gq4p-zz56'
    }).set_geometry('polygon')
