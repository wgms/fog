"""
Open Street Map (OSM) via the Overpass API.

See https://wiki.openstreetmap.org/wiki/Overpass_API.
"""
import json
import urllib.parse

import geopandas as gpd
import osm2geojson

from fog.imports.base import Import, multipolygonize


class OSMCountry(Import):

  def __init__(self) -> None:
    base_url = 'http://overpass-api.de/api/interpreter?data='
    query = 'relation["ISO3166-1:alpha2"];out body;>;out skel;'
    url = base_url + urllib.parse.quote(query)
    super().__init__(
      url=url,
      path='osm_country.xml',
      name='osm_country',
      table='country'
    )

  def _extract(self) -> gpd.GeoDataFrame:
    xml = self.download_path.read_text()
    # Convert Overpass XML to GeoJSON
    obj = osm2geojson.xml2geojson(xml)
    del xml
    txt = json.dumps(obj)
    del obj
    gdf = gpd.read_file(txt, driver='GeoJSON')
    del txt
    # Keep only country relations
    gdf = gdf[gdf['type'].eq('relation')][['tags', 'geometry']]
    # Keep only polygon geometries
    gdf = gdf[gdf['geometry'].type.str.contains('Polygon')]
    # Extract ISO3166-1:alpha2 tag
    gdf['ISO3166-1:alpha2'] = gdf['tags'].apply(lambda x: x.get('ISO3166-1:alpha2'))
    # Extract name tag
    gdf['name'] = gdf['tags'].apply(lambda x: x.get('name:en') or x.get('name'))
    return gdf

  def _transform(self) -> gpd.GeoDataFrame:
    gdf = self.extract()
    # Match WGMS political unit conventions
    transfers = {
      'NO': 'SJ',
      'FR': 'TF',
      'AU': 'HM',
      # NOTE: Gives advantage to Argentina (https://en.wikipedia.org/wiki/Southern_Patagonian_Ice_Field_dispute)
      'CL': 'AR',
      # NOTE: Gives advantage to India (https://en.wikipedia.org/wiki/India%E2%80%93Nepal_border)
      'NP': 'IN'
    }
    for parent, child in transfers.items():
      mask = gdf['ISO3166-1:alpha2'].eq(parent)
      assert mask.sum() == 1
      i = gdf[mask].index[0]
      mask = gdf['ISO3166-1:alpha2'].eq(child)
      assert mask.sum() == 1
      j = gdf[mask].index[0]
      # Remove child from parent
      gdf.loc[i, 'geometry'] = gdf.loc[i, 'geometry'].difference(gdf.loc[j, 'geometry'])
    # Adjust country names
    NAME_REPLACEMENTS = {
      'Democratic Republic of the Congo': 'Congo (Democratic Republic)',
      'French Southern and Antarctic Lands': 'French Southern Territories'
    }
    gdf = gpd.GeoDataFrame({
      'id': gdf['ISO3166-1:alpha2'],
      'name': gdf['name'].str.replace(r' \(.+\)', '', regex=True).replace(NAME_REPLACEMENTS),
      'polygon': gdf['geometry']
    }).set_geometry('polygon')
    # Dissolve by id
    gdf = gdf.dissolve(by=['id'], aggfunc='first', as_index=False, sort=True)
    # Cast to multipolygon
    gdf.geometry = gdf.geometry.apply(multipolygonize)
    return gdf.convert_dtypes()
