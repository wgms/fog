"""
Global Land Ice Measurements from Space (GLIMS)

See https://nsidc.org/data/nsidc-0272/versions/1.
"""
import zipfile

import geopandas as gpd
import pandas as pd
import shapely

from fog.imports.base import Import, request_file_nsidc, multipolygonize


class GLIMSPolygonSouth(Import):

  def __init__(self) -> None:
    super().__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0272_GLIMS_v1/NSIDC-0272_glims_db_south_20241025_v01.0.zip',
      name='glims_polygons_south',
      request=request_file_nsidc
    )

  def _extract(self) -> pd.DataFrame:
    with zipfile.ZipFile(self.download_path) as archive:
      points = [
        name for name in archive.namelist()
        if name.endswith('glims_polygons.shp')
      ]
      name = points[0]
    gdf: gpd.GeoDataFrame = gpd.read_file(f'{self.download_path}!{name}')
    gdf.geometry = shapely.force_2d(gdf.geometry)
    return gdf.convert_dtypes()

  def _transform(self) -> pd.DataFrame:
    gdf = gpd.read_parquet(self.extract_path)
    # Ignore all feature types except glacier boundary and internal rock
    gdf = gdf[gdf['line_type'].isin(['glac_bound', 'intrnl_rock'])]
    # Repair invalid geometries
    is_invalid = ~gdf.geometry.is_valid
    gdf.loc[is_invalid, 'geometry'] = gdf.geometry[is_invalid].make_valid()
    # Set aside internal rock for later
    is_rock = gdf['line_type'].eq('intrnl_rock')
    rocks = gdf[is_rock]
    gdf = gdf[~is_rock]
    # Dissolve by anlys_id
    gdf = gdf.dissolve(
      by=['anlys_id'], aggfunc='first', as_index=True, sort=False
    )
    # # Cut out rocks
    # sindex = gdf.sindex
    # xi, xj = sindex.query(rocks, predicate='intersects')
    # for i, j in zip(xi, xj):
    #   print(i, j, end='\r', flush=True)
    #   gdf.geometry.iloc[j] = gdf.geometry.iloc[j].difference(rocks.iloc[i])
    # Cut out rocks by anlys_id
    # NOTE: This is slow. Should we be applying rock polygons as holes instead?
    groupby = rocks.groupby('anlys_id', sort=False)['geometry']
    for i, (key, index) in enumerate(groupby.groups.items()):
      print(i, end='\r', flush=True)
      shell = gdf.geometry.loc[key]
      if isinstance(shell, shapely.GeometryCollection):
        shell = multipolygonize(shell)
      if isinstance(shell, shapely.MultiPolygon):
        for rock in index:
          shell = shell.difference(rocks.geometry.loc[rock])
      else:
        shell = shapely.Polygon(shell=shell, holes=rocks.geometry.loc[index].to_list())
      gdf.geometry.loc[key] = shell

    for i, row in enumerate(rocks.to_dict(orient='records')):
      print(i, end='\r', flush=True)
      if row['anlys_id'] in gdf.index:
        gdf.geometry.loc[row['anlys_id']] = gdf.geometry.loc[row['anlys_id']].difference(row['geometry'])
    # Clean multi-geometries and convert to multipolygon
    mask = gdf.geometry.type != 'Polygon'
    gdf.loc[mask, 'geometry'] = gdf.loc[mask, 'geometry'].apply(multipolygonize)
    # Remove empty or null geometries
    mask = gdf.geometry.isnull() | gdf.geometry.is_empty
    gdf = gdf[~mask]
    # Compute area
    gdf['db_area'] = gdf.geometry.to_crs({'proj': 'cea'}).area * 1e-6
    # Ensure CRS is exactly the same as for other inventories
    gdf.geometry.set_crs('EPSG:4326', inplace=True)
    gdf = gdf.reset_index()
    return gpd.GeoDataFrame({
      'original_id': gdf['anlys_id'],
      'original_glacier_id': gdf['glac_id'],
      'glacier_name': gdf['glac_name'].str.strip().str.replace('\s+', ' ', regex=True),
      'polygon': gdf['geometry'],
      'date_min': gdf['src_date'],
      'date_max': gdf['src_date'],
      'reference': 'GLIMS Consortium (2005). GLIMS Glacier Database, Version 1 [2021-09-14]. National Snow and Ice Data Center. https://doi.org/10.7265/N5V98602'
    }).set_geometry('polygon')


class GLIMSPolygonNorth(GLIMSPolygonSouth):

  def __init__(self) -> None:
    super(GLIMSPolygonSouth, self).__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0272_GLIMS_v1/NSIDC-0272_glims_db_north_20241025_v01.0.zip',
      name='glims_polygons_north',
      request=request_file_nsidc
    )


class GLIMSPointSouth(Import):
  LATITUDE_RANGE = (-90, 0)
  POLYGON = GLIMSPolygonSouth

  def __init__(self) -> None:
    super().__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0272_GLIMS_v1/NSIDC-0272_glims_db_south_20241025_v01.0.zip',
      name='glims_points_south',
      table='glims_glacier',
      request=request_file_nsidc
    )

  def _extract(self) -> pd.DataFrame:
    with zipfile.ZipFile(self.download_path) as archive:
      points = [
        name for name in archive.namelist()
        if name.endswith('glims_points.shp')
      ]
      name = points[0]
    gdf: gpd.GeoDataFrame = gpd.read_file(f'{self.download_path}!{name}')
    gdf.geometry = shapely.force_2d(gdf.geometry)
    return gdf.convert_dtypes()

  def _transform(self) -> pd.DataFrame:
    gdf = gpd.read_parquet(self.extract_path)
    # Add names
    gdf = gdf.merge(
      # NOTE: Avoid load_glims for speed
      pd.read_parquet(
        self.POLYGON().extract_path,
        columns=['glac_id', 'glac_name']
      ).drop_duplicates(),
      how='left',
      left_on='glacier_id',
      right_on='glac_id',
      validate='one_to_one'
    )
    gdf.rename(columns={'glacier_id': 'id', 'glac_name': 'glacier_name'}, inplace=True)
    gdf['latitude'] = gdf['geometry'].y
    gdf['longitude'] = gdf['geometry'].x
    gdf.drop(columns='geometry', inplace=True)
    gdf.drop_duplicates(inplace=True)
    # Normalize longitudes
    out_of_bounds = ~gdf['longitude'].between(-180, 180)
    gdf.loc[out_of_bounds, 'longitude'] = 360 - gdf.loc[out_of_bounds, 'longitude']
    # Filter latitudes
    gdf = gdf[gdf['latitude'].between(*self.LATITUDE_RANGE)]
    return pd.DataFrame(gdf)


class GLIMSPointNorth(GLIMSPointSouth):
  LATITUDE_RANGE = (0, 90)
  POLYGON = GLIMSPolygonNorth

  def __init__(self) -> None:
    super(GLIMSPointSouth, self).__init__(
      url='https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0272_GLIMS_v1/NSIDC-0272_glims_db_north_20241025_v01.0.zip',
      name='glims_points_north',
      table='glims_glacier',
      request=request_file_nsidc
    )

