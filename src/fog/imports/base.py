import abc
import calendar
import datetime
from typing import Callable, Union
from pathlib import Path
import urllib.request
import http.client
import hashlib
import base64
import importlib.resources

import pandas as pd
import geopandas as gpd
import shapely.geometry
import sqlalchemy


import fog.config


def download_file(
  url: str,
  path: str | Path,
  overwrite: bool = False,
  request: Callable[[str], http.client.HTTPResponse] = urllib.request.urlopen
) -> None:
  """
  Download a file from a URL to a local path.

  Computes an MD5 hash of the downloaded file and stores it in a file with the
  same name as the downloaded file but with an `.md5` extension. If the file
  already exists and the hash matches, the download is skipped.

  Parameters
  ----------
  url
    URL of the file to download.
  path
    Local path to save the downloaded file.
  overwrite
    Whether to redownload the file if it already exists (regardless of hash).

  Returns
  -------
  MD5 hash of the downloaded file, or None if download was skipped or hashes matched.
  """
  old_hash = None
  md5_path = Path(f'{path}.md5')
  # Prepare download path
  path = Path(path)
  if path.exists():
    if not overwrite:
      return None
    # Load previous md5 hash
    if md5_path.exists():
      old_hash = md5_path.read_text()
  path.parent.mkdir(parents=True, exist_ok=True)
  # Download file and compute hash
  response = request(url)
  md5 = hashlib.md5()
  counter = 1
  with open(path, 'wb') as file:
    while True:
      data = response.read(2**20)
      if not data:
        break
      print(counter, end='\r', flush=True)
      file.write(data)
      md5.update(data)
      counter += 1
  response.close()
  # Compare hash
  new_hash = md5.hexdigest()
  if new_hash != old_hash:
    md5_path.write_text(data=new_hash)
  return None


def request_file_nsidc(url: str) -> http.client.HTTPResponse:
  """
  Request a file from the NSIDC server.

  Requires `fog.config.EARTHDATA_USERNAME` and `fog.config.EARTHDATA_PASSWORD`.

  Parameters
  ----------
  url
    URL of the file to download.
  """
  if not fog.config.EARTHDATA_USERNAME or not fog.config.EARTHDATA_PASSWORD:
    raise ValueError(
      'Missing fog.config.EARTHDATA_USERNAME or fog.config.EARTHDATA_PASSWORD'
    )
  request = urllib.request.Request(url)
  authentication = f'{fog.config.EARTHDATA_USERNAME}:{fog.config.EARTHDATA_PASSWORD}'
  token = base64.b64encode(authentication.encode('ascii')).decode('ascii')
  request.add_header('Authorization', f'Basic {token}')
  opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor())
  return opener.open(request)


def read_parquet(path: str | Path) -> pd.DataFrame:
  """Read parquet as either GeoDataFrame or DataFrame."""
  try:
    return gpd.read_parquet(path)
  except ValueError:
    return pd.read_parquet(path)


def parquet_cache(
  path: Union[str, Path],
  read: Callable[[], pd.DataFrame],
  overwrite: bool = False
) -> pd.DataFrame:
  """
  Cache a DataFrame output by a function as a (Geo)Parquet file.

  Parameters
  ----------
  path
    Path to the Parquet file.
  read
    Function that returns a DataFrame.
  overwrite
    Whether to overwrite the file if it already exists.
  """
  if path.exists() and not overwrite:
    return read_parquet(path)
  df = read()
  path.parent.mkdir(parents=True, exist_ok=True)
  df.to_parquet(path)
  return df


def multipolygonize(geom: shapely.geometry.base.BaseGeometry) -> shapely.MultiPolygon:
  """
  Convert geometry to MultiPolygon, removing all zero-area components.

  Raises
  ------
  ValueError
    Geometry has zero area.
  """
  try:
    # Remove zero-area children
    geoms = [x for x in geom.geoms if x.area]
  except AttributeError:
    if not geom.area:
      return pd.NA
    geoms = [geom]
  if not geoms:
    return pd.NA
  return shapely.MultiPolygon(geoms)


def snap_to_start_date(x: str) -> datetime.date:
  """
  Examples
  --------
  >>> snap_to_start_date('20100101')
  datetime.date(2010, 1, 1)
  >>> snap_to_start_date('20100199')
  datetime.date(2010, 1, 1)
  >>> snap_to_start_date('20109999')
  datetime.date(2010, 1, 1)
  """
  year = int(x[:4])
  month = int(x[4:6])
  if month == 99:
    return datetime.date(year, 1, 1)
  day = int(x[6:8])
  if day == 99:
    return datetime.date(year, month, 1)
  return datetime.date(year, month, day)


def snap_to_end_date(x: str) -> datetime.date:
  """
  Examples
  --------
  >>> snap_to_end_date('20100101')
  datetime.date(2010, 1, 1)
  >>> snap_to_end_date('20100199')
  datetime.date(2010, 1, 31)
  >>> snap_to_end_date('20109999')
  datetime.date(2010, 12, 31)
  """
  year = int(x[:4])
  month = int(x[4:6])
  if month == 99:
    return datetime.date(year, 12, 31)
  day = int(x[6:8])
  if day == 99:
    return datetime.date(year, month, calendar.monthrange(year, month)[1])
  return datetime.date(year, month, day)


class Import(abc.ABC):

  def __init__(
    self,
    url: str,
    name: str,
    table: str | None = None,
    schema: str = 'meta',
    path: str | Path | None = None,
    request: Callable[[str], http.client.HTTPResponse] = urllib.request.urlopen
  ) -> None:
    self.url = url
    self.name = name
    self.table = table or name
    self.schema = schema
    self.path = path
    self.request = request

  @property
  def download_path(self) -> Path:
    path = self.path or self.url.split('/')[-1]
    return Path(fog.config.DOWNLOAD_PATH) / path

  @property
  def extract_path(self) -> Path:
    return Path(fog.config.EXTRACT_PATH) / f'{self.name}.parquet'

  @property
  def transform_path(self) -> Path:
    return Path(fog.config.TRANSFORM_PATH) / f'{self.name}.parquet'

  def download(self, overwrite: bool = False) -> None:
    return download_file(
      url=self.url,
      path=self.download_path,
      request=self.request,
      overwrite=overwrite
    )

  @abc.abstractmethod
  def _extract(self) -> pd.DataFrame:
    pass

  def extract(self, overwrite: bool = False) -> pd.DataFrame:
    return parquet_cache(self.extract_path, read=self._extract, overwrite=overwrite)

  @abc.abstractmethod
  def _transform(self) -> pd.DataFrame:
    pass

  def transform(self, overwrite: bool = False) -> pd.DataFrame:
    return parquet_cache(self.transform_path, read=self._transform, overwrite=overwrite)

  def drop(self) -> None:
    """Drop the table from the database."""
    if not self.table:
      return NotImplementedError('Table not defined')
    db = fog.iodb.DB()
    if self.table not in db.metadata.tables:
      return None
    sql = f'drop table {self.schema}.{self.table} cascade'
    db.execute(sqlalchemy.sql.text(sql))
    db.commit()
    db.connection.close()

  def load(self, append: bool = False, chunksize: int = 50000) -> None:
    """Load the data into the database."""
    if not self.table:
      return NotImplementedError('Table not defined')
    db = fog.iodb.DB()
    if self.table in db.metadata.tables:
      if not append:
        raise ValueError(f'Table {self.table} already exists')
    else:
      # Create table
      sql_path = (
        importlib.resources.files('fog.imports.sql') /
        f'{self.schema}.{self.table}.create.sql'
      )
      sql = sql_path.read_text()
      db.execute(sqlalchemy.sql.text(sql))
    # Load data
    df = self.transform()
    if isinstance(df, gpd.GeoDataFrame):
      df = df.to_wkb()
    df.to_sql(
      self.table,
      schema=self.schema,
      con=db.connection,
      chunksize=chunksize,
      if_exists='append',
      index=False,
      method='multi'
    )
    db.commit()
    db.connection.close()

  def finalize(self) -> None:
    """Run final SQL commands."""
    if not self.table:
      return NotImplementedError('Table not defined')
    sql_path = (
      importlib.resources.files('fog.imports.sql') /
      f'{self.schema}.{self.table}.finalize.sql'
    )
    if not sql_path.exists():
      return
    sql = sql_path.read_text()
    db = fog.iodb.DB()
    db.execute(sqlalchemy.sql.text(sql))
    db.commit()
    db.connection.close()
