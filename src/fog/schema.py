import copy
import inspect
import re

from validator import Check, Column, Schema, Table, Tables
import validator.convert

import fog.checks
import fog.iodb
import fog.metadata
import fog.migrations


DATE_COLUMNS = ['date', 'begin_date', 'end_date', 'midseason_date']
"""
String columns that represent dates (in WGMS fuzzy format).
"""

SERIAL_KEY_COLUMNS = [
  ('state', 'id'),
  ('state_band', 'state_id'),
  ('change', 'id'),
  ('change_band', 'change_id'),
  ('event', 'id'),
  ('front_variation', 'series_id'),
]
"""
Columns that are not strictly required since they are incremented by database.
"""

GLACIER_NAME_LOOKUP = 'glacier_name'
"""
Redundant name column used to double check glacier identifier.
"""

MIGRATED_COLUMNS = [
  ('glacier', 'country'),
  ('glacier', 'name'),
  (None, GLACIER_NAME_LOOKUP),
  *[(None, date) for date in DATE_COLUMNS],
  *[(None, f'{date}_unc') for date in DATE_COLUMNS],
  (None, 'references'),
  (None, 'investigators'),
  (None, 'agencies')
]
"""
Submitted columns that are dropped or transformed in some way.
"""

SUBMISSION_METADATA = fog.metadata.read(submission=True, deprecated=False)
"""
Datapackage metadata for submissions.
"""

EXTENDED_SUBMISSION_METADATA = fog.metadata.read(submission='extended', deprecated=True)
"""
Datapackage metadata for extended submissions.
"""

EXTENDED_SUBMISSION_NAMES = {
  resource['name']: [field['name'] for field in resource['schema']['fields']]
  for resource in EXTENDED_SUBMISSION_METADATA['resources']
}
"""
Table and column names for extended submissions.
"""

OUTPUT_METADATA = fog.metadata.read(submission=False, deprecated=True)
"""
Datapackage metadata for database.
"""

OUTPUT_METADATA_NAMES = {
  resource['name']: [field['name'] for field in resource['schema']['fields']]
  for resource in OUTPUT_METADATA['resources']
}
"""
Table and column names for database.
"""

GENERATED_TABLES = [
  'bibliography', 'bibliography_reference',
  'team', 'team_member',
  'glacier_outline',
  'outline'
]
"""
Tables that can be added to a submission during migration
"""

CLEAN_SUBMISSION = Schema({
  # Strip and squeeze all whitespace
  Column(): Check.squeeze_whitespace(),
  # Drop empty rows or columns
  Table(): [
    Check.columns_not_null(drop=True),
    Check.rows_not_null(drop=True)
  ],
  # Drop empty tables
  Tables(): Check.drop_empty_tables()
})
"""
Clean and format submission.

Applied before migrations. Must be robust to the presence of 'NULL' strings.
"""

DROP_UNSUPPORTED_NULL_STRINGS = Schema({
  # ---- Reject NULL strings in select columns
  Column(column, table=table): Check.is_not_null_string(
    message="'NULL' cannot (yet) be used in this column",
    drop=True
  )
  for table, column in [
    ('glacier', 'country'),
    ('glacier', 'name'),
    (None, GLACIER_NAME_LOOKUP),
    *[(None, date) for date in DATE_COLUMNS],
    *[(None, f'{date}_unc') for date in DATE_COLUMNS],
    (None, 'references'),
    (None, 'investigators'),
    (None, 'agencies')
  ]
})
"""
Check for (and drop) 'NULL' strings in select columns.
"""

PARSE_SUBMISSION = Schema({
  # Cast select string columns to uppercase
  **{
    Column(name): Check.uppercase()
    for name in [
      'psfg_id', 'wgi_id', 'glims_id', 'rgi50_id', 'rgi60_id',
      'expos_acc_area', 'expos_abl_area',
      'short_name'
    ]
  },
  # Cast select string columns to lowercase
  **{
    Column(name): Check.lowercase()
    for name in [
      'terminus_type',
      'method', 'begin_method', 'end_method',
      'platform', 'begin_platform', 'end_platform',
      'length_change_direction',
      'time_system',
      'ela_position',
      'balance_code'
    ]
  },
  # Parse columns to correct data type
  # NOTE: Vector constraints (required, unique) are checked after merge with database
  **{
    Column(field['name'], table=resource['name']):
    validator.convert.frictionless.field_to_schema(
      {
        **field,
        'constraints': {
          key: value for key, value in field.get('constraints', {}).items()
          if key not in ('required', 'unique')
        }
      }
    ).schema
    for resource in EXTENDED_SUBMISSION_METADATA['resources']
    for field in resource['schema']['fields']
  }
})
"""
Parse submission to correct data types in preparation for merge with database.

Assumes that 'NULL' strings have been removed.
"""

CHECK_SUBMITTED_DATES = Schema({
  # ---- Dates
  # Check that dates (which matched regex) actually exist
  **{
    Column(column): [
      Check.format_date(),
      Check.date_exists()
    ]
    for name in DATE_COLUMNS
    for column in [name, f'{name}_min', f'{name}_max']
  },
  # Check that dates are in the correct order
  Table(): [
    Check.less_than_fuzzydate(
      date='begin_date', date_unc='begin_date_unc',
      other='end_date', other_unc='end_date_unc'
    ),
    Check.less_than_fuzzydate(
      date='begin_date', date_unc='begin_date_unc',
      other='midseason_date', other_unc='midseason_date_unc'
    ),
    Check.less_than_fuzzydate(
      date='midseason_date', date_unc='midseason_date_unc',
      other='end_date', other_unc='end_date_unc'
    )
  ],
  # Check that dates are only submitted using one method
  # (except for state and change tables)
  **{
    Column(column, table): Check.null_if_any_columns_not_null(
        columns=[f'{column}_min', f'{column}_max'],
        message='Cannot provide both date and min-max dates'
      )
    for column in DATE_COLUMNS
    for table in EXTENDED_SUBMISSION_NAMES
    if table not in ('state', 'change') and column in EXTENDED_SUBMISSION_NAMES[table]
  },
  # Check that date_unc is null if date min-max are not null
  **{
    Column(f'{column}_unc'): Check.null_if_any_columns_not_null(
      columns=[f'{column}_min', f'{column}_max'],
      message='Cannot provide both date uncertainty and min-max dates'
    )
    for column in DATE_COLUMNS
  }
})
"""
Check submitted dates.
"""

PRE_MERGE_MIGRATION = Schema({
  # Convert date and uncertainty to min-max dates
  # (except for state_band and change_band tables)
  **{
    Table(table): Check.convert_date_unc_to_min_max(
      date=column,
      unc=f'{column}_unc',
      date_min=f'{column}_min',
      date_max=f'{column}_max'
    )
    for table in (
      'state', 'change', 'mass_balance', 'mass_balance_point',
      'front_variation', 'event'
    )
    for column in DATE_COLUMNS
  },
  # Require required columns not present in output metadata
  **{
    Column(GLACIER_NAME_LOOKUP, table): Check.not_null()
    for table, columns in EXTENDED_SUBMISSION_NAMES.items()
    if GLACIER_NAME_LOOKUP in columns
  },
  Column('glacier_id', 'state_band'): Check.not_null(),
  Column('glacier_id', 'change_band'): Check.not_null()
})
"""
Submission migration before merge with database.
"""

POST_MERGE_MIGRATION = Schema({
  # Drop date and date uncertainty columns (in favor of min-max dates)
  Table(): Check.drop_columns(
    columns=DATE_COLUMNS + [f'{column}_unc' for column in DATE_COLUMNS]
  ),
  # Check and drop glacier name lookup columns
  Table(): {
    Column(GLACIER_NAME_LOOKUP): Check.lookup_glacier_name(
      glacier_id='glacier_id',
      glacier={'table': 'glacier', 'id': 'id', 'name': 'name', 'short_name': 'short_name'},
      name={'table': 'glacier_name', 'glacier_id': 'glacier_id', 'name': 'name'}
    ),
    Table(): Check.drop_columns([GLACIER_NAME_LOOKUP])
  },
  # Check submitted glacier name (either short_name or glacier_name.name)
  Column('name', table='glacier'): Check.glacier_name_exists(
    glacier={'id': 'id', 'short_name': 'short_name'},
    name={'table': 'glacier_name', 'glacier_id': 'glacier_id', 'name': 'name'}
  ),
  # # Move new glacier.name to glacier_name.name
  # Tables(): Check.tabulate_glacier_names(
  #   glacier={'table': 'glacier', 'id': 'id', 'name': 'name', 'short_name': 'short_name'},
  #   name={'table': 'glacier_name', 'id': 'glacier_id', 'name': 'name'}
  # ),
  # Drop glacier.name column
  Table('glacier'): Check.drop_columns(['name']),
  # Drop glacier id from band tables
  Table('state_band'): Check.drop_columns(['glacier_id']),
  Table('change_band'): Check.drop_columns(['glacier_id'])
})
"""
Check and drop (or migrate) remaining submission-only columns.
"""

CUSTOM_ROW_CHECKS = Schema({
  # ---- Dates
  **{
    Column(f'{column}_min'): Check.less_than_or_equal_to_column(
      column=f'{column}_max',
      message='Value is after {column}'
    )
    for column in DATE_COLUMNS
  },
  # ---- Elevation bounds
  Column('lower_elevation'): Check.less_than_column('upper_elevation'),
  # ---- State
  Table('state'): {
    Column('lowest_elevation'): Check.less_than_column('highest_elevation'),
    Column('lowest_elevation'): Check.less_than_column('mean_elevation'),
    Column('mean_elevation'): Check.less_than_column('highest_elevation'),
  },
  Table('state_band'): {
    Column('mean_elevation'): Check.less_than_column('upper_elevation'),
    Column('lower_elevation'): Check.less_than_column('mean_elevation')
  },
  # ---- Change
  **{
    Table(table): {
      Column('volume_change'): Check.same_sign_as_column('elevation_change'),
      Column('volume_change'): Check.close_to_product_of_columns(
        ['area', 'elevation_change'], tolerance=0.1, tag='warning'
      ),
    } for table in ('change', 'change_band')
  },
  # ---- Mass balance
  **{
    Table(table): {
      Column('summer_balance'): [
        Check.less_than(0, tag='warning'),
        Check.less_than_column('winter_balance', tag='warning')
      ],
      Column('winter_balance'): Check.greater_than(0, tag='warning'),
      Column('annual_balance'): Check.close_to_sum_of_columns(
        ['winter_balance', 'summer_balance'], tolerance=0.1, tag='warning'
      )
    } for table in ('mass_balance', 'mass_balance_band')
  },
  Column('balance', table='mass_balance_point'): Check.not_null(tag='warning'),
  # ---- Front variation
  # Front variation
  Table('front_variation'): {
    Column('length_change_direction'):
    Check.consistent_with_quantitative_variation('length_change')
  },
  # ---- Remarks
  # platform, begin_platform, end_platform: other
  **{
    Column('remarks', table=table): [
      Check.not_null_if_column_equal_to(column, 'other', tag='warning')
      for column in columns
      if column in ('platform', 'begin_platform', 'end_platform')
    ]
    for table, columns in EXTENDED_SUBMISSION_NAMES.items()
  },
  # method, begin_method, end_method: other | reconstruction | historical
  **{
    Column('remarks', table=table): [
      Check.not_null_if_column_matches_regex(
        column, r'^other|reconstruction|historical$', tag='warning'
      )
      for column in columns
      if column in ('method', 'begin_method', 'end_method')
    ]
    for table, columns in EXTENDED_SUBMISSION_NAMES.items()
  },
  # date, begin_date, midseason_date, end_date: yyyy or yyyy-mm
  **{
    Column('remarks', table=table): [
      Check.not_null_if_column_matches_regex(
        column, r'^[0-9]{4}(-[0-9]{2})?$', tag='warning'
      )
      for column in columns
      if column in DATE_COLUMNS
    ]
    for table, columns in EXTENDED_SUBMISSION_NAMES.items()
  },
  # time_system: other
  Column('time_system', table='mass_balance'): Check.not_null_if_column_equal_to(
    'time_system', 'other', tag='warning'
  ),
  # ---- Uncertainties
  # Check that uncertainties are null if the value is null
  # Check that uncertainties are less than or equal to the value
  **{
    Column(column, table=table): [
      Check.null_if_column_null(re.sub('_unc$', '', column))
    ] + (
      [Check.less_than_or_equal_to_abs_column(re.sub('_unc$', '', column), tag='warning')]
      if re.sub('_unc$', '', column) not in DATE_COLUMNS else []
    )
    for table, columns in EXTENDED_SUBMISSION_NAMES.items()
    for column in columns
    if column.endswith('_unc') and re.sub('_unc$', '', column) in columns
  },
  # Special cases
  Column('elevation_unc', 'state'): [
    Check.null_if_columns_null(
      ['lowest_elevation', 'highest_elevation', 'mean_elevation']
    ),
    *[
      Check.less_than_or_equal_to_abs_column(column, tag='warning')
      for column in ('highest_elevation', 'mean_elevation')
    ]
  ],
  Column('elevation_unc', 'state_band'): [
    Check.less_than_or_equal_to_abs_column('mean_elevation', tag='warning')
  ],
  # ---- Zeros
  **{
    Column(column, table): Check.not_equal_to(0, tag='warning')
    for table, column in [
      ('state', 'area'),
      ('state', 'length'),
      ('state', 'lowest_elevation'),
      ('state_band', 'area'),
      ('change', 'area'),
      ('change_band', 'area'),
      ('mass_balance', 'area'),
      ('mass_balance_band', 'area')
    ]
  },
  # ---- Spatial coordinates
  Table('glacier'): {
    Column('latitude'): Check.minimum_decimals(3, tag='warning'),
    Column('longitude'): Check.minimum_decimals(3, tag='warning')
  },
  Table('mass_balance_point'): {
    Column('latitude'): Check.minimum_decimals(4, tag='warning'),
    Column('longitude'): Check.minimum_decimals(4, tag='warning')
  },
  # ---- Row completeness
  Table('glacier'): Check.columns_not_all_null(
    columns=['psfg_id', 'wgi_id', 'glims_id', 'rgi50_id', 'rgi60_id', 'rgi70_id'],
    tag='warning'
  ),
  Table('state'): Check.columns_not_all_null(
    columns=[
      'highest_elevation', 'mean_elevation', 'lowest_elevation',
      'length', 'area', 'terminus_type'
    ]
  ),
  Table('state_band'): Check.columns_not_all_null(
    columns=['mean_elevation', 'area']
  ),
  Table('change'): Check.columns_not_all_null(
    columns=['area_change', 'elevation_change', 'volume_change']
  ),
  Table('change_band'): Check.columns_not_all_null(
    columns=['area_change', 'elevation_change', 'volume_change']
  ),
  Table('front_variation'): Check.columns_not_all_null(
    columns=['length_change', 'length_change_direction']
  ),
  Table('mass_balance'): [
    Check.columns_not_all_null(
      columns=['ela', 'aar', 'annual_balance', 'summer_balance', 'winter_balance'],
      tag='warning'
    ),
    Check.columns_not_all_null(
      columns=['annual_balance', 'summer_balance', 'winter_balance'],
      tag='warning'
    )
  ],
  Table('mass_balance_band'): Check.columns_not_all_null_if_column_not_equal_to(
    columns=['annual_balance', 'summer_balance', 'winter_balance'],
    column='area',
    value=0,
    tag='warning'
  ),
  Table('event'): Check.columns_not_all_null(
    columns=[
      'description', 'surge', 'calving', 'flood', 'avalanche', 'rockfall',
      'debris_flow', 'earthquake', 'volcanic_eruption', 'other'
    ]
  )
})
"""
Custom single-row checks not covered by metadata.
"""


CUSTOM_GROUP_CHECKS = Schema({
  Table('state'): {
    Column('area'): Check.close_to_band_sum(
      table='state_band',
      column='area',
      by={'id': 'state_id'},
      bounds=['lower_elevation', 'upper_elevation'],
      tolerance=0.1,
      tag='warning'
    ),
    Column('mean_elevation'): Check.close_to_band_sum(
      table='state_band',
      column='mean_elevation',
      by={'id': 'state_id'},
      bounds=['lower_elevation', 'upper_elevation'],
      weights='area',
      tolerance=0.01,
      tag='warning'
    ),
    Column('mean_elevation'): Check.within_band_bounds(
      table='state_band',
      by={'id': 'state_id'},
      bounds=['lower_elevation', 'upper_elevation']
    )
  },
  Table('state_band'): [
    Check.bounds_do_not_overlap(
      by=['state_id'], bounds=['lower_elevation', 'upper_elevation']
    ),
    Check.bounds_are_continuous(
      by=['state_id'], bounds=['lower_elevation', 'upper_elevation'],
      tag='warning'
    )
  ],
  Table('change'): {
    **{
      Column(column): Check.close_to_band_sum(
        table='change_band',
        column=column,
        by={'id': 'change_id'},
        bounds=['lower_elevation', 'upper_elevation'],
        tolerance=0.1,
        tag='warning'
      )
      for column in ['area', 'area_change', 'volume_change']
    },
    Column('elevation_change'): [
      Check.within_band_range(
        table='change_band',
        column='elevation_change',
        by={'id': 'change_id'}
      ),
      Check.close_to_band_sum(
        table='change_band',
        column='elevation_change',
        by={'id': 'change_id'},
        bounds=['lower_elevation', 'upper_elevation'],
        weights='area',
        tolerance=0.1,
        tag='warning'
      )
    ],
  },
  Table('change_band'): [
    Check.bounds_do_not_overlap(
      by=['change_id'], bounds=['lower_elevation', 'upper_elevation']
    ),
    Check.bounds_are_continuous(
      by=['change_id'], bounds=['lower_elevation', 'upper_elevation'],
      tag='warning'
    )
  ],
  Table('mass_balance'): {
    Column('area'): Check.close_to_band_sum(
      table='mass_balance_band',
      column='area',
      by={'glacier_id': 'glacier_id', 'year': 'year'},
      bounds=['lower_elevation', 'upper_elevation'],
      tolerance=0.1,
      tag='warning'
    ),
    **{
      Column(column): [
        Check.within_band_range(
          table='mass_balance_band',
          column=column,
          by={'glacier_id': 'glacier_id', 'year': 'year'}
        ),
        Check.close_to_band_sum(
          table='mass_balance_band',
          column=column,
          by={'glacier_id': 'glacier_id', 'year': 'year'},
          bounds=['lower_elevation', 'upper_elevation'],
          weights='area',
          tolerance=0.1,
          tag='warning'
        )
      ]
      for column in ['annual_balance', 'summer_balance', 'winter_balance']
    }
  },
  Table('mass_balance_band'): [
    Check.bounds_do_not_overlap(
      by=['glacier_id', 'year'], bounds=['lower_elevation', 'upper_elevation']
    ),
    Check.bounds_are_continuous(
      by=['glacier_id', 'year'], bounds=['lower_elevation', 'upper_elevation'],
      tag='warning'
    )
  ],
  Table('mass_balance_point'): {
    Column('elevation'): Check.within_band_bounds(
      table='mass_balance_band',
      by={'glacier_id': 'glacier_id', 'year': 'year'},
      bounds=['lower_elevation', 'upper_elevation'],
      tag='warning'
    )
  }
})
"""
Custom row group checks not covered by metadata.
"""


SORT_SUBMISSION = Schema({
  # Sort tables
  Tables(): Check.has_sorted_tables(
    tables=[resource['name'] for resource in EXTENDED_SUBMISSION_METADATA['resources']],
    sort=True
  ),
  # Sort columns
  **{
    Table(resource['name']): Check.has_sorted_columns(
      columns=[field['name'] for field in resource['schema']['fields']],
      sort=True
    )
    for resource in EXTENDED_SUBMISSION_METADATA['resources']
  }
})
"""
Sort order of tables and columns in submission.
"""


FILL_SUBMISSION = Schema({
  # Fill columns
  **{
    Table(resource['name']): Check.has_columns(
      columns=[field['name'] for field in resource['schema']['fields']],
      fill=True
    )
    for resource in SUBMISSION_METADATA['resources']
  }
})
"""
Fill missing columns in submission.
"""


REJECT_EXTRA_COLUMNS = Schema({
  # Check for extra tables
  Tables(): Check.only_has_tables(
    [resource['name'] for resource in EXTENDED_SUBMISSION_METADATA['resources']]
  ),
  # Check for extra columns
  **{
    Table(resource['name']): Check.only_has_columns(
      [field['name'] for field in resource['schema']['fields']]
    )
    for resource in EXTENDED_SUBMISSION_METADATA['resources']
  },
})
"""
Reject presence of extra tables or columns.
"""


def get_submission_migration(version: int = 2021) -> Schema:
  """
  Get the migration that converts the submission to a standard format.

  Loads the migration defined in
  `fog.migrations.v{version}.SUBMISSION_MIGRATION`, if present.

  Parameters
  ----------
  version
    Submission version
  """
  schema = Schema({})
  # Check version number
  module_name: str = f'v{version}'
  if module_name not in dir(fog.migrations):
    raise ValueError(f'Unrecognized version: {version}')
  module = getattr(fog.migrations, module_name)
  # Load submission migration
  if hasattr(module, 'SUBMISSION_MIGRATION'):
    schema += getattr(module, 'SUBMISSION_MIGRATION')
  return schema


def get_schema_migration(version: int = 2021) -> Schema:
  """
  Get the migration that converts the submission to the latest schema.

  Loads the migration defined in `fog.migrations.v{version}.SCHEMA_MIGRATION`,
  if present, for all versions equal to and greater than `version`.

  Parameters
  ----------
  version
    Submission version
  """
  schema = Schema({})
  # Check version number
  module_name: str = f'v{version}'
  if module_name not in dir(fog.migrations):
    raise ValueError(f'Unrecognized version: {version}')
  # Load schema migrations
  for name in sorted(dir(fog.migrations)):
    value = getattr(fog.migrations, name)
    if (
      name >= module_name and
      inspect.ismodule(value) and
      hasattr(value, 'SCHEMA_MIGRATION')
    ):
      schema += getattr(value, 'SCHEMA_MIGRATION')
  schema += REJECT_EXTRA_COLUMNS
  return schema


def get_standard_checks(submission: bool = True) -> Schema:
  """
  Get standard checks.

  Parameters
  ----------
  submission
    Whether the data to check is a submission.
  """
  if submission:
    metadata = copy.deepcopy(OUTPUT_METADATA)
    for resource in metadata['resources']:
      for field in resource['schema']['fields']:
        # HACK: Do not enforce column types or constraints other than required/unique
        # (see PARSE_SUBMISSION)
        field['type'] = 'any'
        field['constraints'] = {
          key: value for key, value in field.get('constraints', {}).items()
          if key in ('required', 'unique')
        }
    schema = (
      validator.convert.frictionless.package_to_schema(
        metadata,
        # HACK: Migration adds columns and tables not described in the metadata
        strict=False,
        order=None,
        columns={'strict': False, 'order': None}
      )
    )
  else:
    schema = validator.convert.frictionless.package_to_schema(
      OUTPUT_METADATA,
      strict=False,
      order=None,
      columns={'strict': False, 'order': None}
    )
  return schema + CUSTOM_ROW_CHECKS + CUSTOM_GROUP_CHECKS


def get_remote_fkey_checks(db: fog.iodb.DB) -> Schema:
  """
  Get remote foreign key checks.

  Best applied before merging with database
  (where these constraints are already enforced).

  Parameters
  ----------
  db:
    Database.
  """
  return Schema({
    Table('glacier'): {
      Column('wgi_id'): Check.in_db_column(
        db=db,
        table='wgi_glacier',
        column='id',
        message='Not found in WGI'
      ),
      Column('glims_id'): Check.in_db_column(
        db=db,
        table='outline',
        column='original_glacier_id',
        where_equals=('inventory', 'glims'),
        message='Not found in GLIMS 20230725. Maybe an update is needed?'
      ),
      Column('rgi50_id'): Check.in_db_column(
        db=db,
        table='outline',
        column='original_id',
        where_equals=('inventory', 'rgi5'),
        message='Not found in RGI 5.0'
      ),
      Column('rgi60_id'): Check.in_db_column(
        db=db,
        table='outline',
        column='original_id',
        where_equals=('inventory', 'rgi6'),
        message='Not found in RGI 6.0'
      ),
      Column('rgi70_id'): Check.in_db_column_rgi7_id(
        db=db,
        glacier_table='rgi7_glacier',
        complex_table='rgi7_complex',
        column='id'
      ),
    },
    Table('glacier_name'): {
      Column('language'): Check.in_db_column(
        db=db,
        table='language',
        column='name',
        message='Not found in database ({table}.{column}). Maybe you need to add it?'
      ),
      Column('script'): Check.in_db_column(
        db=db,
        table='script',
        column='name',
        message='Not found in database ({table}.{column}). Maybe you need to add it?'
      )
    }
  })


def get_country_checks(db: fog.iodb.DB) -> Schema:
  """
  Get country checks.

  Applied after merging with database so that coordinates are updated.
  """
  return Schema({
    Table('glacier'): {
      # Check that glacier coordinates are within country bounds
      Column('country'): Check.country_contains_point(
        coordinates=('latitude', 'longitude'), db=db
      ),
      # Drop country column
      Table(): Check.drop_columns(['country'])
    }
  })


def get_serial_key_checks(db: fog.iodb.DB) -> Schema:
  """
  Get serial key checks.
  """
  return Schema({
    # Require serial key columns with a warning
    **{
      Column(column, table=table): Check.not_null(
        message='Value is missing and will thus be set by the database',
        tag='warning'
      )
      for table, column in SERIAL_KEY_COLUMNS
    },
    # -- state
    # state.id
    Table('state'): Check.increment_id_from_db(id='id', table='state', column='id', db=db),
    # state_band.date -> state_band.state_id
    Tables(): Check.replace_band_dates_with_id(
      glacier={'table': 'state', 'glacier_id': 'glacier_id', 'dates': ['date'], 'id': 'id'},
      band={'table': 'state_band', 'glacier_id': 'glacier_id', 'dates': ['date'], 'id': 'state_id'}
    ),
    # -- change
    # change.id
    Table('change'): Check.increment_id_from_db(id='id', table='change', column='id', db=db),
    # change_band.begin_date, change_band.end_date -> change_band.change_id
    Tables(): Check.replace_band_dates_with_id(
      glacier={'table': 'change', 'glacier_id': 'glacier_id', 'dates': ['begin_date', 'end_date'], 'id': 'id'},
      band={'table': 'change_band', 'glacier_id': 'glacier_id', 'dates': ['begin_date', 'end_date'], 'id': 'change_id'}
    ),
    # -- event.id
    Table('event'): Check.increment_id_from_db(
      id='id', table='event', column='id', db=db
    ),
    # -- front_variation.series_id (0: in-situ)
    Table('front_variation'): Check.fillna_front_variation_series_id(value=0),
    # -- mass_balance_point.id
    Table('mass_balance_point'): Check.increment_id_from_db(
      id='id', table='mass_balance_point', column='id', db=db
    ),
  })


BIBLIOGRAPHY_TABLES = [
  'glacier', 'glacier_name', 'state', 'change', 'front_variation', 'mass_balance',
  'event', 'outline'
]
"""
Tables that have references column replaced by bibliography_id column.
"""


def get_references_checks(db: fog.iodb.DB) -> Schema:
  """
  Get references checks.
  """
  return Schema({
    # Check *.references
    **{
      Column('references', table=table): Check.references_syntax_is_valid()
      for table in BIBLIOGRAPHY_TABLES
    },
    # Migrate *.references -> bibliography_id
    Tables(): Check.process_references(
      references='references', bibliography_id='bibliography_id', db=db
    ),
    # Drop *.references
    **{
      Table(table): Check.drop_columns(['references'])
      for table in BIBLIOGRAPHY_TABLES
    },
  })


TEAM_TABLES = ['state', 'change', 'front_variation', 'mass_balance', 'event']
"""
Tables that have investigators and agencies columns replaced by team_id column.
"""


def get_investigators_agencies_checks(db: fog.iodb.DB) -> Schema:
  """
  Get investigators & agencies checks.
  """
  return Schema({
    # Check *.investigators, *.agencies
    **{
      Table(table): Check.investigators_agencies_syntax_is_valid(
        investigators='investigators', agencies='agencies'
      )
      for table in TEAM_TABLES
    },
    # Migrate (*.investigators, *.agencies) -> team_id
    Tables(): Check.process_investigators_agencies(
      investigators='investigators', agencies='agencies', team_id='team_id', db=db
    ),
    # Drop *.investigators, *.agencies
    **{
      Table(table): Check.drop_columns(['investigators', 'agencies'])
      for table in TEAM_TABLES
    }
  })


def get_rgi_checks(db: fog.iodb.DB) -> Schema:
  """
  Get RGI checks.
  """
  return Schema({
    Table('glacier'): [
      Check.process_rgi_ids(db=db),
      Check.drop_columns(['rgi50_id', 'rgi60_id'])
    ]
  })


def get_outline_checks(db: fog.iodb.DB) -> Schema:
  """
  Get outline checks.
  """
  return Schema({
    # Parse outline polygons
    Column('polygon', table='outline'): Check.wkt_polygon_is_valid(fix_invalid=True),
    # Assign id to submitted outlines
    Table('outline'): Check.increment_id_from_db(
      id='id', table='outline', column='id', db=db
    ),
    # Parse outline_id columns
    **{
      Column(column, table=table): Check.parse_outline_id(db=db)
      for table, column in fog.metadata.OUTLINE_ID_COLUMNS
    },
    # Check whether custom outlines are unused
    Tables(): Check.submitted_outlines_are_used()
  })
