import colorama

colorama.init(strip=False)

FOG_DB: str = None
"""
Path to FoG PostgreSQL database.

Should be of the form `postgresql://{user}:{password}@{host}/{database}`.
"""

MAX_REPORT_COLUMN_WIDTH: int = 80
"""Maximum column width (in characters) for reports printed to the console."""

EARTHDATA_USERNAME: str = None
"""Earthdata username."""

EARTHDATA_PASSWORD: str = None
"""Earthdata username."""

DOWNLOAD_PATH: str = 'imports/download'
"""Path to the download directory."""

EXTRACT_PATH: str = 'imports/extract'
"""Path to the extract directory."""

TRANSFORM_PATH: str = 'imports/transform'
"""Path to the transform directory."""

DOI_PATH: str = 'doi'
"""Path to the DOI directory."""
