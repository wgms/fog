import re

DOI_URL_REGEX = re.compile(
  r'^(?:https?\:\/\/)?(?:dx\.)?doi\.org\/(10\.\d+\/\S+[a-zA-Z0-9])$', re.I
)
"""Regular expression for a DOI URL."""

DOI_REGEX = re.compile(
  (
    r'(?:doi\s*:\s*)?' +
    r'(?:(?:https?\:\/\/)?(?:dx\.)?doi\.org\/)?' +
    r'(10\.\d+\/\S+[a-zA-Z0-9])'
  ),
  re.I
)
"""Regular expression for DOI."""

ORCID_REGEX = re.compile(
  (
    r'(?:orcid\s*:\s*)?' +
    r'(?:(?:https?\:\/\/)?(?:www\.)?orcid\.org\/)?' +
    r'(\d{4}-\d{4}-\d{4}-\d{3}[0-9X])'
  ),
  re.I
)
"""Regular expression for ORCID."""

URL_REGEX = re.compile(r'(https?:\/\/\S+(?:[^\s\)\]\|.,;:]))', re.I)
"""
Regular expression for a generic URL.

URLs must start with http or https and cannot end with whitespace or punctuation.
"""

LIST_SEPARATOR = ' | '
"""Separator for lists."""

LIST_SPLIT_REGEX = re.compile(r' ?\| ?')
"""Regular expression for splitting lists."""

AGENCY_PATH_SEPARATOR = ' > '
"""Separator for agency paths."""

AGENCY_PATH_SPLIT_REGEX = re.compile(r' ?\> ?')
"""Regular expression for splitting agency paths."""

REFERENCE_REGEX = re.compile(r'^(?:\[(?P<prefix>[^\]]+)\] )?(?P<reference>.+?)(?: \[(?P<suffix>[^\]]+)\])?$')
"""Regular expression for a reference in the format `[{prefix}] {reference} [{suffix}]."""

FULL_AGENCY_REGEX = re.compile(r'^(?P<id>[0-9]+\.) (?P<path>.+)$')
"""Regular expression for an agency in the format `{id}. {path}`."""

AGENCY_REGEX = re.compile(r'^(?:(?P<abbreviation>[^\[\]]+)(?: \[(?P<english_abbreviation>[^\[\]]+)\])?: )?(?P<name>[^\[\]]+)(?: \[(?P<english_name>[^\[\]]+)\])?$')
"""
Regular expression for an agency in the format `{abbreviation}: {name}`.

Abbreviation is optional and Engish names can follow for each in square brackets.
"""

INVESTIGATOR_REGEX = re.compile(r'^(?:\[(?P<remarks>[^\[\]]+)\] ?)?(?:(?P<name>[^\[\]\(\)0-9]+[^\s])(?: \[(?P<english_name>[^\[\]\(\)0-9]+)\])?(?: (?P<orcid>\d{4}-\d{4}-\d{4}-\d{3}[0-9X]))?)?(?: ?\((?P<agencies>[0-9]+(?: ?, ?[0-9]+)*)\))?$')
"""
Regular expression for an investigator.

`[{remarks}] {name} [{english_name}] {orcid} ({agencies})`.
"""

SURNAME_REGEX = re.compile(r'\{(?P<surname>[^\}]+)\}')
"""Regular expression for a surname in the format `{surname}`."""


def extract_dois(text: str) -> list[str]:
  """
  Extract DOIs from text.

  Supports formats {doi} and doi:{doi}, where {doi} is a raw DOI or a DOI URL
  with optional {http, https} protocol and {doi.org, dx.doi.org} domain.
  """
  matches = re.findall(DOI_REGEX, text)
  return list(dict.fromkeys(matches))


def extract_orcids(text: str) -> list[str]:
  """
  Extract ORCIDs from text.

  Supports formats {orcid} and orcid:{orcid}, where {orcid} is a raw ORCID or
  an ORCID URL with optional {http, https} protocol and
  {orcid.org, www.orcid.org} domain.
  """
  matches = re.findall(ORCID_REGEX, text)
  matches = [match.upper() for match in matches]
  return list(dict.fromkeys(matches))


def extract_urls(text: str) -> list[str]:
  """
  Extract URLs from text.

  Supports URLs with http or https protocol.
  """
  matches = re.findall(URL_REGEX, text)
  return list(dict.fromkeys(matches))


def parse_references(text: str) -> list[dict | None]:
  """
  Parse references from text.

  Assumes references are formatted as `[prefix] reference [suffix] | ...`.

  Examples
  --------
  >>> text = (
  ...   '[prefix 1] reference 1 [suffix 1] | [prefix 2] reference 2| '
  ...   'reference 3 [suffix 3] |reference 4|reference 5|'
  ... )
  >>> parse_references(text)
  [{'prefix': 'prefix 1', 'reference': 'reference 1', 'suffix': 'suffix 1', 'dois': [], 'urls': []},
   {'prefix': 'prefix 2', 'reference': 'reference 2', 'suffix': None, 'dois': [], 'urls': []},
   {'prefix': None, 'reference': 'reference 3', 'suffix': 'suffix 3', 'dois': [], 'urls': []},
   {'prefix': None, 'reference': 'reference 4', 'suffix': None, 'dois': [], 'urls': []},
   {'prefix': None, 'reference': 'reference 5', 'suffix': None, 'dois': [], 'urls': []},
   None]
  >>> parse_references('10.7265/cc6e-zp12')
  [{'prefix': None, 'reference': '10.7265/cc6e-zp12', 'suffix': None,
  'dois': ['10.7265/cc6e-zp12'], 'urls': []}]
  >>> parse_references('https://doi.org/10.7265/cc6e-zp12')
  [{'prefix': None, 'reference': 'https://doi.org/10.7265/cc6e-zp12', 'suffix': None,
  'dois': ['10.7265/cc6e-zp12'], 'urls': []}]
  >>> parse_references('10.1111/1468-0459.00105 https://www.tandfonline.com/doi/abs/10.1111/1468-0459.00105')
  [{'prefix': None,
  'reference': '10.1111/1468-0459.00105 https://www.tandfonline.com/doi/abs/10.1111/1468-0459.00105',
  'suffix': None, 'dois': ['10.1111/1468-0459.00105'],
  'urls': ['https://www.tandfonline.com/doi/abs/10.1111/1468-0459.00105']}]
  """
  items = re.split(LIST_SPLIT_REGEX, text)
  results = []
  for item in items:
    match = re.fullmatch(REFERENCE_REGEX, item)
    if match:
      groups = match.groupdict()
      groups['dois'] = extract_dois(groups['reference'])
      urls = extract_urls(groups['reference'])
      # Drop any URLs that are DOI URLs
      groups['urls'] = [url for url in urls if not re.fullmatch(DOI_URL_REGEX, url)]
      results.append(groups)
    else:
      results.append(None)
  return results


def are_references_valid(references: list[dict | None]) -> bool:
  """
  Check whether references are valid.

  Examples
  --------
  >>> are_references_valid([{'reference': 'reference'}])
  True
  >>> are_references_valid([{'reference': '[reference]'}])
  False
  >>> are_references_valid([{'reference': 'reference', 'prefix': '[prefix]'}])
  False
  >>> are_references_valid([{'reference': 'reference [prefix]'}])
  False
  """
  for reference in references:
    if reference is None:
      return False
    # Reference cannot start or end with square brackets
    if re.search(r'^[\[\]]|[\[\]]$', reference['reference']):
      return False
    # Reference cannot contain unmatched square brackets
    if not are_brackets_balanced(reference['reference'], open='[', close=']'):
      return False
    # Prefix and suffix cannot contain square brackets
    if reference.get('prefix') and ('[' in reference['prefix'] or ']' in reference['prefix']):
      return False
    if reference.get('suffix') and ('[' in reference['suffix'] or ']' in reference['suffix']):
      return False
    # Cannot contain multiple DOIs
    if len(reference.get('dois', [])) > 1:
      return False
    # Cannot contain multiple URLs
    if len(reference.get('urls', [])) > 1:
      return False
  return True


def parse_investigator(text: str) -> dict | None:
  """
  Parse investigator from text.

  Examples
  --------
  >>> parse_investigator('Michael Zemp 0000-0003-2391-7877 (1, 2)')
  {'name': 'Michael Zemp', 'orcid': '0000-0003-2391-7877', 'agencies': [1, 2]}
  >>> parse_investigator('李忠勤 [{Li} Zhongqin]')
  {'name': '李忠勤', 'english_name': 'Li Zhongqin', 'english_surname': 'Li'}
  >>> parse_investigator('[remarks] (3)')
  {'remarks': 'remarks', 'agencies': [3]}
  >>> parse_investigator('Michael Zemp ()') is None
  True
  """
  match = re.fullmatch(INVESTIGATOR_REGEX, text)
  if not match:
    return None
  groups = match.groupdict()
  # Extract surnames
  for name in ('name', 'english_name'):
    if groups.get(name):
      surnames = re.findall(SURNAME_REGEX, groups[name])
      if len(surnames) > 1:
        return None
      if len(surnames) == 1:
        groups[name.replace('name', 'surname')] = surnames[0]
        groups[name] = re.sub(r'[\{\}]', '', groups[name])
  # Convert agencies to integer list
  if groups.get('agencies'):
    agency_ids = [int(index) for index in groups['agencies'].split(',')]
    # Drop any duplicate agencies
    groups['agencies'] = list(dict.fromkeys(agency_ids))
  # Drop empty values
  return {key: value for key, value in groups.items() if value}


def is_investigator_valid(obj: dict) -> bool:
  """
  Check whether investigator is valid.

  Examples
  --------
  >>> is_investigator_valid({'name': '{Li} Huiling'})
  False
  >>> is_investigator_valid({'name': 'Li Huilin'})
  True
  >>> is_investigator_valid({'remarks': 'remarks'})
  False
  >>> is_investigator_valid({'agencies': [1]})
  True
  """
  # Must have a name or agencies
  if not obj.get('name') and not obj.get('agencies'):
    return False
  # Name cannot contain special characters other than -, ', .
  for key in ('name', 'english_name'):
    if key in obj and re.search(r'[\[\]\(\)0-9"#$%&*+,\/:;<=>?@\\^_`{}~]', obj[key]):
      return False
  return True


def parse_investigators(text: str) -> list[dict | None]:
  """
  Parse investigators from text.

  Examples
  --------
  >>> parse_investigators('Michael Zemp (1, 2) | (3)')
  [{'name': 'Michael Zemp', 'agencies': [1, 2]}, {'agencies': [3]}]
  """
  items = re.split(LIST_SPLIT_REGEX, text)
  return [parse_investigator(item) for item in items]


def parse_agency(text: str) -> list[dict | None]:
  """
  Parse agency from text.

  Examples
  --------
  >>> parse_agency('1. WGMS')
  {'id': 1, 'name': 'WGMS'}
  >>> parse_agency('2. UZH > GIUZ: Geographisches Institut')
  {'id': 2, 'abbreviation': 'GIUZ', 'name': 'Geographisches Institut',
  'parents': [{'name': 'UZH'}]}
  >>> parse_agency('3. 名古屋大学 [Nagoya University]')
  {'id': 3, 'name': '名古屋大学', 'english_name': 'Nagoya University'}
  """
  match = re.fullmatch(FULL_AGENCY_REGEX, text)
  if not match:
    return None
  groups = match.groupdict()
  # Convert id to integer (after stripping trailing period)
  groups['id'] = int(groups['id'][:-1])
  # Split path into list of agencies
  agency_strings = re.split(AGENCY_PATH_SPLIT_REGEX, groups['path'])
  del groups['path']
  # Parse each agency
  agencies = []
  for string in agency_strings:
    match = re.fullmatch(AGENCY_REGEX, string)
    if not match:
      return None
    agency_groups = match.groupdict()
    # Drop empty values
    agency = {key: value for key, value in agency_groups.items() if value}
    agencies.append(agency)
  groups.update(agencies[-1])
  groups['parents'] = agencies[:-1]
  return {key: value for key, value in groups.items() if value}


def is_agency_valid(obj: dict) -> bool:
  """
  Check whether agency is valid.

  Examples
  --------
  >>> is_agency_valid({'name': 'WGMS'})
  True
  >>> is_agency_valid({'name': 'WGMS; GIUZ'})
  False
  """
  # Name cannot contain special characters other than -, ', ., 0-9, &
  for key in ('name', 'english_name', 'abbreviation', 'english_abbreviation'):
    if key in obj and re.search(r'[\[\]\(\)"#$%*+\/:;<=>?@\\^_`{}~]', obj[key]):
      return False
  return True


def parse_agencies(text: str) -> list[dict | None]:
  """
  Parse agencies from text.

  Examples
  --------
  >>> parse_agencies('1. WGMS | 2. GIUZ: Geographisches Institut')
  [{'id': 1, 'name': 'WGMS'},
  {'id': 2, 'abbreviation': 'GIUZ', 'name': 'Geographisches Institut'}]
  """
  items = re.split(LIST_SPLIT_REGEX, text)
  return [parse_agency(item) for item in items]


def are_investigators_agencies_valid(
  investigators: list[dict | None],
  agencies: list[dict | None]
) -> bool:
  """
  Check whether investigators and agencies are valid.

  Examples
  --------
  >>> are_investigators_agencies_valid(
  ...   [{'name': 'Michael Zemp', 'agencies': [1, 2]}],
  ...   [{'id': 1, 'name': 'WGMS'}, {'id': 2, 'name': 'GIUZ'}]
  ... )
  True
  >>> are_investigators_agencies_valid(
  ...   [{'name': 'Michael Zemp', 'agencies': [1, 2]}],
  ...   [{'id': 1, 'name': 'WGMS'}]
  ... )
  False
  >>> are_investigators_agencies_valid(
  ...   [{'name': 'Michael Zemp', 'agencies': [1]}],
  ...   [{'id': 1, 'name': 'WGMS'}, {'id': 1, 'name': 'GIUZ'}]
  ... )
  False
  >>> are_investigators_agencies_valid(
  ...   [{'name': 'Michael Zemp'}],
  ...   [],
  ... )
  True
  """
  # Validate individual elements
  for investigator in investigators:
    if investigator is None or not is_investigator_valid(investigator):
      return False
  for agency in agencies:
    if agency is None or not is_agency_valid(agency):
      return False
  # Check that agency ids are unique
  agency_ids = [agency['id'] for agency in agencies]
  if len(set(agency_ids)) != len(agency_ids):
    return False
  # Check that agency ids match exactly between investigators and agencies
  investigator_agency_ids = set(
    agency_id for investigator in investigators
    for agency_id in investigator.get('agencies', [])
  )
  if investigator_agency_ids != set(agency_ids):
    return False
  return True


def are_brackets_balanced(text: str, open: str = '[', close: str = ']') -> bool:
  """
  Check whether text has balanced brackets.

  Examples
  --------
  >>> are_brackets_balanced('hello')
  True
  >>> are_brackets_balanced('[ hello]')
  True
  >>> are_brackets_balanced('[a [hello]')
  False
  """
  count = 0
  for char in text:
    if char == open:
      count += 1
    elif char == close:
      count -= 1
    if count < 0:
      return False
  return count == 0


def prune_agencies(
  agencies: list[dict], investigators: list[dict]
) -> list[dict]:
  """
  Prune agencies based on investigator assignments.

  Examples
  --------
  >>> investigators = [{'agencies': [1, 3]}]
  >>> agencies = [
  ...   {'id': 1, 'name': 'WGMS'},
  ...   {'id': 2, 'name': 'GIUZ'},
  ...   {'id': 3, 'name': 'UZH'}]
  >>> prune_agencies(agencies, investigators)
  [{'id': 1, 'name': 'WGMS'}, {'id': 3, 'name': 'UZH'}]
  """
  agency_ids = {
    agency_id
    for investigator in investigators
    for agency_id in investigator.get('agencies', [])
  }
  return [agency for agency in agencies if agency['id'] in agency_ids]


def print_investigator(obj: dict) -> str:
  """
  Print investigator to text.

  >>> obj = {'name': 'Michael Zemp', 'orcid': '0000-0003-2391-7877', 'agencies': [1, 2]}
  >>> print_investigator(obj)
  'Michael Zemp 0000-0003-2391-7877 (1, 2)'
  >>> obj = {'name': '李忠勤', 'english_name': 'Li Zhongqin', 'english_surname': 'Li'}
  >>> print_investigator(obj)
  '李忠勤 [{Li} Zhongqin]'
  >>> obj = {'remarks': 'remarks', 'agencies': [3]}
  >>> print_investigator(obj)
  '[remarks] (3)'
  >>> print_investigator({}) is None
  True
  """
  parts = []
  if obj.get('name'):
    parts.append(obj['name'])
  if obj.get('english_name'):
    name = obj['english_name']
    if obj.get('english_surname'):
      surname = obj['english_surname']
      # HACK: Assumes that surname is unique in name
      name = re.sub(fr'(?:^| ){surname}(?= |$)', f'{{{surname}}}', name)
    parts.append(f'[{name}]')
  if obj.get('orcid'):
    parts.append(obj['orcid'])
  if obj.get('agencies'):
    agencies = ', '.join(str(x) for x in obj['agencies'])
    parts.append(f'({agencies})')
  if obj.get('remarks'):
    parts = [f"[{obj['remarks']}]", *parts]
  return ' '.join(parts) or None


def print_investigators(investigators: list[dict]) -> str:
  """
  Print investigators to text.

  Examples
  --------
  >>> investigators = [{'name': 'Michael Zemp', 'agencies': [1, 2]}, {'agencies': [3]}]
  >>> print_investigators(investigators)
  'Michael Zemp (1, 2) | (3)'
  """
  return ' | '.join([print_investigator(i) for i in investigators])


def print_agency(obj: dict) -> str:
  """
  Print agency to text.

  Examples
  --------
  >>> obj = {'id': 1, 'name': 'WGMS'}
  >>> print_agency(obj)
  '1. WGMS'
  >>> obj = {'id': 2, 'abbreviation': 'GIUZ', 'name': 'Geographisches Institut',
  ...        'parents': [{'name': 'UZH'}]}
  >>> print_agency(obj)
  '2. UZH > GIUZ: Geographisches Institut'
  >>> obj = {'id': 3, 'name': '名古屋大学', 'english_name': 'Nagoya University'}
  >>> print_agency(obj)
  '3. 名古屋大学 [Nagoya University]'
  """
  parts = []
  if obj.get('id'):
    parts.append(f"{obj['id']}.")
  if obj.get('parents'):
    parents = [print_agency(parent) for parent in obj['parents']]
    parts.append(' > '.join(parents) + ' >')
  if obj.get('abbreviation'):
    abbreviation = obj['abbreviation']
    if obj.get('english_abbreviation'):
      abbreviation = f"{abbreviation} [{obj['english_abbreviation']}]"
    parts.append(f"{obj['abbreviation']}:")
  if obj.get('name'):
    parts.append(obj['name'])
  if obj.get('english_name'):
    parts.append(f"[{obj['english_name']}]")
  return ' '.join(parts) or None


def print_agencies(agencies: list[dict]) -> str:
  """
  Print agencies to text.

  Examples
  --------
  >>> agencies = [
  ...   {'id': 1, 'name': 'WGMS'},
  ...   {'id': 2, 'abbreviation': 'GIUZ', 'name': 'Geographisches Institut'}]
  >>> print_agencies(agencies)
  '1. WGMS | 2. GIUZ: Geographisches Institut'
  """
  return ' | '.join([print_agency(a) for a in agencies])
