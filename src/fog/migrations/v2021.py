import re
from typing import Dict, Hashable, Iterable, List, Optional, Tuple

import pandas as pd
from validator import Column, Check, Schema, Table, Tables, register_check
from fog.checks import transform_group_test
import fog.helpers


# ---- Constants ----

TABLE_RENAMES = {
  'GLACIER': 'glacier',
  'STATE': 'state',
  'CHANGE': 'change',
  'FRONT_VARIATION': 'front_variation',
  'MASS_BALANCE': 'mass_balance_band',
  'MASS_BALANCE_POINT': 'mass_balance_point',
  'MASS_BALANCE_OVERVIEW': 'mass_balance',
  'SPECIAL_EVENT': 'event',
}

COUNTRY_CODES = {
  'AF': 'Afghanistan',
  'AL': 'Albania',
  'AQ': 'Antarctica',
  'AR': 'Argentina',
  'AT': 'Austria',
  'AZ': 'Azerbaijan',
  'BG': 'Bulgaria',
  'BO': 'Bolivia',
  'BT': 'Bhutan',
  'CA': 'Canada',
  'CD': 'Congo (Democratic Republic)',
  'CH': 'Switzerland',
  'CL': 'Chile',
  'CN': 'China',
  'CO': 'Colombia',
  'DE': 'Germany',
  'EC': 'Ecuador',
  'ES': 'Spain',
  'FR': 'France',
  'GE': 'Georgia',
  'GL': 'Greenland',
  'GS': 'South Georgia and the South Sandwich Islands',
  'HM': 'Heard Island and McDonald Islands',
  'ID': 'Indonesia',
  'IN': 'India',
  'IR': 'Iran',
  'IS': 'Iceland',
  'IT': 'Italy',
  'JP': 'Japan',
  'KE': 'Kenya',
  'KG': 'Kyrgyzstan',
  'KZ': 'Kazakhstan',
  'ME': 'Montenegro',
  'MM': 'Myanmar',
  'MN': 'Mongolia',
  'MX': 'Mexico',
  'NO': 'Norway',
  'NP': 'Nepal',
  'NZ': 'New Zealand',
  'PE': 'Peru',
  'PK': 'Pakistan',
  'PL': 'Poland',
  'RU': 'Russia',
  'SE': 'Sweden',
  'SI': 'Slovenia',
  'SJ': 'Svalbard and Jan Mayen',
  'TF': 'French Southern Territories',
  'TJ': 'Tajikistan',
  'TR': 'Turkey',
  'TZ': 'Tanzania',
  'UG': 'Uganda',
  'US': 'United States',
  'UZ': 'Uzbekistan',
  'VE': 'Venezuela',
  'ZA': 'South Africa'
}

PLATFORM_METHOD_CODES = [
  'tR', 'tM', 'tG', 'tP', 'tL', 'tZ', 'tC', 'tX', 'aR', 'aM', 'aG', 'aP', 'aL',
  'aZ', 'aC', 'aX', 'sR', 'sM', 'sG', 'sP', 'sL', 'sZ', 'sC', 'sX', 'cR', 'cM',
  'cG', 'cP', 'cL', 'cZ', 'cC', 'cX', 'xR', 'xM', 'xG', 'xP', 'xL', 'xZ', 'xC',
  'xX'
]

PLATFORM_CODES = {
  't': 'ground',  # terrestrial
  'a': 'air',
  's': 'space',
  'c': 'other',  # multiple, combined
  'x': 'other'
}

METHOD_CODES = {
  'G': 'direct',  # in-situ
  'M': 'map',
  'P': 'photo',
  'L': 'laser',
  'Z': 'radar',
  'R': 'reconstruction',
  'C': 'other',  # multiple, combined
  'X': 'other'
}

RECONSTRUCTION_METHOD_CODES = {
  'MAP': 'map',
  'PHO': 'photo',
  'WRS': 'text',
  'PRT': 'print',  # refers to reproduction method (print from engraving, etc)
  'PAI': 'painting',
  'DRA': 'drawing',
  'RAD': 'radiocarbon',
  'DEN': 'dendrochronology',
  'HIS': 'historical',  # subset of reconstruction
  'COM': 'reconstruction',
  'OTH': 'reconstruction'
}

QUALITATIVE_VARIATIONS = {
  '-X': 'retreat',
  'X': 'advance',
  'ST': 'stationary'
}

MORAINE_CONDITIONS = {
  'MMP': 'mainly preserved',
  'MPE': 'partly eroded',
  'MME': 'mainly eroded'
}

ELA_PREFIX = {
  '<': 'below',
  '>': 'above'
}

BALANCE_CODES = {
  'BW': 'winter',
  'BS': 'summer',
  'BA': 'annual',
  'IN': 'index'
}

TIME_SYSTEM_CODES = {
  'FXD': 'fixed',
  'FLO': 'floating',
  'OTH': 'other',
  # Deprecated codes:
  'STR': 'stratigraphic',
  'COM': 'combined'
}


# ---- Check functions ----

@register_check(test=False)
def remap_political_unit(s: pd.Series) -> pd.Series:
  """Map political unit country code to country names."""
  return s.replace(COUNTRY_CODES)


@register_check(test=False)
def drop_tables_whose_name_match_regex(
  dfs: Dict[Hashable, pd.DataFrame], *, regex: str, case: bool = True
) -> Optional[Dict[Hashable, pd.DataFrame]]:
  """Drop tables whose name matches regex."""
  flags = 0 if case else re.IGNORECASE
  dropped = [table for table in dfs if re.fullmatch(regex, str(table), flags)]
  if dropped:
    return {table: df for table, df in dfs.items() if table not in dropped}


@register_check(
  message='Tables {glacier} and {lut} do not share any columns named {on}',
  required=lambda glacier, lut: [Table(glacier), Table(lut)]
)
def join_glacier_and_glacier_id_lut(
  dfs: Dict[str, pd.DataFrame],
  *,
  glacier: str = 'GLACIER',
  lut: str = 'GLACIER_ID_LUT',
  on: List[str] = ['POLITICAL_UNIT', 'NAME', 'WGMS_ID'],
  remarks: str = 'REMARKS'
) -> bool:
  """
  Join GLACIER and GLACIER_ID_LUT and remove GLACIER_ID_LUT.

  * Join on POLITICAL_UNIT, NAME and WGMS_ID (whichever are present).
  * Paste REMARKS together and remove duplicates.
  * Any other columns present in both tables are prefixed `GLACIER_ID_LUT`.

  Examples
  --------
  >>> glacier = pd.DataFrame({'WGMS_ID': [1, 2, 3], 'REMARKS': ['foo', 'foo | bar', 'foo']})
  >>> lut = pd.DataFrame({'WGMS_ID': [2, 1, 3], 'GID': [4, 3, 5], 'REMARKS': ['foo', 'baz', 'foo']})
  >>> dfs = {'GLACIER': glacier, 'GLACIER_ID_LUT': lut}
  >>> join_glacier_and_glacier_id_lut(dfs)
  True
  >>> 'GLACIER_ID_LUT' not in dfs
  True
  >>> dfs['GLACIER']
     WGMS_ID          REMARKS  GID
  0        1        foo | baz    3
  1        2        foo | bar    4
  2        3              foo    5
  """
  if glacier in dfs and lut in dfs:
    # Filter join columns
    on = [column for column in on if column in dfs[glacier] and column in dfs[lut]]
    if not on:
      return False
    # Prefix lut column names
    dfs[lut].columns = [
      f'{lut}.{column}' if column not in on else column
      for column in dfs[lut]
    ]
    # Merge tables
    merge = dfs[glacier].merge(
      dfs[lut],
      how='outer',
      on=on
    )
    # Merge remarks
    lut_remarks = f'{lut}.{remarks}'
    if lut_remarks in merge:
      if remarks in merge:
        merge[remarks] = fog.helpers.paste_string_columns(
          merge[remarks], merge[lut_remarks]
        )
        merge[remarks] = fog.helpers.deduplicate_string_list(merge[remarks])
      else:
        merge[remarks] = merge[lut_remarks]
      del merge[lut_remarks]
    # Remove prefix
    for column in dfs[lut]:
      rename = re.sub(rf'^{lut}\.', '', column)
      dfs[lut].rename(columns={column: rename}, inplace=True)
      if rename not in merge:
        merge.rename(columns={column: rename}, inplace=True)
  elif lut in dfs:
    merge = dfs[lut]
  if lut in dfs:
    del dfs[lut]
  dfs[glacier] = merge
  return True


@register_check(
  message='Does not share any columns named {key} with table {series}',
  required=lambda series: [Table(series)]
)
def merge_with_reconstruction_series(
  df: pd.DataFrame,
  dfs: Dict[str, pd.DataFrame],
  *,
  series: str = 'reconstruction_series',
  key: List[str] = ['glacier_id', 'series_id'],
  remarks: str = 'remarks'
) -> Tuple[bool, pd.DataFrame]:
  """
  Merge reconstruction front variation with reconstruction series.

  Examples
  --------
  >>> df = pd.DataFrame({
  ...   'glacier_id': [1, 1, 1, 2],
  ...   'series_id': [1, 1, 2, 3],
  ...   'remarks': ['foo', 'bar', 'baz', pd.NA],
  ...   'change': [1, 2, 4, 3]
  ... })
  >>> series = pd.DataFrame({
  ...   'glacier_id': [1, 1, 2],
  ...   'series_id': [1, 2, 3],
  ...   'remarks': ['foo', 'bar', 'baz']
  ... })
  >>> dfs = {'series': series}
  >>> valid, merge = merge_with_reconstruction_series(df, dfs, series='series')
  >>> valid
  True
  >>> merge
     glacier_id  series_id    remarks  change
  0           1          1        foo       1
  1           1          1  foo | bar       2
  2           1          2  bar | baz       4
  3           2          3        baz       3
  """
  if remarks in key:
    raise ValueError('Remarks cannot be a key column')
  key = [column for column in key if column in df and column in dfs[series]]
  if not key:
    return False, df
  # Prefix series columns
  dfs[series].columns = [
    f'{series}.{column}' if column not in key else column
    for column in dfs[series]
  ]
  merge = df.set_index(key).join(
    dfs[series].set_index(key),
    how='outer'
  ).reset_index()
  # Merge remarks
  series_remarks = f'{series}.{remarks}'
  if series_remarks in merge:
    if remarks in merge:
      merge[remarks] = fog.helpers.paste_string_columns(
        merge[series_remarks], merge[remarks]
      )
      merge[remarks] = fog.helpers.deduplicate_string_list(merge[remarks])
    else:
      merge[remarks] = merge[series_remarks]
    del merge[series_remarks]
  # Remove prefix if possible
  for column in dfs[series]:
    rename = re.sub(rf'^{series}\.', '', column)
    dfs[series].rename(columns={column: rename}, inplace=True)
    if rename not in merge:
      merge.rename(columns={column: rename}, inplace=True)
  return True, merge


@register_check(test=False, required=lambda tables: [Table(table) for table in tables])
def stack_tables(
  dfs: Dict[str, pd.DataFrame], *, tables: List[str], name: str
) -> Dict[str, pd.DataFrame]:
  """Stack tables together."""
  if name in dfs and name not in tables:
    raise ValueError(f'Table {name} already exists')
  # NOTE: Modifies inplace
  dfs[name] = pd.concat([dfs[x] for x in tables], ignore_index=True)
  return dfs


@register_check(
  tag='warning',
  axis='column',
  message='Column dropped but contained non-null values'
)
def drop_columns_and_warn(df: pd.DataFrame, *, columns: Iterable[Hashable]) -> Dict[Hashable, bool]:
  """Drop columns from dataframe and warn if not null."""
  valid = {}
  for column in columns:
    valid[column] = column not in df or df[column].isnull().all()
    if column in df:
      del df[column]
  return valid


@register_check(
  message='{old} contains invalid values',
  required=lambda old: [Column(old)]
)
def remap_platform_method(
  df: pd.DataFrame, *, old: str, new: Tuple[str, str], remarks: str = None
) -> bool:
  """
  Split survey platform-method column into separate columns with new codes.

  If remarks are available but null, then `other` are nulled.

  Examples
  --------
  >>> df = pd.DataFrame({
  ...   'pm': [pd.NA, 'xP', 'sX'],
  ...   'r': [pd.NA, pd.NA, 'radar']
  ... })

  Without remarks:

  >>> temp = df.copy()
  >>> remap_platform_method(temp, old='pm', new=('p', 'm'))
  True
  >>> temp
         r      p      m
  0   <NA>   <NA>   <NA>
  1   <NA>  other  photo
  2  radar  space  other

  With remarks:

  >>> temp = df.copy()
  >>> remap_platform_method(temp, old='pm', new=('p', 'm'), remarks='r')
  True
  >>> temp
         r      p      m
  0   <NA>   <NA>   <NA>
  1   <NA>   <NA>  photo
  2  radar  space  other

  Column name collision:

  >>> temp = df.copy()
  >>> remap_platform_method(temp, old='pm', new=('p', 'r'))
  Traceback (most recent call last):
  ValueError: Columns ['r'] already exist

  Invalid values:

  >>> remap_platform_method(pd.DataFrame({'pm': [0]}), old='pm', new=('p', 'r'))
  False
  """
  exists = [x for x in new if x in df and x != old]
  if exists:
    raise ValueError(f"Columns {exists} already exist")
  # Test values
  s = df[old]
  valid = s.isnull() | s.isin(PLATFORM_METHOD_CODES)
  if not valid.all():
    return False
  # NOTE: Modifies dataframe in place
  del df[old]
  platform, method = new
  df[platform] = s.str.slice(0, 1).replace(PLATFORM_CODES)
  df[method] = s.str.slice(1, 2).replace(METHOD_CODES)
  # Replace 'other' with null if remarks are null
  if remarks and remarks in df:
    no_remarks = df[remarks].isnull()
    df.loc[df[platform].eq('other') & no_remarks, platform] = pd.NA
    df.loc[df[method].eq('other') & no_remarks, method] = pd.NA
  return True


@register_check(
  message='Missing bounds, missing glacier row, or non-equal {constants}',
  required=lambda table: [Table(table)]
)
def move_change_bands_to_new_table(
  dfs: Dict[str, pd.DataFrame],
  *,
  table: str = 'change',
  glacier_id: str = 'glacier_id',
  keys: Iterable[str] = ['id', 'end_date'],
  bounds: Iterable[str] = ['lower_elevation', 'upper_elevation'],
  constants: Iterable[str] = ['investigators', 'references'],
  values: Iterable[str] = None,
  name: str = 'change_band'
) -> pd.Series:
  """
  Split change into change and change_band.

  Requirements:
  * All rows have elevation bounds.
  * All rows with same glacier id and any additional keys (whichever are present)
    have single glacier-wide row with same constants.

  Examples
  --------
  >>> dfs = {'change': pd.DataFrame({
  ...   'glacier_id': [1, 2, 2, 2, 2],
  ...   'id': [pd.NA, 1, 2, 2, 2],
  ...   'lower_elevation': [9999, 9999, 9999, 0, 5],
  ...   'upper_elevation': [9999, 9999, 9999, 5, 10],
  ...   'value': [0, 1, 2, 3, 4],
  ...   'constant': [0, 1, 2, 2, 2]
  ... })}
  >>> move_change_bands_to_new_table(
  ...   dfs, keys=['id'], values=['value'], constants=['constant']
  ... )
  0    True
  1    True
  2    True
  3    True
  4    True
  dtype: bool
  >>> dfs['change']
     glacier_id    id  value  constant
  0           1  <NA>      0         0
  1           2     1      1         1
  2           2     2      2         2
  >>> dfs['change_band']
     glacier_id id  lower_elevation  upper_elevation  value
  3           2  2                0                5      3
  4           2  2                5               10      4
  """
  if name in dfs:
    raise ValueError(f'Table {name} already exists')
  df = dfs[table]
  # All rows have glacier id and elevation bounds
  required = [glacier_id, *bounds]
  if not all(column in df for column in required):
    return pd.Series(False, index=df.index)
  valid = df[required].notnull().all(axis='columns')
  # Filter columns
  keys = [glacier_id] + [column for column in keys if column in df]
  constants = [column for column in constants if column in df]
  values = [column for column in values if column in df]
  # Each group has single glacier-wide row
  valid &= transform_group_test(
    df,
    by=keys,
    dropna=False,
    fn=lambda gdf: (
      gdf[bounds]
      .astype('string')
      .eq('9999')
      .all(axis='columns')
      .sum()
    ) == 1
  )
  # Each group has equal constants
  if constants:
    valid &= transform_group_test(
      df,
      by=keys,
      dropna=False,
      fn=lambda gdf: gdf[constants].drop_duplicates().shape[0] == 1
    )
  if not valid.all():
    return valid
  # Split into two tables
  columns = [*keys, *bounds, *values]
  is_glacier = df[bounds].astype('string').eq('9999').all(axis='columns')
  dfs[table] = df[is_glacier].drop(columns=bounds)
  if (~is_glacier).any():
    dfs[name] = df[~is_glacier][columns]
  return pd.Series(True, index=df.index)


@register_check(
  message='Invalid or missing bounds in {from_table}',
  required=lambda from_table: [Table(from_table)]
)
def move_glacier_wide(
  dfs: Dict[str, pd.DataFrame],
  *,
  from_table: str,
  to_table: str,
  keys: List[str],
  bounds: List[str],
  values: List[str],
  remarks: str = 'remarks'
) -> bool:
  """
  Move glacier wide bands to new table.

  Examples
  --------
  >>> band = pd.DataFrame({
  ...   'id': [1, 1, 1, 2],
  ...   'lower': [9999, 0, 5, 9999],
  ...   'upper': [9999, 5, 10, 9999],
  ...   'value': [0, -1, 1, 3],
  ...   'remarks': ['foo', 'bar', 'baz', pd.NA]
  ... })

  Without target table:

  >>> dfs = {'band': band.copy()}
  >>> move_glacier_wide(
  ...   dfs, from_table='band', to_table='glacier', keys=['id'],
  ...   bounds=['lower', 'upper'], values=['value'], remarks='remarks'
  ... )
  True
  >>> bands_only = dfs['band']
  >>> bands_only
     id  lower  upper  value remarks
  1   1      0      5     -1     bar
  2   1      5     10      1     baz
  >>> dfs['glacier'].convert_dtypes()
     id remarks  value
  0   1     foo      0
  1   2    <NA>      3

  With target table but no value columns:

  >>> glacier = pd.DataFrame({'id': [1, 2], 'remarks': ['y', 'z']})
  >>> dfs = {'band': band.copy(), 'glacier': glacier}
  >>> move_glacier_wide(
  ...   dfs, from_table='band', to_table='glacier', keys=['id'],
  ...   bounds=['lower', 'upper'], values=['value'], remarks='remarks'
  ... )
  True
  >>> pd.testing.assert_frame_equal(bands_only, dfs['band'])
  >>> dfs['glacier']
     id  remarks  value
  0   1  y | foo      0
  1   2        z      3

  With target table and value column:

  >>> glacier = pd.DataFrame({'id': [1, 2], 'remarks': ['y', 'z'], 'value': [0, pd.NA]})
  >>> dfs = {'band': band.copy(), 'glacier': glacier}
  >>> move_glacier_wide(
  ...   dfs, from_table='band', to_table='glacier', keys=['id'],
  ...   bounds=['lower', 'upper'], values=['value'], remarks='remarks'
  ... )
  True
  >>> pd.testing.assert_frame_equal(bands_only, dfs['band'])
  >>> dfs['glacier']
      id remarks  value
  0   1  y | foo      0
  1   2        z      3

  With target table and inconsistent value column:

  >>> glacier = pd.DataFrame({'id': [1, 2], 'remarks': ['y', 'z'], 'value': [0, 1]})
  >>> dfs = {'band': band.copy(), 'glacier': glacier}
  >>> move_glacier_wide(
  ...   dfs, from_table='band', to_table='glacier', keys=['id'],
  ...   bounds=['lower', 'upper'], values=['value'], remarks='remarks'
  ... )
  True
  >>> pd.testing.assert_frame_equal(bands_only, dfs['band'])
  >>> dfs['glacier']
      id remarks  value  band.value
  0   1  y | foo      0           0
  1   2        z      1           3
  """
  if any(x for x in values if x in keys):
    raise ValueError(f'Columns {values} cannot also be present in `keys`')
  # Require bounds to not be null
  if any(column not in dfs[from_table] or dfs[from_table][column].isnull().any() for column in bounds):
    return False
  is_glacier = dfs[from_table][bounds].astype('string').eq('9999').all(axis='columns')
  if not is_glacier.any():
    return True
  # Insert missing keys
  if to_table not in dfs:
    dfs[to_table] = pd.DataFrame()
  for column in keys:
    for df in (dfs[from_table], dfs[to_table]):
      if column not in df:
        df[column] = pd.NA
  # Initialize new tables
  gdf = dfs[from_table][is_glacier].set_index(keys)
  df = dfs[to_table].set_index(keys)
  # Drop glacier-wide from bands
  # NOTE: Modifies in place
  dfs[from_table] = dfs[from_table][~is_glacier]
  # ---- Move glacier-wide to overview
  # Drop missing value columns
  values = [column for column in values if column in gdf]
  # Apply prefix to value and remark column names
  if remarks and remarks not in gdf:
    remarks = None
  values_remarks = values
  if remarks:
    values_remarks.append(remarks)
  prefixed = {column: f'{from_table}.{column}' for column in values_remarks}
  gdf.columns = [prefixed.get(column, column) for column in gdf]
  # Join tables
  merge = df.join(gdf[list(prefixed.values())], how='outer').reset_index()
  # Merge remarks
  if remarks:
    if prefixed[remarks] in merge:
      if remarks in merge:
        merge[remarks] = fog.helpers.paste_string_columns(
          merge[remarks], merge[prefixed[remarks]]
        )
        merge[remarks] = fog.helpers.deduplicate_string_list(merge[remarks])
      else:
        merge[remarks] = merge[prefixed[remarks]]
      del merge[prefixed[remarks]]
  # Join columns with same name (ignoring prefix) and values
  for column in values:
    if prefixed[column] in merge:
      if column in merge:
        merge[column] = merge[column].combine_first(merge[prefixed[column]])
      else:
        merge[column] = merge[prefixed[column]]
      if (merge[prefixed[column]].isnull() | merge[prefixed[column]].eq(merge[column])).all():
        del merge[prefixed[column]]
  # NOTE: Modifies in place
  dfs[to_table] = merge
  return True


@register_check(test=False)
def remap_qualitative_variation(s: pd.Series) -> pd.Series:
  """Map qualitative variation code to new codes."""
  return s.replace(QUALITATIVE_VARIATIONS)


@register_check(test=False)
def append_remarks(
  s: pd.Series,
  df: pd.DataFrame,
  *,
  column: str,
  prefix: str = None
) -> pd.Series:
  """
  Append column to remarks.

  Examples
  --------
  >>> s = pd.Series([pd.NA, 'foo', pd.NA, 'foo', 'foo | bar'])
  >>> df = pd.DataFrame({'remarks': s, 'x': [pd.NA, pd.NA, 'a', 'b', 'c']})
  >>> append_remarks(s, df, column='x', prefix='x: ')
  0                <NA>
  1                 foo
  2                x: a
  3          foo | x: b
  4    foo | bar | x: c
  dtype: object
  """
  b = df[column]
  if prefix:
    b = prefix + b.astype('string', copy=True)
  return fog.helpers.paste_string_columns(s, b, sep=' | ')


@register_check(
  message='Invalid moraine condition',
  required=lambda moraine_condition: [Column(moraine_condition)]
)
def append_moraine_condition_remarks(
  s: pd.Series,
  df: pd.DataFrame,
  *,
  moraine_condition: str,
  prefix: str
) -> pd.Series:
  """
  Append moraine condition to remarks.

  Examples
  --------
  >>> s = pd.Series([pd.NA, 'something', pd.NA, 'else', 'foo | bar', pd.NA])
  >>> moraine = pd.Series([pd.NA, pd.NA, 'MPE', 'MME', 'MMP', 'OTH'])
  >>> df = pd.DataFrame({'remarks': s, 'moraine': moraine})
  >>> valid, s = append_moraine_condition_remarks(s, df, moraine_condition='moraine', prefix='M: ')
  >>> pd.DataFrame({'remarks': s, 'moraine': moraine, 'valid': valid})
                             remarks moraine  valid
  0                             <NA>    <NA>   True
  1                        something    <NA>   True
  2                 M: partly eroded     MPE   True
  3          else | M: mainly eroded     MME   True
  4  foo | bar | M: mainly preserved     MMP   True
  5                             <NA>     OTH  False
  """
  mc = df[moraine_condition].copy()
  valid = mc.isnull() | mc.isin(MORAINE_CONDITIONS)
  mc = prefix + mc.map(MORAINE_CONDITIONS).astype('string', copy=False)
  return valid, fog.helpers.paste_string_columns(s, mc, sep=' | ')


@register_check(test=False)
def remap_ela_prefix(s: pd.Series) -> pd.Series:
  """Map ELA prefix code to new codes."""
  return s.replace(ELA_PREFIX)


@register_check(test=False)
def remap_time_system(s: pd.Series) -> pd.Series:
  """Map time system code to new codes."""
  return s.replace(TIME_SYSTEM_CODES)


@register_check(test=False)
def fill_area_aar_from_acc_abl(
  df: pd.DataFrame, *, area: str, aar: str, acc: str, abl: str
) -> pd.DataFrame:
  """
  Fill area and AAR from accumulation and ablation areas.

  Area is calculated as (decreasing priority):
  * area
  * acc + abl
  * acc / aar
  * abl / (1 - aar)

  AAR is calculated as (decreasing priority):
  * aar
  * acc / area
  * (area - abl) / area

  Examples
  --------
  >>> area, aar, acc, abl = 4, 0.25, 1, 3
  >>> df = pd.DataFrame([
  ...   {},
  ...   {'area': area},
  ...   {'aar': aar},
  ...   {'acc': acc, 'abl': abl},
  ...   {'aar': aar, 'acc': acc},
  ...   {'aar': aar, 'abl': abl},
  ...   {'aar': aar, 'acc': acc},
  ...   {'area': area, 'acc': acc},
  ...   {'area': area, 'abl': abl},
  ...   {'area': area, 'aar': aar},
  ...   {'area': area, 'aar': aar, 'acc': acc * 2},
  ...   {'area': area, 'aar': aar, 'abl': abl * 2},
  ...   {'area': area, 'aar': aar, 'acc': acc * 2, 'abl': abl * 2}
  ... ])
  >>> out = fill_area_aar_from_acc_abl(df, area='area', aar='aar', acc='acc', abl='abl')
  >>> out[['area', 'aar']].convert_dtypes()
      area   aar
  0   <NA>  <NA>
  1      4  <NA>
  2   <NA>  0.25
  3      4  0.25
  4      4  0.25
  5      4  0.25
  6      4  0.25
  7      4  0.25
  8      4  0.25
  9      4  0.25
  10     4  0.25
  11     4  0.25
  12     4  0.25
  """
  # Fill area
  if area not in df:
    df[area] = pd.NA
  # acc + abl
  if acc in df and abl in df:
    df[area] = df[area].combine_first(df[acc] + df[abl])
  if aar in df:
    # acc / aar
    if acc in df:
      mask = df[aar].ne(0)
      df.loc[mask, area] = df[area][mask].combine_first(df[acc][mask] / df[aar][mask])
    # abl / (1 - aar)
    if abl in df:
      mask = df[aar].ne(1)
      df.loc[mask, area] = df[area][mask].combine_first(df[abl][mask] / (1 - df[aar][mask]))
  # Fill aar
  if aar not in df:
    df[aar] = pd.NA
  mask = df[area].ne(0)
  # acc / area
  if acc in df:
    df.loc[mask, aar] = df[aar][mask].combine_first(df[acc][mask] / df[area][mask])
  # (area - abl) / area
  if abl in df:
    df.loc[mask, aar] = df[aar][mask].combine_first((df[area] - df[abl])[mask] / df[area][mask])
  return df


@register_check(test=False)
def remap_balance_code(s: pd.Series) -> pd.Series:
  """Map balance code to new codes."""
  return s.replace(BALANCE_CODES)


@register_check(test=False)
def convert_km_to_m(s: pd.Series) -> pd.Series:
  """Convert kilometers to meters."""
  return s * 1e3


@register_check(test=False)
def convert_km2_to_m2(s: pd.Series) -> pd.Series:
  """Convert square kilometers to square meters."""
  return s * 1e6


@register_check(test=False)
def convert_1000m2_to_m2(s: pd.Series) -> pd.Series:
  """Convert 1000 square meters to square meters."""
  return s * 1e3


@register_check(test=False)
def convert_1000m3_to_m3(s: pd.Series) -> pd.Series:
  """Convert 1000 cubic meters to cubic meters."""
  return s * 1e3


@register_check(test=False)
def convert_mm_to_m(s: pd.Series) -> pd.Series:
  """Convert millimeters to meters."""
  return s * 1e-3


@register_check(
  message='Original value in interval (0 ≤ 1]. Maybe already a fraction?',
  tag='warning'
)
def convert_percent_to_fraction(s: pd.Series) -> Tuple[pd.Series, pd.Series]:
  """
  Convert percentage to fraction but warn if in interval (0, 1].

  Examples
  --------
  >>> s = pd.Series([pd.NA, 0.5, 1, 2, 100])
  >>> valid, fraction = convert_percent_to_fraction(s)
  >>> pd.DataFrame({'fraction': fraction, 'valid': valid})
    fraction  valid
  0     <NA>   True
  1    0.005  False
  2     0.01  False
  3     0.02   True
  4      1.0   True
  """
  valid = s.isnull() | s.eq(0) | s.gt(1)
  return valid, s * 1e-2


@register_check(test=False)
def convert_years_to_days(s: pd.Series) -> pd.Series:
  """Convert years to days."""
  return s * 365.25


@register_check(
  message='First four characters not equal to {year}',
  required=lambda year: [Column(year)]
)
def fill_date_from_year(s: pd.Series, df: pd.DataFrame, *, year: str) -> Tuple[pd.Series, pd.Series]:
  """
  Fill date from year.

  Expects both columns to be strings.
  * If date is null, use year.
  * If date is not null and year is not null, check that date[:4] == year.

  Examples
  --------
  >>> s = pd.Series([pd.NA, pd.NA, '20230102', '20230102'])
  >>> year = pd.Series([pd.NA, '2023', '2023', '2024'])
  >>> df = pd.DataFrame({'date': s, 'year': year})
  >>> valid, date = fill_date_from_year(s, df, year='year')
  >>> pd.DataFrame({'date': date, 'year': year, 'valid': valid}).convert_dtypes()
         date  year  valid
  0      <NA>  <NA>   <NA>
  1      2023  2023   <NA>
  2  20230102  2023   True
  3  20230102  2024  False
  """
  both = s.notnull() & df[year].notnull()
  valid = s[both].str.slice(0, 4) == df[year][both]
  return valid, s.combine_first(df[year])


@register_check(test=False)
def remap_reconstruction_method(s: pd.Series) -> Tuple[pd.Series, pd.Series]:
  """Map method code to new codes."""
  return s.replace(RECONSTRUCTION_METHOD_CODES).fillna('reconstruction')


@register_check(test=False)
def format_date_as_iso8601(s: pd.Series) -> pd.Series:
  """
  Format legacy date to variable precision ISO 8601 format.

  Examples
  --------
  >>> s = pd.Series(['2023', '20239999', '20230199', '20230102'])
  >>> format_date_as_iso8601(s)
  0          2023
  1          2023
  2       2023-01
  3    2023-01-02
  dtype: object
  """
  return (
    s.str.replace(r'^([0-9]{4})([0-9]{2})([0-9]{2})$', '\\1-\\2-\\3', regex=True)
    .str.replace('^([0-9]{4})-99-99$', '\\1', regex=True)
    .str.replace('^([0-9]{4}-[0-9]{2})-99$', '\\1', regex=True)
  )


@register_check(
  message="Column '{area}' already exists (case-insensitive)",
  required=lambda end: [Column(end)]
)
def fill_area_from_end_area_and_change(df: pd.DataFrame, *, end: str, change: str, area: str) -> Tuple[bool, pd.Series]:
  """
  Fill area from end area and area change.

  Examples
  --------
  >>> df = pd.DataFrame({'end': [pd.NA, 2, 3], 'change': [pd.NA, pd.NA, 1]})
  >>> fill_area_from_end_area_and_change(df, end='end', change='change', area='area')
  (True, end change  area
     0  <NA>   <NA>  <NA>
     1     2   <NA>     2
     2     3      1   2.5)
  >>> df['area'] = pd.NA
  >>> fill_area_from_end_area_and_change(df, end='end', change='change', area='area')
  (False, None)
  """
  # NOTE: Modifies dataframe in place
  if area.lower() in [(x.lower() if isinstance(x, str) else x) for x in df]:
    return False, None
  df[area] = df[end]
  if change in df:
    mask = df[change].notnull()
    df.loc[mask, area] -= df.loc[mask, change] / 2
  return True, df

@register_check(
  message='Ignored if average of {lowest} and {highest}, otherwise renamed to {mean} and flagged in {remarks}',
  tag='warning'
)
def migrate_median_elevation_to_mean_elevation(
  s: pd.Series,
  df: pd.DataFrame,
  *,
  mean: str,
  lowest: str,
  highest: str,
  remarks: str,
  threshold: float = 5
) -> pd.Series:
  """
  Migrate median elevation to mean elevation.

  * If actually the median of lowest and highest, set to null.
  * If not, rename to mean and flag in remarks.
  """
  # NOTE: Modifies dataframe in place
  is_mean = s.notnull()
  if lowest in df and highest in df:
    median = (df[lowest] + df[highest]) / 2
    is_not_median = ((s - median).abs() > threshold).fillna(False)
    is_mean &= is_not_median
  df.loc[is_mean, mean] = df.loc[is_mean, median]
  df.loc[is_mean, remarks] = fog.helpers.paste_string_columns(
    df.loc[is_mean, remarks],
    pd.Series(f'[flag] {mean}: Originally submitted as median elevation', index=df.index[is_mean])
  )
  return is_mean

# ---- Schemas ----

SUBMISSION_MIGRATION = Schema({
  # Drop submitter contact sheet
  Tables(): Check.drop_tables(tables=['Submitter contact']),
  # Drop sheets used for dropdown menus ('enum.*')
  Tables(): Check.drop_tables_whose_name_match_regex(
    regex=r'enum\..+',
    case=False
  ),
  # Since 2023, a single sheet named 'lists' is used instead
  Tables(): Check.drop_tables(tables=['lists']),
  # Drop unnamed columns read by pd.read_excel ('unnamed*')
  Table(): Check.drop_columns_whose_name_match_regex(
    regex=r'unnamed.*',
    case=False
  )
})

SCHEMA_MIGRATION = Schema({
  # ---- Glacier
  Table('GLACIER'): {
    Table(): Check.drop_columns([
      'GLACIER_REGION_CODE',
      'GLACIER_SUBREGION_CODE'
    ]),
    Column('POLITICAL_UNIT'): Check.remap_political_unit()
  },
  Tables(): Check.join_glacier_and_glacier_id_lut(
    glacier='GLACIER',
    lut='GLACIER_ID_LUT',
    on=['POLITICAL_UNIT', 'NAME', 'WGMS_ID'],
    remarks='REMARKS'
  ),
  Table('GLACIER'): Check.rename_columns({
    'POLITICAL_UNIT': 'country',
    'NAME': 'name',
    'WGMS_ID': 'id',
    'LATITUDE': 'latitude',
    'LONGITUDE': 'longitude',
    'GLIMS_ID': 'glims_id',
    'RGI50_ID': 'rgi50_id',
    # RGI_ID was briefly used in 2023
    'RGI_ID': 'rgi60_id',
    'RGI60_ID': 'rgi60_id',
    'WGI_ID': 'wgi_id',
    'PSFG_ID': 'psfg_id',
    'PARENT_GLACIER': 'parent_glacier_id',
    'REMARKS': 'remarks',
    # Deprecated columns
    'PRIM_CLASSIFIC': 'prim_classific',
    'FORM': 'form',
    'FRONTAL_CHARS': 'frontal_chars',
    'EXPOS_ACC_AREA': 'expos_acc_area',
    'EXPOS_ABL_AREA': 'expos_abl_area',
    'GEN_LOCATION': 'gen_location',
    'SPEC_LOCATION': 'spec_location'
  }),
  # ---- State
  Table('STATE'): {
    # Migrate median elevation to mean elevation
    **{
      Column(column): Check.parse_as_type('number')
      for column in ['MEDIAN_ELEVATION', 'LOWEST_ELEVATION', 'HIGHEST_ELEVATION']
    },
    Column('MEDIAN_ELEVATION'): Check.migrate_median_elevation_to_mean_elevation(
      lowest='LOWEST_ELEVATION',
      highest='HIGHEST_ELEVATION',
      remarks='REMARKS',
      mean='mean_elevation'
    ),
    Table(): Check.drop_columns([
      'MEDIAN_ELEVATION'
    ]),
    **{
      Column(column): Check.parse_as_type('number')
      for column in ['LENGTH', 'LENGTH_UNC', 'AREA', 'AREA_UNC']
    },
    Column('LENGTH'): Check.convert_km_to_m(),
    Column('LENGTH_UNC'): Check.convert_km_to_m(),
    Column('AREA'): Check.convert_km2_to_m2(),
    Column('AREA_UNC'): Check.convert_km2_to_m2(),
    Column('SURVEY_DATE'): [
      Check.fill_date_from_year(year='YEAR'),
      Check.format_date_as_iso8601()
    ],
    Table(): [
      Check.drop_columns([
        'POLITICAL_UNIT',
        'YEAR'
      ]),
      Check.remap_platform_method(
        old='SURVEY_PLATFORM_METHOD',
        new=('platform', 'method'),
        remarks='REMARKS'
      ),
      Check.rename_columns({
        'NAME': 'glacier_name',
        'WGMS_ID': 'glacier_id',
        'SURVEY_DATE': 'date',
        'HIGHEST_ELEVATION': 'highest_elevation',
        'LOWEST_ELEVATION': 'lowest_elevation',
        'ELEVATION_UNC': 'elevation_unc',
        'LENGTH': 'length',
        'LENGTH_UNC': 'length_unc',
        'AREA': 'area',
        'AREA_UNC': 'area_unc',
        'INVESTIGATOR': 'investigators',
        'SPONS_AGENCY': 'agencies',
        'REFERENCE': 'references',
        'REMARKS': 'remarks',
      })
    ]
  },
  # ---- Change
  Table('CHANGE'): {
    **{
      Column(column): Check.parse_as_type('number')
      for column in [
        'AREA_SURVEY_YEAR', 'AREA_CHANGE', 'AREA_CHANGE_UNC',
        'THICKNESS_CHG', 'THICKNESS_CHG_UNC',
        'VOLUME_CHANGE', 'VOLUME_CHANGE_UNC'
      ]
    },
    Column('AREA_SURVEY_YEAR'): Check.convert_km2_to_m2(),
    Column('AREA_CHANGE'): Check.convert_1000m2_to_m2(),
    Column('AREA_CHANGE_UNC'): Check.convert_1000m2_to_m2(),
    Column('THICKNESS_CHG'): Check.convert_mm_to_m(),
    Column('THICKNESS_CHG_UNC'): Check.convert_mm_to_m(),
    Column('VOLUME_CHANGE'): Check.convert_1000m3_to_m3(),
    Column('VOLUME_CHANGE_UNC'): Check.convert_1000m3_to_m3(),
    Column('REFERENCE_DATE'): Check.format_date_as_iso8601(),
    Column('SURVEY_DATE'): [
      Check.fill_date_from_year(year='YEAR'),
      Check.format_date_as_iso8601()
    ],
    Table(): [
      Check.drop_columns(columns=[
        'POLITICAL_UNIT',
        'YEAR'
      ]),
      Check.remap_platform_method(
        old='RD_PLATFORM_METHOD',
        new=('begin_platform', 'begin_method'),
        remarks='REMARKS'
      ),
      Check.remap_platform_method(
        old='SD_PLATFORM_METHOD',
        new=('end_platform', 'end_method'),
        remarks='REMARKS'
      ),
      Check.fill_area_from_end_area_and_change(
        end='AREA_SURVEY_YEAR',
        change='AREA_CHANGE',
        area='area'
      ),
      Check.drop_columns(columns=[
        'AREA_SURVEY_YEAR'
      ])
    ]
  },
  Tables(): Check.move_change_bands_to_new_table(
    table='CHANGE',
    name='change_band',
    glacier_id='WGMS_ID',
    keys=['SURVEY_ID', 'REFERENCE_DATE', 'SURVEY_DATE'],
    bounds=['LOWER_BOUND', 'UPPER_BOUND'],
    constants=['INVESTIGATOR', 'SPONS_AGENCY', 'REFERENCE'],
    values=[
      'NAME',
      'area',
      'THICKNESS_CHANGE', 'THICKNESS_CHG_UNC',
      'VOLUME_CHANGE', 'VOLUME_CHANGE_UNC',
      'REMARKS',
      # Deprecated columns
      'AREA_CHANGE', 'AREA_CHANGE_UNC'
    ]
  ),
  Table('CHANGE'): Check.rename_columns({
    'NAME': 'glacier_name',
    'WGMS_ID': 'glacier_id',
    'SURVEY_ID': 'id',
    'REFERENCE_DATE': 'begin_date',
    'SURVEY_DATE': 'end_date',
    'UPPER_BOUND': 'upper_elevation',
    'LOWER_BOUND': 'lower_elevation',
    'THICKNESS_CHG': 'elevation_change',
    'THICKNESS_CHG_UNC': 'elevation_change_unc',
    'VOLUME_CHANGE': 'volume_change',
    'VOLUME_CHANGE_UNC': 'volume_change_unc',
    'INVESTIGATOR': 'investigators',
    'SPONS_AGENCY': 'agencies',
    'REFERENCE': 'references',
    'REMARKS': 'remarks',
    # Deprecated columns
    'AREA_CHANGE': 'area_change',
    'AREA_CHANGE_UNC': 'area_change_unc',
  }),
  Table('change_band'): Check.rename_columns({
    'NAME': 'glacier_name',
    'WGMS_ID': 'glacier_id',
    'SURVEY_ID': 'change_id',
    'REFERENCE_DATE': 'begin_date',
    'SURVEY_DATE': 'end_date',
    'UPPER_BOUND': 'upper_elevation',
    'LOWER_BOUND': 'lower_elevation',
    'THICKNESS_CHG': 'elevation_change',
    'THICKNESS_CHG_UNC': 'elevation_change_unc',
    'VOLUME_CHANGE': 'volume_change',
    'VOLUME_CHANGE_UNC': 'volume_change_unc',
    'REMARKS': 'remarks',
    # Deprecated columns
    'AREA_CHANGE': 'area_change',
    'AREA_CHANGE_UNC': 'area_change_unc',
  }),
  # --- Front variation
  Table('FRONT_VARIATION'): {
    Column('QUALITATIVE_VARIATION'): Check.remap_qualitative_variation(),
    Column('REFERENCE_DATE'): Check.format_date_as_iso8601(),
    Column('SURVEY_DATE'): [
      Check.fill_date_from_year(year='YEAR'),
      Check.format_date_as_iso8601()
    ],
    Table(): [
      Check.drop_columns(columns=[
        'POLITICAL_UNIT',
        'YEAR'
      ]),
      Check.remap_platform_method(
        old='SURVEY_PLATFORM_METHOD',
        new=('end_platform', 'end_method'),
        remarks='remarks'
      ),
      Check.rename_columns(columns={
        'NAME': 'glacier_name',
        'WGMS_ID': 'glacier_id',
        'SURVEY_DATE': 'end_date',
        'REFERENCE_DATE': 'begin_date',
        'FRONT_VARIATION': 'length_change',
        'FRONT_VAR_UNC': 'length_change_unc',
        'QUALITATIVE_VARIATION': 'length_change_direction',
        'INVESTIGATOR': 'investigators',
        'SPONS_AGENCY': 'agencies',
        'REFERENCE': 'references',
        'REMARKS': 'remarks'
      })
    ]
  },
  Table('RECONSTRUCTION_SERIES'): {
    Table(): [
      Check.drop_columns(columns=[
        'POLITICAL_UNIT',
      ]),
    ]
  },
  Table('RECONSTRUCTION_FRONT_VARIATION'): {
    Column('METHOD_CODE'): Check.remap_reconstruction_method(),
    Column('QUALITATIVE_VARIATION'): Check.remap_qualitative_variation(),
    Column('REMARKS'): Check.append_remarks(
      column='METHOD_REMARKS',
      prefix='end_method: '
    ),
    Column('REMARKS'): Check.append_moraine_condition_remarks(
      moraine_condition='MORAINE_DEFINED_MAX',
      prefix='Condition of moraine (or other object) used to determine maximum glacier length at end of time interval: '
    ),
    Column('YEAR_UNC'): [
      Check.parse_as_type('number'),
      Check.convert_years_to_days(),
    ],
    Column('REF_YEAR_UNC'): [
      Check.parse_as_type('number'),
      Check.convert_years_to_days(),
    ],
    Table(): [
      Check.drop_columns(
        columns=[
          'POLITICAL_UNIT',
          'METHOD_REMARKS',
          'MORAINE_DEFINED_MAX'
        ]
      ),
      Check.drop_columns_and_warn(
        columns=[
          'LOWEST_ELEVATION',
          'HIGHEST_ELEVATION',
          'ELEVATION_UNC'
        ]
      )
    ]
  },
  # Merge reconstruction_front_variation into front_variation
  Table('RECONSTRUCTION_FRONT_VARIATION'): [
    Check.merge_with_reconstruction_series(
      series='RECONSTRUCTION_SERIES',
      key=['NAME', 'WGMS_ID', 'REC_SERIES_ID']
    ),
    Check.rename_columns(
      columns={
        'NAME': 'glacier_name',
        'WGMS_ID': 'glacier_id',
        'REC_SERIES_ID': 'series_id',
        'YEAR': 'end_date',
        'YEAR_UNC': 'end_date_unc',
        'REFERENCE_YEAR': 'begin_date',
        'REF_YEAR_UNC': 'begin_date_unc',
        'METHOD_CODE': 'end_method',
        'FRONT_VARIATION': 'length_change',
        'QUALITATIVE_VARIATON': 'length_change_unc',
        'FRONT_VAR_POS_UNC': 'length_change_pos_unc',
        'FRONT_VAR_NEG_UNC': 'length_change_neg_unc',
        'INVESTIGATOR': 'investigators',
        'SPONS_AGENCY': 'agencies',
        'REFERENCE': 'references',
        'REMARKS': 'remarks',
      }
    )
  ],
  Tables(): [
    Check.stack_tables(
      tables=['FRONT_VARIATION', 'RECONSTRUCTION_FRONT_VARIATION'],
      name='FRONT_VARIATION'
    ),
    Check.drop_tables(
      tables=['RECONSTRUCTION_SERIES', 'RECONSTRUCTION_FRONT_VARIATION']
    )
  ],
  # --- Mass balance tables
  Table('MASS_BALANCE_OVERVIEW'): {
    Column('ELA_PREFIX'): Check.remap_ela_prefix(),
    Column('TIME_SYSTEM'): Check.remap_time_system(),
    **{
      Column(column): Check.parse_as_type('number')
      for column in [
        'ACC_AREA', 'ACC_AREA_UNC', 'ABL_AREA',
        'ABL_AREA_UNC', 'AAR',
      ]
    },
    Column('ACC_AREA'): Check.convert_km2_to_m2(),
    Column('ACC_AREA_UNC'): Check.convert_km2_to_m2(),
    Column('ABL_AREA'): Check.convert_km2_to_m2(),
    Column('ABL_AREA_UNC'): Check.convert_km2_to_m2(),
    Column('AAR'): Check.convert_percent_to_fraction(),
    Column('BEGIN_PERIOD'): Check.format_date_as_iso8601(),
    Column('END_WINTER'): Check.format_date_as_iso8601(),
    Column('END_PERIOD'): Check.format_date_as_iso8601(),
    Table(): [
      Check.drop_columns(columns=[
        'POLITICAL_UNIT',
      ])
    ]
  },
  Table('MASS_BALANCE'): {
    **{
      Column(column): Check.parse_as_type('number')
      for column in [
        'AREA', 'WINTER_BALANCE', 'WINTER_BALANCE_UNC',
        'SUMMER_BALANCE', 'SUMMER_BALANCE_UNC',
        'ANNUAL_BALANCE', 'ANNUAL_BALANCE_UNC'
      ]
    },
    Column('AREA'): Check.convert_km2_to_m2(),
    Column('WINTER_BALANCE'): Check.convert_mm_to_m(),
    Column('WINTER_BALANCE_UNC'): Check.convert_mm_to_m(),
    Column('SUMMER_BALANCE'): Check.convert_mm_to_m(),
    Column('SUMMER_BALANCE_UNC'): Check.convert_mm_to_m(),
    Column('ANNUAL_BALANCE'): Check.convert_mm_to_m(),
    Column('ANNUAL_BALANCE_UNC'): Check.convert_mm_to_m(),
    Table(): [
      Check.drop_columns(columns=[
        'POLITICAL_UNIT',
      ])
    ]
  },
  # Move glacier-wide to mass_balance
  Tables(): Check.move_glacier_wide(
    from_table='MASS_BALANCE',
    to_table='MASS_BALANCE_OVERVIEW',
    keys=['NAME', 'WGMS_ID', 'YEAR'],
    bounds=['LOWER_BOUND', 'UPPER_BOUND'],
    values=[
      'AREA',
      'WINTER_BALANCE', 'WINTER_BALANCE_UNC',
      'SUMMER_BALANCE', 'SUMMER_BALANCE_UNC',
      'ANNUAL_BALANCE', 'ANNUAL_BALANCE_UNC'
    ],
    remarks='REMARKS'
  ),
  Table('MASS_BALANCE_OVERVIEW'): [
    Check.fill_area_aar_from_acc_abl(
      area='AREA', aar='AAR', acc='ACC_AREA', abl='ABL_AREA'
    ),
    Check.rename_columns(columns={
      'NAME': 'glacier_name',
      'WGMS_ID': 'glacier_id',
      'YEAR': 'year',
      'TIME_SYSTEM': 'time_system',
      'BEGIN_PERIOD': 'begin_date',
      'END_WINTER': 'midseason_date',
      'END_PERIOD': 'end_date',
      'ELA_PREFIX': 'ela_position',
      'ELA': 'ela',
      'ELA_UNC': 'ela_unc',
      'AAR': 'aar',
      'AREA': 'area',
      'WINTER_BALANCE': 'winter_balance',
      'WINTER_BALANCE_UNC': 'winter_balance_unc',
      'SUMMER_BALANCE': 'summer_balance',
      'SUMMER_BALANCE_UNC': 'summer_balance_unc',
      'ANNUAL_BALANCE': 'annual_balance',
      'ANNUAL_BALANCE_UNC': 'annual_balance_unc',
      'INVESTIGATOR': 'investigators',
      'SPONS_AGENCY': 'agencies',
      'REFERENCE': 'references',
      'REMARKS': 'remarks',
      # Deprecated columns
      'ACC_AREA': 'acc_area',
      'ACC_AREA_UNC': 'acc_area_unc',
      'ABL_AREA': 'abl_area',
      'ABL_AREA_UNC': 'abl_area_unc',
      'MIN_SITES_ACC': 'min_sites_acc',
      'MAX_SITES_ACC': 'max_sites_acc',
      'MIN_SITES_ABL': 'min_sites_abl',
      'MAX_SITES_ABL': 'max_sites_abl'
    })
  ],
  Table('MASS_BALANCE'): Check.rename_columns(columns={
    'NAME': 'glacier_name',
    'WGMS_ID': 'glacier_id',
    'YEAR': 'year',
    'LOWER_BOUND': 'lower_elevation',
    'UPPER_BOUND': 'upper_elevation',
    'AREA': 'area',
    'WINTER_BALANCE': 'winter_balance',
    'WINTER_BALANCE_UNC': 'winter_balance_unc',
    'SUMMER_BALANCE': 'summer_balance',
    'SUMMER_BALANCE_UNC': 'summer_balance_unc',
    'ANNUAL_BALANCE': 'annual_balance',
    'ANNUAL_BALANCE_UNC': 'annual_balance_unc',
    'REMARKS': 'remarks'
  }),
  Table('MASS_BALANCE_POINT'): {
    Column('BALANCE_CODE'): Check.remap_balance_code(),
    **{
      Column(column): Check.parse_as_type('number')
      for column in [
        'POINT_BALANCE', 'POINT_BALANCE_UNCERTAINTY'
      ]
    },
    Column('POINT_BALANCE'): Check.convert_mm_to_m(),
    Column('POINT_BALANCE_UNCERTAINTY'): Check.convert_mm_to_m(),
    Column('FROM_DATE'): Check.format_date_as_iso8601(),
    Column('TO_DATE'): Check.format_date_as_iso8601(),
    Table(): [
      Check.drop_columns(columns=[
        'POLITICAL_UNIT',
      ]),
      Check.rename_columns(columns={
        'NAME': 'glacier_name',
        'WGMS_ID': 'glacier_id',
        'YEAR': 'year',
        'POINT_ID': 'original_id',
        'FROM_DATE': 'begin_date',
        'TO_DATE': 'end_date',
        'POINT_LAT': 'latitude',
        'POINT_LON': 'longitude',
        'POINT_ELEVATION': 'elevation',
        'POINT_BALANCE': 'balance',
        'POINT_BALANCE_UNCERTAINTY': 'balance_unc',
        'DENSITY': 'density',
        'DENSITY_UNCERTAINTY': 'density_unc',
        'BALANCE_CODE': 'balance_code',
        'REMARKS': 'remarks'
      })
    ]
  },
  Table('SPECIAL_EVENT'): {
    Table(): [
      Check.columns_missing_or_null(
        columns=['ET_TECTONIC'],
        message='Replace with new type(s): earthquake, volcanic_eruption, rockfall, debris_flow'
      ),
      Check.columns_missing_or_null(
        columns=['ET_OTHER'],
        message='Consider replacing with new type(s): earthquake, volcanic_eruption, rockfall, debris_flow',
        tag='warning'
      ),
      Check.drop_columns(columns=[
        'POLITICAL_UNIT',
        'ET_TECTONIC'
      ]),
      Check.rename_columns({
        'NAME': 'glacier_name',
        'WGMS_ID': 'glacier_id',
        'EVENT_ID': 'id',
        'EVENT_DATE': 'date',
        'ET_SURGE': 'surge',
        'ET_CALVING': 'calving',
        'ET_FLOOD': 'flood',
        'ET_AVALANCHE': 'avalanche',
        'ET_OTHER': 'other',
        'EVENT_DESCRIPTION': 'description',
        'INVESTIGATOR': 'investigators',
        'SPONS_AGENCY': 'agencies',
        'REFERENCE': 'references',
        'REMARKS': 'remarks'
      })
    ]
  },
  Tables(): [
    Check.rename_tables(TABLE_RENAMES),
    Check.drop_empty_tables(),
  ],
  Table(): Check.columns_not_null(drop=True)
})
