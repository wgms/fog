import re
from typing import Dict, Hashable

import pandas as pd
from validator import Check, Schema, Table, Tables, register_check


# ---- Check functions ----

@register_check(test=False)
def lowercase_column_names(df: pd.DataFrame) -> pd.DataFrame:
  """Convert string column names to lowercase."""
  # NOTE: Modifies dataframe in place
  df.columns = [x.lower() if isinstance(x, str) else x for x in df]
  return df


@register_check(test=False)
def lowercase_table_names(dfs: Dict[Hashable, pd.DataFrame]) -> pd.DataFrame:
  """Convert string table names to lowercase."""
  return {
    (key.lower() if isinstance(key, str) else key): value
    for key, value in dfs.items()
  }


@register_check(test=False)
def restore_hidden_column_names(df: pd.DataFrame) -> pd.DataFrame:
  """Restore column names hidden on second line of header row."""
  pattern = r'^[^\n]+\n([a-z_]+)$'
  rename = {}
  for column in df:
    match = re.fullmatch(pattern, column)
    if match:
      rename[column] = match.groups()[0]
  df.rename(columns=rename, inplace=True)
  return df


# ---- Schemas ----

SUBMISSION_MIGRATION = Schema({
  # ---- Spreadsheet templates
  # Drop sheets used for dropdown menus
  Tables(): Check.drop_tables(['lists']),
  # Restore column names hidden by a line break
  Table(): Check.restore_hidden_column_names(),
  # ---- Other
  # Drop unnamed columns read by pd.read_excel ('unnamed*')
  Table(): Check.drop_columns_whose_name_match_regex(
    regex=r'unnamed.*',
    case=False
  ),
  # Convert all names to lowercase
  Tables(): Check.lowercase_table_names(),
  Table(): Check.lowercase_column_names(),
})
