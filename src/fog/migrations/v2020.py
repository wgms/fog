from collections import defaultdict
import re
from typing import Dict, Hashable, List, Tuple

import pandas as pd
from validator import Check, Column, Schema, Table, Tables


TABLE_RENAMES: Dict[str, str] = {
  'A GENERAL INFORMATION': 'GLACIER',
  'AA GLACIER ID LUT': 'GLACIER_ID_LUT',
  'B STATE': 'STATE',
  'C FRONT VARIATION': 'FRONT_VARIATION',
  'D CHANGE': 'CHANGE',
  'D SECTION': 'CHANGE',
  'E MASS BALANCE OVERVIEW': 'MASS_BALANCE_OVERVIEW',
  'EE MASS BALANCE': 'MASS_BALANCE',
  'EEE MASS BALANCE POINT': 'MASS_BALANCE_POINT',
  'F SPECIAL EVENT': 'SPECIAL_EVENT'
}


OLDER_COLUMN_RENAMES: Dict[str, Dict[str, str]] = {
  'STATE': {
    'ELEVATION_ACCURACY': 'ELEVATION_UNCERTAINTY',
    'LENGTH_ACCURACY': 'LENGTH_UNCERTAINTY',
    'SURVEY_METHOD': 'SURVEY_PLATFORM_METHOD'
  },
  'CHANGE': {
    'AREA_CHANGE_ACCURACY': 'AREA_CHANGE_UNCERTAINTY',
    'THICKNESS_CHANGE_ACCURACY': 'THICKNESS_CHANGE_UNCERTAINTY',
    'VOLUME_CHANGE_ACCURACY': 'VOLUME_CHANGE_UNCERTAINTY',
    'SURVEY_METHOD': 'SURVEY_DATE_PLATFORM_METHOD'
  },
  'FRONT_VARIATION': {
    'FRONT_VARIATION_ACCURACY': 'FRONT_VARIATION_UNCERTAINTY',
    'SURVEY_METHOD': 'SURVEY_PLATFORM_METHOD'
  },
  'MASS_BALANCE_OVERVIEW': {
    'ELA_ACCURACY': 'ELA_UNCERTAINTY',
    'ACCUMULATION_AREA_ACCURACY': 'ACCUMULATION_AREA_UNCERTAINTY',
    'ABLATION_AREA_ACCURACY': 'ABLATION_AREA_UNCERTAINTY'
  },
  'MASS_BALANCE': {
    'SPECIFIC_ANNUAL_BALANCE_ACCURACY': 'SPECIFIC_ANNUAL_BALANCE_UNCERTAINTY',
    'SPECIFIC_WINTER_BALANCE_ACCURACY': 'SPECIFIC_WINTER_BALANCE_UNCERTAINTY',
    'SPECIFIC_SUMMER_BALANCE_ACCURACY': 'SPECIFIC_SUMMER_BALANCE_UNCERTAINTY',
    'SPECIFIC_NET_BALANCE': 'SPECIFIC_ANNUAL_BALANCE',
    'SPECIFIC_NET_BALANCE_ACCURACY': 'SPECIFIC_ANNUAL_BALANCE_UNCERTAINTY'
  },
  'MASS_BALANCE_POINT': {
    'POINT_MASS_BALANCE': 'POINT_BALANCE'
  }
}


NEWER_COLUMN_RENAMES: Dict[str, Dict[str, str]] = {
  'GLACIER': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'GEOGRAPHICAL_LOCATION_GENERAL': 'GEN_LOCATION',
    'GEOGRAPHICAL_LOCATION_SPECIFIC': 'SPEC_LOCATION',
    'PRIMARY_CLASSIFICATION': 'PRIM_CLASSIFIC',
    'FRONTAL_CHARACTERISTICS': 'FRONTAL_CHARS',
    'EXPOSITION_OF_ACCUMULATION_AREA': 'EXPOS_ACC_AREA',
    'EXPOSITION_OF_ABLATION_AREA': 'EXPOS_ABL_AREA'
  },
  'GLACIER_ID_LUT': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT'
  },
  'STATE': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'MAXIMUM_ELEVATION_OF_GLACIER': 'HIGHEST_ELEVATION',
    'MEDIAN_ELEVATION_OF_GLACIER': 'MEDIAN_ELEVATION',
    'MINIMUM_ELEVATION_OF_GLACIER': 'LOWEST_ELEVATION',
    'ELEVATION_UNCERTAINTY': 'ELEVATION_UNC',
    'LENGTH_UNCERTAINTY': 'LENGTH_UNC',
    'AREA_UNCERTAINTY': 'AREA_UNC',
    'SPONSORING_AGENCY': 'SPONS_AGENCY'
  },
  'CHANGE': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'AREA_CHANGE_UNCERTAINTY': 'AREA_CHANGE_UNC',
    'THICKNESS_CHANGE': 'THICKNESS_CHG',
    'THICKNESS_CHANGE_UNCERTAINTY': 'THICKNESS_CHG_UNC',
    'VOLUME_CHANGE_UNCERTAINTY': 'VOLUME_CHANGE_UNC',
    'SURVEY_DATE_PLATFORM_METHOD': 'SD_PLATFORM_METHOD',
    'REFERENCE_DATE_PLATFORM_METHOD': 'RD_PLATFORM_METHOD',
    'SPONSORING_AGENCY': 'SPONS_AGENCY'
  },
  'FRONT_VARIATION': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'FRONT_VARIATION_UNCERTAINTY': 'FRONT_VAR_UNC',
    'SPONSORING_AGENCY': 'SPONS_AGENCY'
  },
  'MASS_BALANCE_OVERVIEW': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'TIME_MEASUREMENT_SYSTEM': 'TIME_SYSTEM',
    'BEGINNING_OF_SURVEY_PERIOD': 'BEGIN_PERIOD',
    'END_OF_WINTER_SEASON': 'END_WINTER',
    'END_OF_SURVEY_PERIOD': 'END_PERIOD',
    'EQUILIBRIUM_LINE_ALTITUDE': 'ELA',
    'ELA_UNCERTAINTY': 'ELA_UNC',
    'MIN_NUMBER_OF_MEASUREMENT_SITES_IN_ACCUMULATION_AREA': 'MIN_SITES_ACC',
    'MAX_NUMBER_OF_MEASUREMENT_SITES_IN_ACCUMULATION_AREA': 'MAX_SITES_ACC',
    'MIN_NUMBER_OF_MEASUREMENT_SITES_IN_ABLATION_AREA': 'MIN_SITES_ABL',
    'MAX_NUMBER_OF_MEASUREMENT_SITES_IN_ABLATION_AREA': 'MAX_SITES_ABL',
    'ACCUMULATION_AREA': 'ACC_AREA',
    'ACCUMULATION_AREA_UNCERTAINTY': 'ACC_AREA_UNC',
    'ABLATION_AREA': 'ABL_AREA',
    'ABLATION_AREA_UNCERTAINTY': 'ABL_AREA_UNC',
    'ACCUMULATION_AREA_RATIO': 'AAR',
    'SPONSORING_AGENCY': 'SPONS_AGENCY'
  },
  'MASS_BALANCE': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'LOWER_BOUNDARY_OF_ALTITUDE_INTERVAL': 'LOWER_BOUND',
    'UPPER_BOUNDARY_OF_ALTITUDE_INTERVAL': 'UPPER_BOUND',
    'ALTITUDE_INTERVAL_AREA': 'AREA',
    'SPECIFIC_WINTER_BALANCE': 'WINTER_BALANCE',
    'SPECIFIC_WINTER_BALANCE_UNCERTAINTY': 'WINTER_BALANCE_UNC',
    'SPECIFIC_SUMMER_BALANCE': 'SUMMER_BALANCE',
    'SPECIFIC_SUMMER_BALANCE_UNCERTAINTY': 'SUMMER_BALANCE_UNC',
    'SPECIFIC_ANNUAL_BALANCE': 'ANNUAL_BALANCE',
    'SPECIFIC_ANNUAL_BALANCE_UNCERTAINTY': 'ANNUAL_BALANCE_UNC'
  },
  'MASS_BALANCE_POINT': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'POINT_LATITUDE': 'POINT_LAT',
    'POINT_LONGITUDE': 'POINT_LON'
  },
  'SPECIAL_EVENT': {
    'GLACIER_NAME': 'NAME',
    'POLITCAL_UNIT': 'POLITICAL_UNIT',
    'GLACIER_SURGE': 'ET_SURGE',
    'CALVING_INSTABILITY': 'ET_CALVING',
    'GLACIER_FLOOD': 'ET_FLOOD',
    'ICE_AVALANCHE': 'ET_AVALANCHE',
    'TECTONIC_EVENT': 'ET_TECTONIC',
    'OTHER': 'ET_OTHER',
    'SPONSORING_AGENCY': 'SPONS_AGENCY'
  }
}


COLUMN_RENAMES: Dict[str, Dict[str, str]] = {}
for table in [*OLDER_COLUMN_RENAMES, *NEWER_COLUMN_RENAMES]:
  older = OLDER_COLUMN_RENAMES.get(table, {})
  newer = NEWER_COLUMN_RENAMES.get(table, {})
  base = {**older}
  for k, v in base.items():
    if v in newer:
      base[k] = newer[v]
  base.update(newer)
  COLUMN_RENAMES[table] = base


# ---- Helper functions ----

def normalize_wgms_table_name(name: str) -> str:
  """
  Rename WGMS sheet name to a table name.

  Examples
  --------
  >>> normalize_wgms_table_name('B STATE')
  'B STATE'
  >>> normalize_wgms_table_name('B - STATE')
  'B STATE'
  >>> normalize_wgms_table_name('STATE')
  'STATE'
  """
  return re.sub(r'[\s-]+', ' ', name)


def deduplicate_names(names: list) -> List[str]:
  """
  Example
  -------
  >>> deduplicate_names(['x', None, None, 'x', 'z', 'y', 'z'])
  ['x', 'unnamed', 'unnamed.1', 'x.1', 'z', 'y', 'z.1']
  """
  counters = defaultdict(int)
  unique_names = []
  for name in names:
    if pd.isnull(name):
      name = 'unnamed'
    i = counters[name]
    rename = f'{name}.{i}' if i else name
    while rename in unique_names:
      i += 1
      rename = f'{name}.{i}'
    unique_names.append(rename)
    counters[name] = i + 1
  return unique_names

def normalize_wgms_table_names(
  dfs: Dict[str, pd.DataFrame]
) -> Tuple[Dict[str, bool], Dict[str, pd.DataFrame]]:
  valid = {}
  output = {}
  for name, df in dfs.items():
    renamed = normalize_wgms_table_name(name)
    valid[name] = renamed == name or renamed not in dfs
    final = renamed if valid[name] else name
    output[final] = df
  return valid, output


# ---- Check functions ----

def set_column_names_and_drop_unused(
  df: pd.DataFrame
) -> Tuple[bool, pd.DataFrame]:
  """
  Extract column names and drop unused rows and columns.
  """
  # Find 'add new data below' (case insensitive, ignore spurious whitespace)
  new_data = (
    df.iloc[:, 0]
    .str.strip()
    .str.replace(r'\s+', ' ', regex=True)
    .str.lower().eq('add new data below')
  )
  if new_data.sum() != 1:
    return False, df
  # Extract column names from 3rd row (2nd row below header)
  column_names = df.iloc[1]
  # Drop all header rows, including new data marker
  df = df.drop(index=df.index[[*range(new_data.argmax() + 1)]])
  # Drop unnamed columns
  is_unnamed = column_names.isnull()
  df = df.drop(columns=df.columns[is_unnamed])
  # Update column names
  df.columns = deduplicate_names(column_names[~is_unnamed])
  return True, df


def clean_column_names(
  df: pd.DataFrame
) -> Tuple[Dict[Hashable, bool], pd.DataFrame]:
  """
  Clean column names and test whether results are valid names.

  - Remove trailing and leading whitespace (' GLACIER_NAME ').
  - Fix interior whitespace ('GLACIER NAME', 'GLACIER_ NAME').
  - Verify that result is uppercase snakecase format ('GLACIER_NAME').

  Example
  -------
  >>> names = [' GLACIER NAME', 'GLACIER_ NAME ', 'GLACIER _NAME', 'unknown']
  >>> df = pd.DataFrame(columns=names)
  >>> valid, output = clean_column_names(df)
  >>> valid
  {' GLACIER NAME': True,
   'GLACIER_ NAME ': True,
   'GLACIER _NAME': True,
   'unknown': False}
  >>> output.columns.tolist()
  ['GLACIER_NAME', 'GLACIER_NAME', 'GLACIER_NAME', 'unknown']
  """
  valid = {}
  rename = {}
  for name in df.columns:
    # Replace white space surrounded by letters with underscore
    # e.g. 'GLACIER NAME' -> 'GLACIER_NAME'
    clean = re.sub(r'([A-Z])\s+([A-Z])', r'\1_\2', name)
    # Remove white space surrounded by letter and underscore
    # e.g. 'GLACIER_ NAME' -> 'GLACIER_NAME'
    clean = re.sub(r'([A-Z])\s+_([A-Z])', r'\1_\2', clean)
    clean = re.sub(r'([A-Z])_\s+([A-Z])', r'\1_\2', clean)
    # Remove leading and trailing white space
    # e.g. ' GLACIER_NAME ' -> 'GLACIER_NAME'
    clean = clean.strip()
    # Check that cleaned column name is uppercase snake case
    if not re.fullmatch(r'^[A-Z]+(?:_[A-Z]+)*$', clean):
      valid[name] = False
    else:
      # Rename column
      valid[name] = True
      rename[name] = clean
  if rename:
    df = df.rename(columns=rename)
  return valid, df


def update_survey_method(s: pd.Series) -> pd.Series:
  """
  Update SURVEY_METHOD codes to SURVEY_PLATFORM_METHOD codes.

  Example
  -------
  >>> s = pd.Series(['A', 'tP', pd.NA])
  >>> update_survey_method(s)
  0      aP
  1      tP
  2    <NA>
  dtype: object
  """
  CONVERSIONS: Dict[str, str] = {
    # Aerial photography -> {a: airborne} {P: photogrammetry}
    'A': 'aP',
    # Terrestrial photogrammetry -> {t: terrestrial} {P: photogrammetry}
    'B': 'tP',
    # Geodetic ground survey (theodolite, tape, etc.)
    # -> {t: terrestrial} {G: ground survey}
    'C': 'tG',
    # Combination of A, B or C -> {x: unknown} {C: combined}
    'D': 'xC',
    # Other methods -> {x: unknown} {X: unknown}
    'E': 'xX'
  }
  # Only update matches, leaving others in place for later validation.
  return s.where(~s.isin(CONVERSIONS), s.map(CONVERSIONS))


# ---- Schemas ----

SUBMISSION_MIGRATION: Schema = Schema({
  # Drop instruction sheet
  Tables(): Check.drop_tables(['Instructions']),
  # Normalize sheet names for legacy compatibility
  Tables(): Check(
    normalize_wgms_table_names,
    message=(
      'Other table with normalized name already exists' +
      " (e.g. 'B - STATE' -> 'B STATE', but latter already exists)"
    )
  ),
  # Extract WGMS sheet header and data rows, dropping unnamed columns
  **{
    Table(name): Check(
      set_column_names_and_drop_unused,
      message="Found no or multiple 'ADD NEW DATA BELOW' rows"
    )
    for name in TABLE_RENAMES
  },
  # Rename sheets to match database tables
  Tables(): Check.rename_tables(TABLE_RENAMES),
  # Fix whitespace and underscores in column names
  Table(): Check(
    clean_column_names,
    message='Column name format is invalid',
    axis='column'
  )
})


SCHEMA_MIGRATION: Schema = Schema({
  # Replace survey method codes with survey platform method codes
  Column('SURVEY_METHOD'): Check(update_survey_method, test=False),
  # Flag for manual migration
  Table('SPECIAL_EVENT'): Check.columns_missing_or_null(
    columns=['DATA_SOURCE'],
    message='Manually split into INVESTIGATOR, SPONSORING_AGENCY, and REFERENCE'
  ),
  Table('MASS_BALANCE_POINT'): Check.columns_missing_or_null(
    columns=['POINT_ANNUAL_BALANCE', 'POINT_SUMMER_BALANCE', 'POINT_WINTER_BALANCE'],
    message='Manually refactor as FROM_DATE, TO_DATE, and POINT_BALANCE'
  ),
  # Rename columns
  **{
    Table(table): Check.rename_columns(renames)
    for table, renames in COLUMN_RENAMES.items()
  }
})
