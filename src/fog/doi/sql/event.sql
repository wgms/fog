select
  glacier_country.country_id as country,
  glacier.short_name as glacier_name,
  event.glacier_id,
  event.id,
  date_mean_from_min_max(date_min, date_max) as date,
  date_unc_from_min_max(date_min, date_max) as date_unc,
  trim_scale(round(event.latitude, 6)) as latitude,
  trim_scale(round(event.longitude, 6)) as longitude,
  -- point::geography as point,
  description,
  surge, calving, flood, avalanche, rockfall, debris_flow, earthquake, volcanic_eruption, other,
  team_as_strings.investigators,
  team_as_strings.agencies,
  bibliography_as_string.string as references,
  event.remarks
from event
left join bibliography_as_string on bibliography_as_string.id = bibliography_id
left join team_as_strings on team_as_strings.id = team_id
left join glacier_country on glacier_country.glacier_id = event.glacier_id
left join glacier on glacier.id = event.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  date_min
