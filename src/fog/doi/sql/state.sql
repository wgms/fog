select
  glacier_country.country_id as country,
  glacier.short_name as glacier_name,
  state.glacier_id,
  outline_id_as_string.string as outline_id,
  state.id,
  date_mean_from_min_max(date_min, date_max) as date,
  date_unc_from_min_max(date_min, date_max) as date_unc,
  trim_scale(round(highest_elevation)) as highest_elevation,
  trim_scale(round(lowest_elevation)) as lowest_elevation,
  trim_scale(round(mean_elevation)) as mean_elevation,
  trim_scale(round(elevation_unc)) as elevation_unc,
  trim_scale(round(area)) as area,
  trim_scale(round(area_unc)) as area_unc,
  trim_scale(round(length)) as length,
  trim_scale(round(length_unc)) as length_unc,
  terminus_type,
  platform,
  method,
  team_as_strings.investigators,
  team_as_strings.agencies,
  bibliography_as_string.string as references,
  state.remarks
from state
left join bibliography_as_string on bibliography_as_string.id = bibliography_id
left join team_as_strings on team_as_strings.id = team_id
left join outline_id_as_string on outline_id = outline_id_as_string.id
left join glacier_country on glacier_country.glacier_id = state.glacier_id
left join glacier on glacier.id = state.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  date_min
