select
  id,
  name,
  surname,
  original_name,
  synonyms,
  orcid,
  url,
  birth_year,
  death_year,
  remarks
from person
-- Limit to investigators
where id in (select person_id from team_member where person_id is not null)
order by surname, name
