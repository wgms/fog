select
  glacier_country.country_id as country,
  glacier.short_name as glacier_name,
  mass_balance_point.glacier_id,
  year,
  mass_balance_point.id,
  original_id,
  date_mean_from_min_max(begin_date_min, begin_date_max) as begin_date,
  date_unc_from_min_max(begin_date_min, begin_date_max) as begin_date_unc,
  date_mean_from_min_max(end_date_min, end_date_max) as end_date,
  date_unc_from_min_max(end_date_min, end_date_max) as end_date_unc,
  trim_scale(round(mass_balance_point.latitude, 6)) as latitude,
  trim_scale(round(mass_balance_point.latitude, 6)) as longitude,
  -- point::geography as point,
  trim_scale(round(elevation)) as elevation,
  trim_scale(round(balance, 3)) as balance,
  trim_scale(round(balance_unc, 3)) as balance_unc,
  trim_scale(round(density)) as density,
  trim_scale(round(density_unc)) as density_unc,
  balance_code,
  mass_balance_point.remarks
from mass_balance_point
left join glacier on mass_balance_point.glacier_id = glacier.id
left join glacier_country on glacier.id = glacier_country.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  year
