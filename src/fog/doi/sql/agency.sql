-- Limit to team agencies and their parents and former selves
with recursive ids as (
  select id, former_agency_id, parent_agency_id
  from agency
  where id in (select agency_id from team_member where agency_id is not null)
  union
  select a.id, a.former_agency_id, a.parent_agency_id
  from agency a
  inner join ids on ids.parent_agency_id = a.id or ids.former_agency_id = a.id
)
select
  country,
  id,
  parent_agency_id,
  name,
  abbreviation,
  alternate_name,
  alternate_abbreviation,
  english_name,
  english_abbreviation,
  url,
  wikipedia_url,
  wikidata_id,
  -- rorid,
  begin_year,
  end_year,
  former_agency_id,
  remarks
from agency
where id in (select id from ids)
order by country, id
