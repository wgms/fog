select
  glacier_country.country_id as country,
  glacier.short_name as glacier_name,
  mass_balance.glacier_id,
  outline_id_as_string.string as outline_id,
  year,
  time_system,
  date_mean_from_min_max(begin_date_min, begin_date_max) as begin_date,
  date_unc_from_min_max(begin_date_min, begin_date_max) as begin_date_unc,
  date_mean_from_min_max(midseason_date_min, midseason_date_max) as midseason_date,
  date_unc_from_min_max(midseason_date_min, midseason_date_max) as midseason_date_unc,
  date_mean_from_min_max(end_date_min, end_date_max) as end_date,
  date_unc_from_min_max(end_date_min, end_date_max) as end_date_unc,
  trim_scale(round(winter_balance, 3)) as winter_balance,
  trim_scale(round(winter_balance_unc, 3)) as winter_balance_unc,
  trim_scale(round(summer_balance, 3)) as summer_balance,
  trim_scale(round(summer_balance_unc, 3)) as summer_balance_unc,
  trim_scale(round(annual_balance, 3)) as annual_balance,
  trim_scale(round(annual_balance_unc, 3)) as annual_balance_unc,
  ela_position,
  trim_scale(round(ela)) as ela,
  trim_scale(round(ela_unc)) as ela_unc,
  trim_scale(round(aar, 3)) as aar,
  trim_scale(round(area)) as area,
  team_as_strings.investigators,
  team_as_strings.agencies,
  bibliography_as_string.string as references,
  mass_balance.remarks
from mass_balance
left join outline_id_as_string on outline_id = outline_id_as_string.id
left join team_as_strings on team_id = team_as_strings.id
left join bibliography_as_string on bibliography_id = bibliography_as_string.id
left join glacier_country on glacier_country.glacier_id = mass_balance.glacier_id
left join glacier on glacier.id = mass_balance.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  year
