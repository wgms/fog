with
glacier_names as (
  select
    glacier_id,
    string_agg(name || (case when suffix is null then '' else ' (' || suffix || ')' end), ' | ') as names
  from glacier_name
  group by glacier_id
),
glacier_rgi5_ids as (
  select
    glacier_id,
    string_agg(original_id, ' | ') as rgi_ids
  from glacier_outline
  join outline on glacier_outline.outline_id = outline.id
  where outline.inventory = 'rgi5'
  group by glacier_id
),
glacier_rgi6_ids as (
  select
    glacier_id,
    string_agg(original_id, ' | ') as rgi_ids
  from glacier_outline
  join outline on glacier_outline.outline_id = outline.id
  where outline.inventory = 'rgi6'
  group by glacier_id
)
select
  country_id as country,
  short_name,
  glacier_names.names,
  glacier.id,
  trim_scale(round(latitude, 6)) as latitude,
  trim_scale(round(longitude, 6)) as longitude,
  -- glacier.point::geography as point,
  gtng_region_2023.long_id as gtng_region,
  glims_id,
  glacier_rgi5_ids.rgi_ids as rgi50_ids,
  glacier_rgi6_ids.rgi_ids as rgi60_ids,
  rgi70_id as rgi70_ids,
  wgi_id,
  parent_glacier_id,
  bibliography_as_string.string as references,
  remarks
from glacier
left join glacier_region_2023 on glacier.id = glacier_region_2023.glacier_id
left join gtng_region_2023 on glacier_region_2023.gtng_region_id = gtng_region_2023.id
left join glacier_country on glacier.id = glacier_country.glacier_id
left join glacier_names on glacier.id = glacier_names.glacier_id
left join glacier_rgi5_ids on glacier.id = glacier_rgi5_ids.glacier_id
left join glacier_rgi6_ids on glacier.id = glacier_rgi6_ids.glacier_id
left join bibliography_as_string on glacier.bibliography_id = bibliography_as_string.id
order by
  country,
  short_name,
  id
