select
  glacier_country.country_id as country,
  glacier.short_name as glacier_name,
  mass_balance_band.glacier_id,
  year,
  lower_elevation,
  upper_elevation,
  trim_scale(round(area)) as area,
  trim_scale(round(winter_balance, 3)) as winter_balance,
  trim_scale(round(winter_balance_unc, 3)) as winter_balance_unc,
  trim_scale(round(summer_balance, 3)) as summer_balance,
  trim_scale(round(summer_balance_unc, 3)) as summer_balance_unc,
  trim_scale(round(annual_balance, 3)) as annual_balance,
  trim_scale(round(annual_balance_unc, 3)) as annual_balance_unc,
  mass_balance_band.remarks
from mass_balance_band
left join glacier on mass_balance_band.glacier_id = glacier.id
left join glacier_country on glacier.id = glacier_country.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  year,
  lower_elevation,
  upper_elevation
