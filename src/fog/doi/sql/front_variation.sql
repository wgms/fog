select
  glacier_country.country_id as country,
  glacier.short_name as glacier_name,
  front_variation.glacier_id,
  series_id,
  date_mean_from_min_max(begin_date_min, begin_date_max) as begin_date,
  date_unc_from_min_max(begin_date_min, begin_date_max) as begin_date_unc,
  date_mean_from_min_max(end_date_min, end_date_max) as end_date,
  date_unc_from_min_max(end_date_min, end_date_max) as end_date_unc,
  trim_scale(round(length_change, 3)) as length_change,
  trim_scale(round(coalesce(length_change_unc, greatest(length_change_neg_unc, length_change_pos_unc)), 3)) as length_change_unc,
  length_change_direction,
  end_platform,
  end_method,
  team_as_strings.investigators,
  team_as_strings.agencies,
  bibliography_as_string.string as references,
  front_variation.remarks
from front_variation
left join bibliography_as_string on bibliography_as_string.id = bibliography_id
left join team_as_strings on team_as_strings.id = team_id
left join glacier_country on glacier_country.glacier_id = front_variation.glacier_id
left join glacier on glacier.id = front_variation.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  begin_date_min,
  end_date_max
