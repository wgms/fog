select
  glacier_country.country_id as country,
  glacier.short_name as glacier_name,
  change.glacier_id,
  change_id,
  lower_elevation,
  upper_elevation,
  trim_scale(round(change_band.area)) as area,
  trim_scale(round(change_band.elevation_change, 3)) as elevation_change,
  trim_scale(round(change_band.elevation_change_unc, 3)) as elevation_change_unc,
  trim_scale(round(change_band.volume_change)) as volume_change,
  trim_scale(round(change_band.volume_change_unc)) as volume_change_unc,
  change_band.remarks
from change_band
left join change on change_band.change_id = change.id
left join glacier on glacier.id = change.glacier_id
left join glacier_country on glacier.id = glacier_country.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  begin_date_min,
  end_date_max,
  lower_elevation,
  upper_elevation
