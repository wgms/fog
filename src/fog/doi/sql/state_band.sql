select
  country_id as country,
  glacier.short_name as glacier_name,
  state.glacier_id,
  state_id,
  lower_elevation,
  upper_elevation,
  trim_scale(round(state_band.mean_elevation)) as mean_elevation,
  trim_scale(round(state_band.elevation_unc)) as elevation_unc,
  trim_scale(round(state_band.area)) as area,
  trim_scale(round(state_band.area_unc)) as area_unc,
  state_band.remarks
from state_band
left join state on state_band.state_id = state.id
left join glacier on state.glacier_id = glacier.id
left join glacier_country on glacier.id = glacier_country.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  date_min,
  lower_elevation,
  upper_elevation
