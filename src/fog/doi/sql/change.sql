select
  country_id as country,
  glacier.short_name as glacier_name,
  change.glacier_id,
  begin_outline_id_as_string.string as begin_outline_id,
  end_outline_id_as_string.string as end_outline_id,
  change.id,
  date_mean_from_min_max(begin_date_min, begin_date_max) as begin_date,
  date_unc_from_min_max(begin_date_min, begin_date_max) as begin_date_unc,
  date_mean_from_min_max(end_date_min, end_date_max) as end_date,
  date_unc_from_min_max(end_date_min, end_date_max) as end_date_unc,
  trim_scale(round(area)) as area,
  trim_scale(round(elevation_change, 3)) as elevation_change,
  trim_scale(round(elevation_change_unc, 3)) as elevation_change_unc,
  trim_scale(round(volume_change)) as volume_change,
  trim_scale(round(volume_change_unc)) as volume_change_unc,
  begin_platform,
  begin_method,
  end_platform,
  end_method,
  team_as_strings.investigators,
  team_as_strings.agencies,
  bibliography_as_string.string as references,
  change.remarks
from change
left join bibliography_as_string on bibliography_as_string.id = bibliography_id
left join team_as_strings on team_as_strings.id = team_id
left join outline_id_as_string as begin_outline_id_as_string on begin_outline_id = begin_outline_id_as_string.id
left join outline_id_as_string as end_outline_id_as_string on end_outline_id = end_outline_id_as_string.id
left join glacier_country on glacier_country.glacier_id = change.glacier_id
left join glacier on glacier.id = change.glacier_id
order by
  country,
  glacier_name,
  glacier_id,
  begin_date_min,
  end_date_max
