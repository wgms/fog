"""
Write a DOI release.

* Export tables from database using SQL and write to CSV files.
* Compute data-dependent variables.
* Build a datapackage.json file from the base metadata and these variables.
* Build readme from the metadata.
* Validate the datapackage.
"""

import copy
from pathlib import Path
import importlib.resources
import json
import re
import subprocess
import tempfile

import bleach
import jinja2
import jsonschema
import markdown
import pandas as pd
import sqlalchemy
import validator
import zipfile

import fog.helpers
import fog.metadata


# ---- Archive ----

def archive_db_to_parquet(path: Path) -> None:
  """Archive the database to parquet."""
  db = fog.iodb.DB()
  # Reflect with explicit schemas
  db.metadata = sqlalchemy.MetaData()
  for schema in ('meta', 'enum', 'coop', 'public'):
    db.metadata.reflect(bind=db.engine, schema=schema)
  path.mkdir(parents=True, exist_ok=True)
  for key, table in db.metadata.tables.items():
    # HACK: Skip tables without schema
    if table.schema is None:
      continue
    out_path = path / f'{key}.parquet'
    if out_path.exists():
      print(f'[{key}] Already exists')
      continue
    print(f'[{key}] Writing')
    df = db.select_table_from_copy(key)
    df.to_parquet(out_path)


# ---- Data ----

def read_table_sql(name: str) -> str:
  """Read table select SQL."""
  sql_path: Path = importlib.resources.files('fog.doi.sql').joinpath(f'{name}.sql')
  if not sql_path.exists():
    raise FileNotFoundError(f'{sql_path} not found')
  return sql_path.read_text()


def write_tables_to_csv() -> None:
  """Write tables to CSV."""
  db = fog.iodb.DB()
  sql_paths = sorted(importlib.resources.files('fog.doi.sql').glob('*.sql'))
  data_path = Path(fog.config.DOI_PATH, 'data')
  data_path.mkdir(parents=True, exist_ok=True)
  for sql_path in sql_paths:
    name = sql_path.stem
    csv_path = data_path / f'{name}.csv'
    if csv_path.exists():
      print(f'[{name}] Already exists')
      continue
    print(f'[{name}] Writing')
    df = db.select_query_from_copy(sql=read_table_sql(name), dtype='string')
    # HACK: Convert boolean columns from 't', 'f' to 'true', 'false'
    for column in df:
      if (df[column].eq('t') | df[column].eq('f')).all():
        df[column] = df[column].map({'t': 'true', 'f': 'false'})
    df.to_csv(csv_path, index=False)


def read_tables_from_csv() -> dict[str, pd.DataFrame]:
  """Read tables from CSV."""
  csv_paths = sorted(Path(fog.config.DOI_PATH, 'data').glob('*.csv'))
  dfs = {}
  for csv_path in csv_paths:
    name = csv_path.stem
    dfs[name] = pd.read_csv(csv_path, dtype='string')
  return dfs


# ---- Metadata ----

def convert_resource_and_field_names_to_keys(package: dict) -> None:
  """Convert resource and field names to keys."""
  for resource in package['resources']:
    resource['schema']['fields'] = {
      field['name']: field for field in resource['schema']['fields']
    }
  package['resources'] = {
    resource['name']: resource for resource in package['resources']
  }


def build_metadata_variables(dfs: dict[str, pd.DataFrame]) -> dict:
  """Build metadata variables."""
  variables = fog.metadata.read_yaml(
    importlib.resources.files('fog.doi').joinpath('variables.yml')
  )
  # Gather measurement years
  years = []
  for table, df in dfs.items():
    if not table in ('state', 'change', 'front_variation', 'mass_balance', 'event'):
      continue
    for name in ['year']:
      if name in df:
        years.extend(df[name].dropna().drop_duplicates())
    for name in ['date', 'begin_date', 'end_date']:
      if name in df:
        years.extend(df[name].dropna().drop_duplicates().str.slice(0, 4))
  years = pd.Series(years).astype(int)
  # Compute variables
  variables['begin_year'] = years.min()
  variables['end_year'] = years.max()
  variables['version'] = variables['date'][:7]
  variables['year'] = variables['date'][:4]
  # megabytes
  total_bytes = 0
  for path in Path(fog.config.DOI_PATH).glob('data/*.csv'):
    total_bytes += path.stat().st_size
  variables['megabytes'] = round(total_bytes / 1e6)
  return variables


def order_dict(d: dict, keys: list, extra: bool = False) -> dict:
  """Return a new dictionary with ordered keys."""
  ordered = {k: d[k] for k in keys if k in d}
  if extra:
    for k in d:
      if k not in keys:
        ordered[k] = d[k]
  return ordered


def prune_null_from_dict(d: dict) -> dict:
  """Return a new dictionary with null values dropped."""
  return {k: v for k, v in d.items() if v is not None}


def fill_dict(d: dict, default: dict) -> dict:
  """Return a new dictionary filled with default values."""
  keys = set(default.keys()) | set(d.keys())
  return {
    key: d.get(key, default.get(key))
    for key in keys
  }


RESOURCE_KEYS = [
  '$schema',
  'name',
  'title',
  'description',
  'path',
  'format',
  'mediatype',
  'schema'
]
"""Resource keys to include."""

SCHEMA_KEYS = ['$schema', 'primaryKey', 'foreignKeys', 'missingValues', 'fields']
"""Schema keys to include."""

FIELD_KEYS = [
  'name',
  'title',
  'description',
  'units',
  'type',
  'format',
  'trueValues',
  'falseValues',
  'example',
  'constraints'
]
"""Field keys to include."""

CONSTRAINTS_KEYS = [
  'required',
  'unique',
  'minimum',
  'maximum',
  'minLength',
  'maxLength',
  'enum',
  'pattern'
]
"""Field constraints keys to include."""


def write_metadata_to_json(variables: dict) -> None:
  """Build DOI metadata and write to datapackage.json."""
  # Read standard fog submission metadata
  metadata = fog.metadata.read(
    submission='extended',
    primary_keys=False,
    foreign_keys=False,
    deprecated=False,
    migrate_categories=False
  )
  metadata = copy.deepcopy(metadata)
  convert_resource_and_field_names_to_keys(metadata)
  # Inject variables and read custom doi metadata
  path: Path = importlib.resources.files('fog.doi.metadata').joinpath('index.yml')
  txt = jinja2.Template(path.read_text()).render(variables)
  with tempfile.NamedTemporaryFile(prefix=path.stem, dir=path.parent) as file:
    file.write(txt.encode())
    file.seek(0)
    doi = fog.metadata.read_yaml(file.name)
  # Merge custom doi metadata
  resources = []
  for resource in doi['resources']:
    if resource['name'] in metadata['resources']:
      old = metadata['resources'][resource['name']]
      new = fill_dict(resource, old)
      fields = []
      for field in new['schema']['fields']:
        if field['name'] in old['schema']['fields']:
          old_field = old['schema']['fields'][field['name']]
          new_field = fill_dict(field, old_field)
          if 'constraints' in field and 'constraints' in old_field:
            new_field['constraints'] = fill_dict(
              new_field['constraints'], old_field['constraints']
            )
        else:
          new_field = field
        fields.append(new_field)
      new['schema']['fields'] = fields
    else:
      new = resource
    resources.append(new)
  doi['resources'] = resources
  # Migrate categories
  fog.metadata.migrate_categories_to_enum(doi)
  # Add resource path and dialect
  for resource in doi['resources']:
    resource.update({
      '$schema': 'https://datapackage.org/profiles/2.0/dataresource.json',
      'path': f'data/{resource["name"]}.csv',
      'format': 'csv',
      'mediatype': 'text/csv',
      'dialect': {
        '$schema': 'https://datapackage.org/profiles/2.0/tabledialect.json',
        'header': True,
        'delimiter': ',',
        'quoteChar': '"',
        'doubleQuote': True,
        'lineTerminator': '\n'
      },
    })
    resource['schema'].update({
      '$schema': 'https://datapackage.org/profiles/2.0/tableschema.json',
      'missingValues': ['']
    })
    for field in resource['schema']['fields']:
      if field['type'] == 'boolean':
        field.update({
          'trueValues': ['true'],
          'falseValues': ['false']
        })
  # Order and prune keys
  doi['resources'] = [
    prune_null_from_dict(order_dict(resource, RESOURCE_KEYS))
    for resource in doi['resources']
  ]
  for resource in doi['resources']:
    resource['schema'] = prune_null_from_dict(
      order_dict(resource['schema'], SCHEMA_KEYS)
    )
    for field in resource['schema']['fields']:
      field = prune_null_from_dict(order_dict(field, FIELD_KEYS))
      if 'constraints' in field:
        field['constraints'] = prune_null_from_dict(
          order_dict(field['constraints'], CONSTRAINTS_KEYS)
        )
  # HACK: Cast example to string
  for resource in doi['resources']:
    for field in resource['schema']['fields']:
      if 'example' in field:
        field['example'] = str(field['example'])
  path = Path(fog.config.DOI_PATH, 'datapackage.json')
  path.write_text(json.dumps(doi, indent=2, ensure_ascii=False))


def read_metadata() -> dict:
  """Read DOI metadata."""
  path = Path(fog.config.DOI_PATH, 'datapackage.json')
  return json.loads(path.read_text())


# ---- Validate ----

def validate_metadata(metadata: dict) -> list[str]:
  """Validate DOI metadata against JSON Schema."""
  schema_url = metadata.get('$schema')
  if not schema_url:
    raise ValueError('Metadata missing $schema')
  validator = jsonschema.Draft202012Validator({'$ref': schema_url})
  return [e.message for e in list(validator.iter_errors(metadata))]


def check_agency_ids(df: pd.DataFrame, dfs: dict[str, pd.DataFrame]) -> bool:
  """Check whether all agency ids are present."""
  ids = []
  for idf in dfs.values():
    if 'agencies' in idf:
      ids.extend(
        idf['agencies']
        .drop_duplicates()
        .str.split(' | ', regex=False, expand=True)
        .stack(dropna=True)
        .drop_duplicates()
        .str.extract(r'^([0-9]+)\.', expand=False)
        .astype(int)
      )
  missing = set(ids) - set(df['id'])
  if missing:
    raise ValueError(f'Missing agency ids: {missing}')
  return True


def check_person_names(df: pd.DataFrame, dfs: dict[str, pd.DataFrame]) -> bool:
  """Check whether all person names are present."""
  names = []
  for idf in dfs.values():
    if 'investigators' in idf:
      names.extend(
        idf['investigators']
        .drop_duplicates()
        .str.split(' | ', regex=False, expand=True)
        .stack(dropna=True)
        .drop_duplicates()
        .str.extract(r'^([^\(]+)', expand=False)
        .str.strip()
        .dropna()
      )
  missing = set(names) - set(df['name'])
  if missing:
    raise ValueError(f'Missing person names: {missing}')
  return True


def validate_data(dfs: dict[str, pd.DataFrame]) -> validator.schema.Report:
  """Validate DOI data against metadata."""
  path = Path(fog.config.DOI_PATH, 'datapackage.json')
  package = json.loads(path.read_text())
  # HACK: Add key['reference']['resource'] to internal foreign keys
  for resource in package['resources']:
    for fk in resource['schema'].get('foreignKeys', []):
      if 'resource' not in fk['reference']:
        fk['reference']['resource'] = None
  schema = validator.convert.frictionless.package_to_schema(
    package,
    require=True,
    order=False,
    strict=True,
    columns=dict(require=True, order=True, strict=True)
  ) + validator.Schema({
    validator.Table('agency'): validator.Check(check_agency_ids),
    validator.Table('person'): validator.Check(check_person_names)
  })
  return schema(dfs)


# ---- Documentation ----

def only_linkify_http(attrs: dict, new: bool = False) -> dict:
  """Linkify only http and https (with exceptions)."""
  if not new:
    return attrs
  link_text = attrs['_text']
  if (
    not (link_text.startswith('http:') or link_text.startswith('https:'))
    or link_text in (
      'https://doi.org/doi',  # Used in references example
      'https://doi.org/{doi}',  # Used in references example
      'https://doi.org/',  # Linkified string of above
      'https://doi.org/10.2307/1551585',  # Used in references example
      'https://timarit.is/page/6575323',  # Used in references example
      'https://timarit.is/page/6575323):',  # Linkified string of above
      'https://orcid.org/{orcid}',
      'https://orcid.org/',
      'https://wikidata.org/wiki/{wikidata}',
      'https://wikidata.org/wiki/',
    )
  ):
    return None
  return attrs


LINKER = bleach.linkifier.Linker(callbacks=[only_linkify_http])
"""Custom linkifier."""

def write_documentation(metadata: dict) -> None:
  metadata = copy.deepcopy(metadata)
  # Rename "collaborators" to "contributors" with changing key order
  metadata = {
    (key if key != 'collaborators' else 'contributors') : metadata[key]
    for key in metadata
  }
  # Replace column names with links to corresponding table
  for resource in metadata['resources']:
    column_names = [field['name'] for field in resource['schema']['fields']]
    for field in resource['schema']['fields']:
      if 'description' not in field:
        raise ValueError(f'Missing description for {resource["name"]}.{field["name"]}')
      description = field['description']
      position = 0
      # Search for column names with `backticks`
      matches = list(re.finditer(r'`([a-z_\.]+)`', description))
      for match in matches:
        content = match.group(1)
        if '.' in content:
          # Link to table and column
          table, column = content.split('.')
        elif content in column_names and content != field['name']:
          # Link to column in same table
          table = resource['name']
          column = content
        else:
          continue
        inject = f'[`{content}`](#{table}.{column})'
        description = description[:match.start() + position] + inject + description[match.end() + position:]
        position += len(inject) - len(match.group(0))
      field['description'] = description
  # Render markdown
  text = fog.helpers.render_template(
    importlib.resources.files('fog.templates').joinpath('package-publish.md.jinja'),
    data={'package': metadata}
  )
  markdown_path = Path(fog.config.DOI_PATH, 'readme.md')
  markdown_path.write_text(text)
  # Convert markdown to html
  html_path = markdown_path.with_suffix('.html')
  css_path = importlib.resources.files('fog.templates').joinpath('style.css')
  subprocess.call(
    f'pandoc --number-sections --shift-heading-level-by=-1 --self-contained {markdown_path} -c {css_path} -o {html_path}',
    shell=True
  )
  # Linkify html
  text = html_path.read_text()
  text = LINKER.linkify(text)
  html_path.write_text(text)
  # Convert html to pdf
  pdf_path = html_path.with_suffix('.pdf')
  subprocess.call(
    f'wkhtmltopdf --enable-local-file-access --dpi 300 --print-media-type --footer-center [page] {html_path} {pdf_path}',
    shell=True
  )


def write_doi_release() -> validator.schema.Report:
  """Write a DOI release."""
  write_tables_to_csv()
  dfs = read_tables_from_csv()
  variables = build_metadata_variables(dfs)
  write_metadata_to_json(variables)
  metadata = read_metadata()
  write_documentation(metadata)
  errors = validate_metadata(metadata)
  if errors:
    message = 'Metadata validation errors:\n\n' + '\n'.join(
      [f'  * {error}' for error in errors]
    )
    print(message)
  report = validate_data(dfs)
  if not report.valid:
    print('Data validation errors (see report for details)')
  valid = not errors and report.valid
  if valid:
    # Build zip file
    zip_path = Path(fog.config.DOI_PATH, f"DOI-WGMS-FoG-{metadata['version']}.zip")
    with zipfile.ZipFile(zip_path, 'w', compression=zipfile.ZIP_DEFLATED) as file:
      file.write(Path(fog.config.DOI_PATH, 'datapackage.json'), 'datapackage.json')
      file.write(Path(fog.config.DOI_PATH, 'readme.pdf'), 'readme.pdf')
      for path in Path(fog.config.DOI_PATH, 'data').glob('*.csv'):
        file.write(path, f'data/{path.name}')
    # Build website HTML
    # Replace ` with <code> in collaborators (but strip <p> tags)
    metadata['collaborators'] = markdown.markdown(metadata['collaborators']).replace('<p>', '').replace('</p>', '')
    path = importlib.resources.files('fog.templates').joinpath('doi-webpage.html.jinja')
    html = jinja2.Template(path.read_text()).render({**variables, 'metadata': metadata})
    html = LINKER.linkify(html)
    Path(fog.config.DOI_PATH, 'webpage.html').write_text(html)
    # Build website info (copy readme.pdf to WGMS_DOI_{{version}}.pdf)
    Path(fog.config.DOI_PATH, f"WGMS_DOI_{metadata['version']}.pdf").write_bytes(
      Path(fog.config.DOI_PATH, 'readme.pdf').read_bytes()
    )
  return report
