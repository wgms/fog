import calendar
import datetime
import os
import re
from typing import Any, Dict, Hashable, Iterable, List, Optional, Tuple, Union

import jinja2
import numpy as np
import pandas as pd


def mask_dataframes(
  dfs: Dict[Hashable, pd.DataFrame],
  masks: Dict[Hashable, pd.DataFrame],
  value: Any = pd.NA,
  dtype: str = None,
  inplace: bool = False
) -> Optional[Dict[Hashable, pd.DataFrame]]:
  """
  Replace values at each column index to a value.

  Examples
  --------
  >>> df = pd.DataFrame({'x': ['NULL', '1', pd.NA], 'y': ['0', 'NULL', pd.NA]})
  >>> dfs = {'a': df}
  >>> nulls = {'a': df.eq('NULL')}
  >>> mask_dataframes(dfs, nulls)['a']
        x     y
  0  <NA>     0
  1     1  <NA>
  2  <NA>  <NA>

  Masking is label-based, so it only applies to cells with matching column and
  row labels.

  >>> dfs2 = {'a': df.rename(columns={'y': 'z'}, index={0: 3})}
  >>> mask_dataframes(dfs2, nulls)['a']
        x     z
  3  NULL     0
  1     1  NULL
  2  <NA>  <NA>

  Masking can be applied inplace.

  >>> mask_dataframes(dfs, nulls, inplace=True)
  >>> dfs['a']
        x     y
  0  <NA>     0
  1     1  <NA>
  2  <NA>  <NA>
  """
  new = dfs if inplace else dfs.copy()
  copied: List[Hashable] = []
  for table in set(dfs) & set(masks):
    for column in set(dfs[table]) & set(masks[table]):
      index = dfs[table][column].index.intersection(
        masks[table].index[masks[table][column]]
      )
      if not index.empty:
        if not inplace and table not in copied:
          new[table] = dfs[table].copy()
          copied.append(table)
        if dtype and dtype != str(dfs[table][column].dtype):
          new[table][column] = dfs[table][column].astype(dtype, copy=False)
        new[table].loc[index, column] = value
  return None if inplace else new


def get_index(
  df: pd.DataFrame,
  key: Iterable[Hashable] = None
) -> pd.Index:
  """
  Make an Index from a DataFrame.

  Wrapper of :meth:`pandas.MultiIndex.from_frame`.

  Examples
  --------
  Single column.

  >>> df = pd.DataFrame({'x': [0, pd.NA]})
  >>> get_index(df)
  Index([0, <NA>], dtype='object', name='x')

  Multiple columns.

  >>> df = pd.DataFrame({'x': [0, 1], 'y': [1, 2]})
  >>> get_index(df)
  MultiIndex([(0, 1), (1, 2)], names=['x', 'y'])
  """
  if key is None:
    key = list(df)
  key = list(key)
  if len(key) == 1:
    return pd.Index(df[key[0]])
  elif len(key) > 0:
    return pd.MultiIndex.from_frame(df[key])
  raise ValueError('Empty key')


def get_intersection_indexers(
  a: pd.Index,
  b: pd.Index
) -> Tuple[np.ndarray, np.ndarray]:
  """
  Get the row indices of the intersection (inner join) of two indices.

  NOTE: Gives wrong results for MultiIndex with null values.

  Examples
  --------
  >>> a = pd.Index([2, 1, 0, 3, 0, 1])
  >>> b = pd.Index([0, 2])
  >>> get_intersection_indexers(a, b)
  (array([0, 2, 4]), array([1, 0, 0]))
  """
  if not b.is_unique:
    raise ValueError('b is not unique')
  _, ia, ib = a.join(b, how='inner', return_indexers=True, sort=False)
  if ia is None:
    ia = np.arange(a.size)
  if ib is None:
    ib = np.arange(b.size)
  order = np.argsort(ia)
  ia, ib = ia[order], ib[order]
  return ia, ib


def update_dataframe(
  df: pd.DataFrame,
  update: pd.DataFrame,
  mask: pd.DataFrame = None,
  ignore_null: bool = True,
  inplace: bool = False
) -> pd.DataFrame:
  """
  Update DataFrame.

  Parameters
  ----------
  df
    Dataframe to update
  update
    Updated values
  mask
    Where to allow updates
  ignore_null
    Whether to ignore null values in update
  inplace
    Whether to modify df in place

  Example
  -------
  >>> df = pd.DataFrame({'x': [0, 1, 2]}, index=[7, 8, 9]).convert_dtypes()
  >>> update = pd.DataFrame({'x': [13, 12, pd.NA]}, index=[10, 9, 8])
  >>> mask = pd.DataFrame({'x': [False, True, False]}, index=[7, 8, 9])
  >>> update_dataframe(df, update)
      x
  7   0
  8   1
  9  12
  >>> update_dataframe(df, update, mask=mask)
     x
  7  0
  8  1
  9  2
  >>> update_dataframe(df, update, ignore_null=False)
        x
  7     0
  8  <NA>
  9    12
  >>> update_dataframe(df, update, ignore_null=False, mask=mask)
        x
  7     0
  8  <NA>
  9     2
  >>> update_dataframe(df, update, inplace=True) is None
  True
  >>> df
      x
  7   0
  8   1
  9  12
  """
  idx = df.index.intersection(update.index)
  columns = [x for x in df if x in update]
  if mask is not None:
    idx = idx.intersection(mask.index)
    columns = [x for x in columns if x in mask]
  patch: pd.DataFrame = update.loc[idx, columns]
  if ignore_null:
    # Fill null in update with original values
    patch = patch.mask(patch.isnull(), df.loc[idx, columns])
  if mask is not None:
    patch = patch.mask(
      ~mask.loc[idx, columns].astype('boolean', copy=False),
      df.loc[idx, columns]
    )
  if not inplace:
    df = df.copy()
  df.loc[idx, columns] = patch.astype(df.dtypes[columns], copy=False)
  return None if inplace else df


def fill_dataframe(
  df: pd.DataFrame,
  defaults: pd.DataFrame,
  key: List[Hashable],
  mask: pd.DataFrame = None,
) -> pd.DataFrame:
  """
  Fill dataframe with default values.

  Rows with equal `key` in both `df` and `defaults` are matched to each other.
  Null values in `df` are filled with values from `defaults` only if
  `mask` is False for that cell.

  NOTE: Modifies `df` inplace.

  Parameters
  ----------
  df
    Dataframe with new values.
  defaults
    Dataframe with default (old) values.
  key
    Primary key column names of both `df` and `defaults`.
  mask
    Where to apply updates from `df`. By default, all are applied.
    Must have the same size and indices as `df`.

  Returns
  -------
  Cells that were changed by `df`.

  Examples
  --------
  >>> defaults = pd.DataFrame({'id': [0, 1, 2], 'x': ['0', '1', '2']})
  >>> df = pd.DataFrame({'id': [3, 0, 2, 1], 'x': ['3', pd.NA, pd.NA, '9']})
  >>> fill_dataframe(df, defaults, key=['id'])
        id     x
  1  False  True
  2  False  True
  3  False  True
  >>> df
     id     x
  0   3     3
  1   0  <NA>
  2   2  <NA>
  3   1     9

  Mask can be used to control behavior of null values in `df`.

  >>> df = pd.DataFrame({'id': [3, 0, 2, 1], 'x': ['3', pd.NA, pd.NA, '9']})
  >>> mask = pd.DataFrame({'x': [True, True, False, True]})
  >>> fill_dataframe(df, defaults, key=['id'], mask=mask)
        id      x
  1  False   True
  2  False  False
  3  False   True
  >>> df
     id     x
  0   3     3
  1   0  <NA>
  2   2     2
  3   1     9

  A missing key column has the same effect as if the column were empty.

  >>> df = pd.DataFrame({'x': ['0']})
  >>> fill_dataframe(df, defaults, key=['id'])
  Empty DataFrame
  Columns: [x]
  Index: []
  """
  # Find rows with complete primary key
  key_exists = all(column in df for column in key)
  if key_exists:
    has_key = df[key].notnull().all(axis=1)
  if not key_exists or not has_key.any():
    return pd.DataFrame(columns=df.columns)

  # Find matching rows in defaults
  a = get_index(df[has_key], key)
  b = get_index(defaults, key)
  ia, ib = get_intersection_indexers(a, b)

  # Updating matching rows
  original = defaults.iloc[ib].copy()
  original.index = df[has_key].index[ia]
  updated = update_dataframe(
    original,
    update=df[has_key].iloc[ia],
    mask=None if mask is None else mask[has_key].iloc[ia],
    ignore_null=False,
    inplace=False
  )

  # Find what actually changed
  columns = [column for column in defaults if column in df]
  adf = original[columns]
  bdf = updated[columns]
  is_change = (adf != bdf).fillna(False) | (adf.isnull() != bdf.isnull())

  # Add missing non-null columns
  kept_columns = []
  for column in updated:
    if column not in df and updated[column].notnull().any():
        df[column] = pd.Series(dtype=updated[column].dtype)
        kept_columns.append(column)
    elif column in df:
      kept_columns.append(column)

  # Update rows
  df.loc[df[has_key].index[ia], kept_columns] = updated[kept_columns]
  return is_change


def extend_dataframe(
  new: pd.DataFrame,
  old: pd.DataFrame,
  key: List[Hashable]
) -> pd.DataFrame:
  """
  Examples
  --------
  >>> new = pd.DataFrame({'id': [pd.NA, 1], 'x': [0, 1]}).convert_dtypes()
  >>> old = pd.DataFrame({'id': [2, 3], 'y': [2, 3]}).convert_dtypes()
  >>> extend_dataframe(new, old, ['id'])
       id     x     y
  0  <NA>     0  <NA>
  1     1     1  <NA>
  2     2  <NA>     2
  3     3  <NA>     3
  """
  a = get_index(new, key)
  b = get_index(old, key)
  keep_from_old = ~b.isin(a)
  if not keep_from_old.any():
    return new
  rows = old[keep_from_old].copy()
  if new.empty:
    start = 0
  else:
    start = new.index.max() + 1
  rows.index = pd.RangeIndex(start, start + len(rows))
  return pd.concat((new, rows), axis=0, ignore_index=False)


def filter_dataframes(
  dfs: Dict[Hashable, pd.DataFrame],
  mask: Dict[Hashable, pd.DataFrame],
  columns = False
) -> Dict[Hashable, pd.DataFrame]:
  """
  Filter dataframes by index.

  Parameters
  ----------
  dfs
    Dataframes to filter.
  mask
    Tables, columns, and rows to select from `dfs`, if present.
  columns
    Whether to omit columns not present in `mask`.

  Examples
  --------
  >>> dfs = {
  ...   'a': pd.DataFrame({'x': [1, 2], 'y': [3, 4]}),
  ...   'b': pd.DataFrame()
  ... }
  >>> mask = {'a': pd.DataFrame(columns=['x'], index=[1])}
  >>> filter_dataframes(dfs, mask, columns=True)
  {'a':    x
  1  2}
  """
  result: Dict[Hashable, pd.DataFrame] = {}
  for table in set(dfs) & set(mask):
    rows = dfs[table].index.intersection(mask[table].index)
    cols = dfs[table].columns
    if columns:
      cols = cols.intersection(mask[table].columns)
    result[table] = dfs[table].loc[rows, cols]
  return result


def float_or_str(x: Any) -> Union[float, str]:
  """
  Return as either float or string.

  Examples
  --------
  >>> float_or_str('')
  ''
  >>> float_or_str('1')
  1.0
  >>> float_or_str('1.0')
  1.0
  """
  try:
    return float(x)
  except ValueError:
    return str(x)


def paste_string_columns(a: pd.Series, b: pd.Series, sep: str = ' | ') -> pd.Series:
  """
  Examples
  --------
  >>> a = pd.Series(['a', 'b', pd.NA, pd.NA])
  >>> b = pd.Series(['e', pd.NA, 'g', pd.NA])
  >>> paste_string_columns(a, b)
  0    a | e
  1        b
  2        g
  3     <NA>
  dtype: object
  """
  if not a.index.equals(b.index):
    raise ValueError('Indices are not equal')
  s = a.copy()
  mask = a.isnull() & b.notnull()
  s[mask] = b[mask]
  # Paste as pipe delimited list
  mask = a.notnull() & b.notnull()
  s[mask] = s[mask] + sep + b[mask]
  return s


def deduplicate_string_list(s: pd.Series, sep: str = ' | ') -> pd.Series:
  """
  Deduplicate string-formatted list.

  Examples
  --------
  >>> s = pd.Series([pd.NA, 'a', 'b | a', 'a | b', 'a | a'])
  >>> deduplicate_string_list(s)
  0     <NA>
  1        a
  2    b | a
  3    a | b
  4        a
  dtype: object
  """
  return s.str.split(pat=sep, regex=False).apply(
    lambda x: sep.join(pd.unique(x)) if isinstance(x, list) else x
  )


FUZZY_DATE_PATTERN = re.compile(r'^(?P<year>\d{4})(?:-(?P<month>\d{2})(?:-(?P<day>\d{2}))?)?$')
DATE_PATTERN = re.compile(r'^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})$')


def date_to_min_max(date: str) -> Optional[Tuple[datetime.date, datetime.date]]:
  """
  Convert fuzzy date to a min-max date range.

  Parameters
  ----------
  date
    Fuzzy date in ISO format (yyyy, yyyy-mm, yyyy-mm-dd).

  Examples
  --------
  >>> date_to_min_max('1986')
  (datetime.date(1986, 1, 1), datetime.date(1986, 12, 31))
  >>> date_to_min_max('1986-03')
  (datetime.date(1986, 3, 1), datetime.date(1986, 3, 31))
  >>> date_to_min_max('1986-03-05')
  (datetime.date(1986, 3, 5), datetime.date(1986, 3, 5))
  >>> date_to_min_max('invalid') is None
  True
  """
  match = FUZZY_DATE_PATTERN.match(date)
  if not match:
    return None
  groups = match.groupdict()
  year = int(groups['year'])
  if groups['month'] is None:
    return datetime.date(year, 1, 1), datetime.date(year, 12, 31)
  month = int(groups['month'])
  if groups['day'] is None:
    _, days = calendar.monthrange(year, month)
    return datetime.date(year, month, 1), datetime.date(year, month, days)
  day = int(groups['day'])
  return datetime.date(year, month, day), datetime.date(year, month, day)


def date_unc_to_min_max(date: str, unc: float = 0) -> Optional[Tuple[datetime.date, datetime.date]]:
  """
  Convert date and uncertainty in days to a min-max date range.

  Parameters
  ----------
  date
    Date in ISO format (yyyy-mm-dd).
  unc
    Uncertainty in days.

  Examples
  --------
  >>> date_unc_to_min_max('2020-01-02', 0.4)
  (datetime.date(2020, 1, 2), datetime.date(2020, 1, 2))
  >>> date_unc_to_min_max('2020-01-02', 0.5)
  (datetime.date(2020, 1, 2), datetime.date(2020, 1, 3))
  >>> date_unc_to_min_max('2020-01-02', 0.6)
  (datetime.date(2020, 1, 1), datetime.date(2020, 1, 3))
  >>> date_unc_to_min_max('2020-01-02', 1.5)
  (datetime.date(2020, 1, 1), datetime.date(2020, 1, 4))
  >>> date_unc_to_min_max('2020-01-02', 1.6)
  (datetime.date(2019, 12, 31), datetime.date(2020, 1, 4))
  >>> date_unc_to_min_max('invalid', 0.5) is None
  True
  """
  match = DATE_PATTERN.match(date)
  if not match:
    return None
  groups = match.groupdict()
  mid_date = datetime.datetime(
    year=int(groups['year']),
    month=int(groups['month']),
    day=int(groups['day']),
    hour=12
  )
  delta = datetime.timedelta(days=unc)
  return (mid_date - delta).date(), (mid_date + delta).date()


def pd_date_unc_to_min_max(df: pd.DataFrame, date: Hashable, unc: Hashable = None) -> pd.DataFrame:
  """
  Convert date and uncertainty to a min-max date range.

  Parameters
  ----------
  df
    Table.
  date
    Name of column with strings with format yyyy, yyyy-mm, or yyyy-mm-dd).
  unc
    Name of column with uncertainties in days.

  Examples
  --------
  >>> df = pd.DataFrame({
  ...   'date': ['2010', '2010-03', '2010-03-02', '2010-03-02', pd.NA, '2010', 'invalid'],
  ...   'unc': [pd.NA, pd.NA, pd.NA, 1, pd.NA, 1, pd.NA]
  ... })
  >>> pd_date_unc_to_min_max(df, 'date', 'unc')
              0           1
  0  2010-01-01  2010-12-31
  1  2010-03-01  2010-03-31
  2  2010-03-02  2010-03-02
  3  2010-03-01  2010-03-03
  4        <NA>        <NA>
  5        <NA>        <NA>
  6        <NA>        <NA>
  """
  is_date = df[date].notnull()
  minmax = pd.DataFrame(index=df.index, columns=[0, 1])
  if unc is not None and unc in df:
    is_fuzzy_date = is_date & df[unc].notnull()
    minmax.loc[is_fuzzy_date, :] = df[is_fuzzy_date].apply(
      lambda x: date_unc_to_min_max(x[date], x[unc]),
      axis='columns',
      result_type='expand'
    )
    is_date &= ~is_fuzzy_date
  minmax.loc[is_date, :] = df[is_date].apply(
    lambda x: date_to_min_max(x[date]) or (pd.NA, pd.NA),
    axis='columns',
    result_type='expand'
  )
  return minmax.fillna(pd.NA)


def date_min_max_to_unc(date_min: datetime.date, date_max: datetime.date) -> Optional[Tuple[str, float]]:
  """
  Convert min-max date range to date and uncertainty in days.

  Parameters
  ----------
  date_min
    Minimum date.
  date_max
    Maximum date.

  Examples
  --------
  >>> date_min_max_to_unc(datetime.date(2020, 1, 2), datetime.date(2020, 1, 2))
  ('2020-01-02', 0.0)
  >>> date_min_max_to_unc(datetime.date(2020, 1, 2), datetime.date(2020, 1, 3))
  ('2020-01-02', 0.5)
  >>> date_min_max_to_unc(datetime.date(2020, 1, 1), datetime.date(2020, 1, 3))
  ('2020-01-02', 1.0)
  >>> date_min_max_to_unc(datetime.date(2020, 1, 1), datetime.date(2020, 1, 4))
  ('2020-01-02', 1.5)
  >>> date_min_max_to_unc(datetime.date(2019, 12, 31), datetime.date(2020, 1, 4))
  ('2020-01-02', 2.0)
  """
  unc = (date_max - date_min) / 2
  mid_date = date_min + unc
  return mid_date.isoformat(), unc.total_seconds() / 86400


# ---- Jinja2 ----

def filter_dict(
  x: dict, include: list = None, exclude: list = None, order: list = None
) -> dict:
  """Filter and order dictionary by key names."""
  if include:
    x = {key: x[key] for key in x if key in include}
  if exclude:
    x = {key: x[key] for key in x if key not in exclude}
  if order:
    index = [
      (order.index(key) if key in order else len(order), i)
      for i, key in enumerate(x)
    ]
    sorted_keys = [key for _, key in sorted(zip(index, x.keys()))]
    x = {key: x[key] for key in sorted_keys}
  return x


def find_overlapping_ranges(df: pd.DataFrame) -> Iterable[list]:
  """
  Find overlapping ranges in a DataFrame.

  Examples
  --------
  >>> df = pd.DataFrame({
  ...   'lower': [0, 0, 0.5, 4, 5, 8],
  ...   'upper': [1, 1, 4, 6, 8, 9]
  ... })
  >>> list(find_overlapping_ranges(df))
  [[0, 1, 2], [3, 4]]
  """
  if len(df.columns) != 2:
    raise ValueError('DataFrame must have exactly two columns')
  # Drop missing bounds and sort by lower bound
  df = df.dropna(how='any')
  df = (
    pd.concat((df.min(axis=1), df.max(axis=1)), axis=1, ignore_index=False)
    .sort_values([0, 1])
  )
  indices = [df.index[0]]
  cursor = df[1].iloc[0]
  for i, low, high in df.iloc[1:].itertuples():
    if low < cursor:
      # Overlapping range
      indices.append(i)
      cursor = max(cursor, high)
    else:
      # New start
      if len(indices) > 1:
        yield indices
      indices = [i]
      cursor = high
  if len(indices) > 1:
    yield indices


def find_gaps_between_ranges(df: pd.DataFrame) -> Iterable[list]:
  """
  Find gaps between ranges in a DataFrame.

  Examples
  --------
  >>> df = pd.DataFrame({
  ...   'lower': [0, 0, 0.5, 5, 7, 8, 10, 11],
  ...   'upper': [1, 1, 4, 6, 8, 9, 11, 12]
  ... })
  >>> list(find_gaps_between_ranges(df))
  [[2, 3, 4], [5, 6]]
  """
  if len(df.columns) != 2:
    raise ValueError('DataFrame must have exactly two columns')
  # Drop missing bounds and sort by lower bound
  df = df.dropna(how='any')
  df = (
    pd.concat((df.min(axis=1), df.max(axis=1)), axis=1, ignore_index=False)
    .sort_values([0, 1])
  )
  indices = [df.index[0]]
  cursor = df[1].iloc[0]
  for i, low, high in df.iloc[1:].itertuples():
    if low > cursor:
      # Gap with previous range
      indices.append(i)
      cursor = max(cursor, high)
    else:
      # New start
      if len(indices) > 1:
        yield indices
      indices = [i]
      cursor = max(cursor, high)
  if len(indices) > 1:
    yield indices


def fill_and_increment_id(df: pd.DataFrame, *, column: Hashable, start: int) -> pd.DataFrame:
  """
  NOTE: Modifies table in place.

  Examples
  --------

  With a partially-filled column:

  >>> s = pd.Series([0, pd.NA, 1, pd.NA], dtype='Int64')
  >>> df = pd.DataFrame({'id': s})
  >>> fill_and_increment_id(df, column='id', start=4)
      id
  0    0
  1    4
  2    1
  3    5

  With an empty column:

  >>> df = pd.DataFrame({'id': pd.NA}, index=[0, 1])
  >>> fill_and_increment_id(df, column='id', start=4)
      id
  0    4
  1    5

  With a missing column:

  >>> df = pd.DataFrame(index=[0, 1])
  >>> fill_and_increment_id(df, column='id', start=4)
      id
  0    4
  1    5
  """
  if column not in df:
    df[column] = pd.Series(dtype='Int64')
  mask = df[column].isnull()
  df[column][mask] = np.arange(start, start + mask.sum())
  return df


def json_to_markdown(
  x: Union[dict, list, int, float, str, bool],
  level: int = 0,
  tab: int = 2,
  flatten_scalar_lists: bool = True,
) -> str:
  """Render any JSON-like object as Markdown, using nested bulleted lists."""

  def _scalar_list(x) -> bool:
    return isinstance(x, list) and all(not isinstance(xi, (dict, list)) for xi in x)

  def _iter(x: Union[dict, list, int, float, str, bool], level: int = 0) -> str:
    if isinstance(x, (dict, list)):
      if isinstance(x, dict):
        labels = [f"- `{key}`" for key in x]
      elif isinstance(x, list):
        labels = [f"- [{i + 1}]" for i in range(len(x))]
      values = x if isinstance(x, list) else list(x.values())
      if isinstance(x, list) and flatten_scalar_lists:
        scalar = [not isinstance(value, (dict, list)) for value in values]
        if all(scalar):
          values = [f"{values}"]
      lines = []
      for label, value in zip(labels, values):
        if isinstance(value, (dict, list)) and (
          not flatten_scalar_lists or not _scalar_list(value)
        ):
          lines.append(f"{label}\n{_iter(value, level=level + 1)}")
        else:
          if isinstance(value, str):
            # Indent to align following lines with '- '
            value = jinja2.filters.do_indent(value, width=2, first=False)
          lines.append(f"{label} {value}")
      txt = "\n".join(lines)
    else:
      txt = str(x)
    if level > 0:
      txt = jinja2.filters.do_indent(txt, width=tab, first=True, blank=False)
    return txt

  return jinja2.filters.do_indent(
    _iter(x, level=0), width=tab * level, first=True, blank=False
  )


def dicts_to_markdown_table(dicts: List[dict], **kwargs) -> str:
  """Tabulate dictionaries and render as a Markdown table."""
  if kwargs:
    dicts = [filter_dict(x, **kwargs) for x in dicts]
  df = pd.DataFrame(dicts)
  return df.where(df.notnull(), None).to_markdown(index=False)


class RelativeEnvironment(jinja2.Environment):
  """Override join_path() to enable relative template paths."""

  def join_path(self, template, parent):
    return os.path.normpath(os.path.join(os.path.dirname(parent), template))


def render_template(path: str, data: dict, encoding: str = "utf-8") -> str:
  dir = os.path.dirname(os.path.abspath(path))
  file = os.path.split(path)[1]
  environment = RelativeEnvironment(
    loader=jinja2.FileSystemLoader(dir, encoding=encoding),
    lstrip_blocks=True,
    trim_blocks=True,
  )
  environment.filters["filter_dict"] = filter_dict
  environment.filters["dict_to_markdown"] = json_to_markdown
  environment.filters["tabulate"] = dicts_to_markdown_table
  template = environment.get_template(file)
  return template.render(**data)
