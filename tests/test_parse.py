import pandas as pd
import pytest

import fog.parsers

DOI = '10.1017/jog.2016.112'
DOI_STRINGS = [
  f'{DOI}',
  f'doi:{DOI}',
  f'DOI:{DOI}',
  f'doi: {DOI}',
  f'doi : {DOI}',
  f'doi: https://doi.org/{DOI}',
  f'doi.org/{DOI}',
  f'dx.doi.org/{DOI}',
  f'http://doi.org/{DOI}',
  f'https://doi.org/{DOI}',
  f'http://dx.doi.org/{DOI}',
  f'https://dx.doi.org/{DOI}',
]
DOI_TEXTS = pd.read_csv('tests/data/substring_to_doi.csv').values.tolist()
ORCID = '0000-0001-8046-2210'
ORCIDX = '0000-0002-1694-233X'
ORCID_BASE_STRINGS = [
  '',
  'orcid:',
  'ORCID:',
  'orcid: ',
  'orcid : ',
  'orcid: https://orcid.org/',
  'orcid.org/',
  'www.orcid.org/',
  'http://orcid.org/',
  'https://orcid.org/',
  'http://www.orcid.org/',
  'https://www.orcid.org/',
]
ORCID_STRINGS = (
  [(base + ORCID, ORCID) for base in ORCID_BASE_STRINGS] +
  [(base + ORCIDX, ORCIDX) for base in ORCID_BASE_STRINGS] +
  [(base + ORCIDX.lower(), ORCIDX) for base in ORCID_BASE_STRINGS]
)
ORCID_TEXTS = [
  (
    'Ethan (0000-0001-8046-2210)',
    ['0000-0001-8046-2210']
  ),
  (
    'Ethan 0000-0001-8046-2210, Michael 0000-0003-2391-7877',
    ['0000-0001-8046-2210', '0000-0003-2391-7877']
  ),
]
URL_TEXTS = pd.read_csv('tests/data/substring_to_url.csv').values.tolist()


@pytest.mark.parametrize('text', DOI_STRINGS)
@pytest.mark.parametrize('doi', [DOI])
def test_extracts_doi_from_doi_string(text: str, doi: str) -> None:
  """Extracts DOI from DOI string."""
  assert fog.parsers.extract_dois(text) == [doi]


@pytest.mark.parametrize('text,doi', DOI_TEXTS)
def test_extracts_doi_from_text(text: str, doi: str) -> None:
  """Extracts DOI from text."""
  assert fog.parsers.extract_dois(text) == [doi]


@pytest.mark.parametrize('text,orcid', ORCID_STRINGS)
def test_extracts_orcid_from_orcid_string(text: str, orcid: str) -> None:
  """Extracts ORCID from ORCID string."""
  assert fog.parsers.extract_orcids(text) == [orcid]


@pytest.mark.parametrize('text,orcids', ORCID_TEXTS)
def test_extracts_orcids_from_text(text: str, orcids: list[str]) -> None:
  """Extracts ORCIDs from text."""
  assert fog.parsers.extract_orcids(text) == orcids


@pytest.mark.parametrize('text,url', URL_TEXTS)
def test_extracts_url_from_reference_substrings(text: str, url: str) -> None:
  """Extracts URL from reference substrings."""
  assert fog.parsers.extract_urls(text) == [url]


@pytest.mark.parametrize('text,url', [
  ('https://www.google.com', 'https://www.google.com'),
  ('https://www.google.com/', 'https://www.google.com/'),
  ('Google 1990 (https://www.google.com)', 'https://www.google.com'),
  ('Google 1990 (https://www.google.com): A search engine', 'https://www.google.com'),
])
def test_extracts_url_from_text(text: str, url: str) -> None:
  """Extracts URL from text."""
  assert fog.parsers.extract_urls(text) == [url]
