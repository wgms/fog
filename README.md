## Installation

Since `fog` is a Python package, it requires Python to be installed.

A reliable cross-platform solution for managing Python environments is the `conda` command line utility.
Installation instructions (for either Anaconda or the smaller MiniConda): [Windows](https://docs.conda.io/projects/continuumio-conda/en/latest/user-guide/install/windows.html) | [MacOS](https://docs.conda.io/projects/continuumio-conda/en/latest/user-guide/install/macos.html) | [Linux](https://docs.conda.io/projects/continuumio-conda/en/latest/user-guide/install/linux.html)

For example, using `conda`, we can create and activate a dedicated Python environment:

```sh
conda create --name wgms --channel conda-forge python=3.9 geopandas git
conda activate wgms
```

Then use [`pip`](https://pip.pypa.io/en/stable) to install `fog` (and its dependencies) into that environment:

```sh
pip install git+https://gitlab.com/wgms/fog.git
```

To run Jupyter notebooks in this environment, you will also need the IPython kernel:

```sh
conda install --channel conda-forge ipykernel
```

To later update your installation of `fog` to the latest version:

```sh
conda activate wgms  # if not already activated
pip install --upgrade git+https://gitlab.com/wgms/fog.git
```
